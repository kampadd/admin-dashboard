import {environment} from '../environments/environment';

export const VERSION = environment.VERSION;
export const CORE_SERVER_API = environment.CORE_SERVER_API;
export const CMDB_SERVER_API = environment.CMDB_SERVER_API;
export const GATEWAY_SERVER_API = environment.GATEWAY_SERVER_API;
export const UAA_SERVER_API = environment.UAA_SERVER_API;
export const BUSINESS_VIEW_IFRAME_URL = 'http://cockpit-dev/at/';
export const NETWORK_IFRAME_URL = 'http://cockpit-dev/nwtree/nwtreec4sam.html';
