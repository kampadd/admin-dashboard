import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthenticationGuard} from './core/auth/authentication-guard.service';

export const routes: Routes = [
  {
    path: 'auth',
    loadChildren: './auth/auth.module#AuthModule'
  }, {
    path: 'reset/finish',
    loadChildren: './auth/reset-password/reset-password.module#ResetPasswordModule'
  }, {
    path: 'home',
    loadChildren: './pages/home/home.module#HomeModule',
    canActivate: [AuthenticationGuard]
  },
  {
    path: '',
    redirectTo: 'auth',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {scrollPositionRestoration: 'top'})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
