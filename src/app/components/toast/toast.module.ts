import {MatButtonModule, MatCardModule, MatIconModule} from '@angular/material';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ToastComponent} from './toast.component';

@NgModule({
  declarations: [ToastComponent],
  imports: [
    CommonModule,
    MatCardModule,
    MatIconModule,
    MatButtonModule
  ],
  exports: [ToastComponent]
})
export class ToastModule {
}
