import {Component, Inject} from '@angular/core';
import {MAT_SNACK_BAR_DATA, MatSnackBarRef} from '@angular/material';

export interface ToastData {
  closable: boolean;
  message: string;
  type: 'info' | 'error' | 'success' | 'warning';
}

@Component({
  selector: 'app-toast',
  templateUrl: './toast.component.html',
  styleUrls: ['./toast.component.scss']
})
export class ToastComponent {

  constructor(@Inject(MAT_SNACK_BAR_DATA) public data: ToastData, private snackBarRef: MatSnackBarRef<ToastComponent>) {
  }

  close(): void {
    this.snackBarRef.dismiss();
  }

}
