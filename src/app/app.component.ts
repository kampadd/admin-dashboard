import {Component, OnInit} from '@angular/core';
import {LoadingService} from './shared/services/loading.service';
import {TranslateService} from '@ngx-translate/core';
import {IconsService} from './core/icons/icons.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  static DEFAULT_LANGUAGE = 'en';

  constructor(public loadingService: LoadingService,
              private translateService: TranslateService,
              private iconsService: IconsService) {
  }

  ngOnInit() {
    this.iconsService.registerSvgIcons();
    this.setLanguage();
  }

  private setLanguage(): void {
    this.translateService.setDefaultLang(AppComponent.DEFAULT_LANGUAGE);
  }
}
