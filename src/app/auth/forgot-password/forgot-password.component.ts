import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Component, OnInit} from '@angular/core';
import {validateEmail} from 'src/app/shared/utils/validators';
import {finalize, first, tap} from 'rxjs/operators';
import {Router} from '@angular/router';
import {AccountService} from 'src/app/core/auth/account.service';
import {LoadingService} from '../../shared/services/loading.service';
import {ToastService} from '../../shared/services/toast.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {

  forgotPasswordForm: FormGroup;

  constructor(private fb: FormBuilder,
              private router: Router,
              private accountService: AccountService,
              private toastService: ToastService,
              private loadingService: LoadingService) {
  }

  ngOnInit() {
    this.forgotPasswordForm = this.fb.group({
      email: ['', Validators.compose([Validators.required, validateEmail()])]
    });
  }

  forgotPassword(): void {
    if (this.forgotPasswordForm.valid) {
      this.loadingService.showProgressBar();
      const email = this.forgotPasswordForm.get('email').value;
      this.accountService.resetPasswordInit(email).pipe(
        first(),
        tap(() => this.toastService.open('A reset link has been set to your email', false, 'success', 5000)),
        finalize(() => this.loadingService.hideProgressBar())).subscribe();
    }
  }
}
