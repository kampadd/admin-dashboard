import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Component, OnInit} from '@angular/core';
import {validatePassword} from 'src/app/shared/utils/validators';
import {Router} from '@angular/router';
import {ICredentials} from '../../shared/interfaces/i-credentials';
import {Store} from '@ngxs/store';
import {Login} from '../../store/actions/account.action';
import {first} from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;

  constructor(private fb: FormBuilder,
              private store: Store,
              private router: Router) {
  }

  ngOnInit() {
    this.loginForm = this.fb.group({
      email: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.required, validatePassword()])],
      rememberMe: ['']
    });
  }

  login(): void {
    if (this.loginForm.valid) {
      const credentials = {
        username: this.loginForm.get('email').value,
        password: this.loginForm.get('password').value,
        rememberMe: this.loginForm.get('rememberMe').value
      } as ICredentials;

      this.store.dispatch(new Login(credentials)).pipe(first()).subscribe(() => {
        this.router.navigate(['/home/business-view']);
      });
    }
  }
}
