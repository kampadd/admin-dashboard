import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Component, OnInit} from '@angular/core';
import {finalize, first} from 'rxjs/operators';
import {AccountService} from 'src/app/core/auth/account.service';
import {ToastService} from '../../shared/services/toast.service';
import {ActivatedRoute, Router} from '@angular/router';
import {mustMatch, validatePassword} from '../../shared/utils/validators';
import {IPasswordKey} from '../../shared/interfaces/i-password-key';
import {LoadingService} from '../../shared/services/loading.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {

  account: Account;
  passwordForm: FormGroup;
  passwordRepeatError = 'New Passwords do not match';
  private key: string;

  constructor(private accountService: AccountService,
              private activatedRoute: ActivatedRoute,
              private fb: FormBuilder,
              private router: Router,
              private toastService: ToastService,
              private loadingService: LoadingService) {
  }

  ngOnInit() {
    this.createForm();
    this.key = this.activatedRoute.snapshot.queryParams['key'];
  }

  activateAccount(): void {
    if (this.passwordForm.valid) {
      this.loadingService.showProgressBar();
      const passwordKey: IPasswordKey = this.passwordForm.value;
      passwordKey.key = this.key;

      this.accountService.resetPasswordFinish(passwordKey).pipe(first(),
        finalize(() => this.loadingService.hideProgressBar())).subscribe(() => {
        this.toastService.open('Password successfully updated', false, 'success', 5000);
        this.router.navigate(['/auth/login']);
      });
    }
  }

  private createForm(): void {
    this.passwordForm = this.fb.group({
      newPassword: ['', [Validators.required, validatePassword]],
      newPasswordRepeat: ['']
    }, {validator: mustMatch});
  }
}
