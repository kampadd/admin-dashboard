export interface ITableData {
  totalCount: number;
  entries: any[];
}
