export interface IPasswordKey {
  key: string;
  newPassword: string;
}

