import {AbstractControl, FormGroup, ValidationErrors, ValidatorFn, Validators} from '@angular/forms';

export function validateEmail() {
  return (control: AbstractControl) => {
    if (!control.value) {
      return undefined;
    }
    return Validators.email(control);
  };
}

export function validatePassword() {
  return (control: AbstractControl) => {
    const pinPatternRegx = new RegExp('^.{4,}$');
    if (!control.value) {
      return undefined;
    }
    return Validators.pattern(pinPatternRegx)(control);
  };
}

export function validatePhone() {
  return (control: AbstractControl) => {
    const phonePatternRegx = new RegExp('^[0-9+()-]{10,15}$');
    if (!control.value) {
      return undefined;
    }
    return Validators.pattern(phonePatternRegx)(control);
  };
}

export function validateTime() {
  return (control: AbstractControl) => {
    if (!control.value) {
      return undefined;
    }
    const regX = new RegExp('^(0[0-9]|1[0-9]|2[0-3]|[0-9]):[0-5][0-9]$');
    if (!regX.test(control.value)) {
      return {time: true};
    }
    return undefined;
  };
}

export function validateIpCidr() {
  return (control: AbstractControl) => {
    // tslint:disable-next-line
    const regX = new RegExp('^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(?:\\/(?:[0-9]|[1-2][0-9]|3[0-2]))?$');
    if (!control.value) {
      return undefined;
    }
    if (!regX.test(control.value)) {
      return {ip: true};
    }
    return undefined;
  };
}

export const mustMatch: ValidatorFn = (formGroup: FormGroup): ValidationErrors | null => {
  const password = formGroup.get('newPassword');
  const passwordRepeat = formGroup.get('newPasswordRepeat');

  if (passwordRepeat.errors && !passwordRepeat.errors.mustMatch) {
    return;
  }

  if (password.value !== passwordRepeat.value) {
    passwordRepeat.setErrors({mustMatch: true});
  } else {
    passwordRepeat.setErrors(null);
  }
};
