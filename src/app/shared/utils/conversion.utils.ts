import moment, {Moment} from 'moment';
import {DATE_FORMAT, DATE_TIME_FORMAT, TIME_FORMAT} from './constants';

export function convertToArray(data: any): any[] {
  if (!data) {
    return [];
  }
  if (Array.isArray(data)) {
    return data;
  }
  return [data];
}

export function convertToNumberArray(data: string | string): any[] {
  if (!data) {
    return [];
  }
  if (Array.isArray(data)) {
    return data.map(Number);
  }
  return [Number(data)];
}

export function createTimeDate(tripDate: Moment, time: string): Moment {
  if (tripDate) {
    const dateLabel = `${tripDate.format(DATE_FORMAT)} ${time}`;
    return moment(dateLabel, DATE_TIME_FORMAT);
  }
  return null;
}

export function extractTime(dateTime: Moment): string {
  if (!dateTime) {
    return null;
  }
  return moment(dateTime).format(TIME_FORMAT);
}
