import {CollectionViewer, DataSource} from '@angular/cdk/collections';
import {BehaviorSubject, Observable} from 'rxjs';
import {ITableData} from '../interfaces/i-table-data';

export class TableDataSource extends DataSource<any> {

  private dataSubject = new BehaviorSubject<any[]>([]);
  private _length: number;

  constructor() {
    super();
  }

  get length() {
    return this._length;
  }

  connect(collectionViewer: CollectionViewer): Observable<any[]> {
    return this.dataSubject.asObservable();
  }

  disconnect(): void {
    this.dataSubject.complete();
  }

  update(tableData: ITableData): void {
    this._length = tableData.totalCount;
    this.dataSubject.next(tableData.entries); 
  }
}
