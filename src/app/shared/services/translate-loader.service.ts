import {Injectable} from '@angular/core';
import {TranslateLoader} from '@ngx-translate/core';
import {Observable} from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';
import {catchError} from 'rxjs/operators';

@Injectable()
export class CustomTranslateLoader implements TranslateLoader {
  constructor(private http: HttpClient) {
  }

  getTranslation(lang: string): Observable<any> {
    const url = `assets/i18n/${lang}.json`;
    return this.http.get(url).pipe(
      catchError(error => {
        return this.http.get(`assets/i18n/${lang}.json`);
      }));
  }
}
