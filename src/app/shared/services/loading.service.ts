import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {
  private _loading: boolean;

  showProgressBar() {
    setTimeout(() => {
      this._loading = true;
    });
  }

  hideProgressBar() {
    this._loading = false;
  }

  get loading(): boolean {
    return this._loading;
  }
}
