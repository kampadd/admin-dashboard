import {Injectable} from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material';
import {ConfirmationDialogComponent} from '../../components/confirmation-dialog/confirmation-dialog.component';

@Injectable({
  providedIn: 'root'
})
export class ConfirmationDialogService {

  private _dialogRef: MatDialogRef<ConfirmationDialogComponent>;

  constructor(public dialog: MatDialog) {
  }

  public open(message: string) {
    this._dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      data: message
    });
  }

  public get dialogRef(): MatDialogRef<ConfirmationDialogComponent> {
    return this._dialogRef;
  }
}
