import {Injectable} from '@angular/core';
import {MatSnackBar} from '@angular/material';
import {ToastComponent, ToastData} from 'src/app/components/toast/toast.component';

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  constructor(public snackBar: MatSnackBar) {
  }

  public open(message: string, closable: boolean, type: 'info' | 'error' | 'success' | 'warning', duration?: number) {
    this.snackBar.openFromComponent(ToastComponent, {
      data: {
        message: message,
        closable: closable
      } as ToastData,
      duration: duration,
      verticalPosition: 'top',
      panelClass: [`toast-${type}`]
    });
  }
}
