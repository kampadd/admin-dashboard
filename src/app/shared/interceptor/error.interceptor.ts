import {ToastService} from '../services/toast.service';
import {LoadingService} from '../services/loading.service';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {Store} from '@ngxs/store';
import {AccountState} from '../../store/states/account.state';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

  constructor(private toastService: ToastService, private loadingService: LoadingService, private router: Router, private store: Store) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const hasAccount = !!this.store.selectSnapshot(AccountState.getAccount);
    return next.handle(request).pipe(
      catchError((error: Error) => {
        this.loadingService.hideProgressBar();
        const message = `${error.message}`.trim();
        this.toastService.open(message, true, 'error');

        if (error instanceof HttpErrorResponse && error.status === 401) {
          if (hasAccount) {
            this.router.navigate(['/auth/login']);
          }
        }
        return throwError(error);
      })
    );
  }
}
