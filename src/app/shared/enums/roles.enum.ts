export enum AppRoles {
  ADMIN = 'ROLE_ADMIN',
  USER = 'ROLE_USER'
}
