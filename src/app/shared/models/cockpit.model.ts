import {Minion} from './minion.model';
import {MetaData} from './meta-data.model';

export enum CockpitConnectedEnum {
  YES = 'YES',
  NO = 'NO'
}

export interface Cockpit {
  id?: string;
  name?: string;
  description?: string;
  url?: string;
  state?: CockpitConnectedEnum;
  metadata?: MetaData;
  minions?: Minion[];
}
