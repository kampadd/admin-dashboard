import { Moment } from 'moment';

export interface IComputer {
  id?: string;
  idClass?: string;
  code?: string;
  description?: string;
  status?: string;
  user?: string;
  beginDate?: Moment;
  notes?: string;
  endDate?: Moment;
  currentId?: string;
  idTenant?: string;
  serialNumber?: string;
  supplier?: number;
  purchaseDate?: Moment;
  acceptanceDate?: Moment;
  finalCost?: number;
  brand?: number;
  model?: string;
  room?: number;
  assignee?: number;
  technicalReference?: number;
  workplace?: number;
  acceptanceNotes?: string;
  ram?: number;
  cpuNumber?: number;
  cpuSpeed?: number;
  hdSize?: number;
  ipAddress?: string;
}