import {Minion} from './minion.model';

export enum Monitoring {
  YES = 'YES',
  NO = 'NO'
}

export interface SnmpConfig {
  id?: string;
  port?: number;
  community?: string;
  monitored?: Monitoring;
  ipaddress?: string;
  minions?: Minion[];
}
