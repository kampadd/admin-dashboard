export interface MetaData {
  createdTimestamp?: string;
  modifiedTimestamp?: string;
  createdByPerson?: string;
  modifiedByPerson?: string;
}
