import {Minion} from './minion.model';

export enum BasicMonitoringType {
  ICMP = 'ICMP',
  TCP = 'TCP',
  HTTP = 'HTTP'
}

export enum BasicMonitoringMode {
  ALL = 'ALL',
  ANY = 'ANY'
}

export interface BasicMonitoring {
  id?: string;
  name?: string;
  minion?: Minion;
  type?: BasicMonitoringType;
  enabled?: boolean;
  schedule?: string;
  ipv4?: boolean;
  ipv6?: boolean;
  mode?: BasicMonitoringMode;
  timeout?: any;
  fields?: any;
  fieldsUnderRoot?: any;
  tags?: any;
  processors?: any;
}
