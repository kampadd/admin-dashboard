export interface Network {
  name?: string;
  gateway?: string;
  cidr?: string;
}
