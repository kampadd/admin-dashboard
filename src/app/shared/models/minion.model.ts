import {MetaData} from './meta-data.model';

export enum MinionConnectedEnum {
  YES = 'YES',
  NO = 'NO'
}

export interface Minion {
  id?: string;
  name?: string;
  command?: string;
  url?: string;
  state?: MinionConnectedEnum;
  metadata?: MetaData;
}
