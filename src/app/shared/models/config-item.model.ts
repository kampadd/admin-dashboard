import { Moment } from 'moment';

export interface IConfigItem {
    id?: string;
    idClass?: string;
    code?: string;
    description?: string;
    status?: string;
    user?: string;
    beginDate?: Moment;
    notes?: string;
    endDate?: Moment;
    currentId?: string;
    idTenant?: string;
  }