import { Moment } from 'moment';

export interface IPrinter {
    id?: string;
    idClass?: string;
    code?: string;
    description?: string;
    status?: string;
    user?: string;
    beginDate?: Moment;
    notes?: string;
    endDate?: Moment;
    currentId?: string;
    idTenant?: string;
    serialNumber?: string;
    supplier?: number;
    purchaseDate?: Moment;
    acceptanceDate?: Moment;
    finalCost?: number;
    brand?: number;
    model?: string;
    room?: number;
    assignee?: number;
    technicalReference?: number;
    workplace?: number;
    acceptanceNotes?: string;
    type?: number;
    paperSize?: number;
    color?: boolean;
    usage?: number;
  }