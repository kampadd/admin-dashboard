import {Minion} from './minion.model';
import {Moment} from 'moment';

export enum RunState {
  SCHEDULED = 'SCHEDULED',
  SUCCESS = 'SUCCESS',
  FAILED = 'FAILED',
  RUNNING = 'RUNNING',
  STALLED = 'STALLED',
  STARTED = 'STARTED',
  STOPPED = 'STOPPED'
}

export interface IpDiscoveryConfig {
  id?: string;
  name?: string;
  network?: string;
  description?: string;
  schedule?: Moment;
  runStatus?: RunState;
  minion?: Minion;
}
