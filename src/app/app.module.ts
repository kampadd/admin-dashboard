import {ConfirmationDialogComponent} from './components/confirmation-dialog/confirmation-dialog.component';
import {ConfirmationDialogModule} from './components/confirmation-dialog/confirmation-dialog.module';
import {MatProgressBarModule, MatSnackBarModule} from '@angular/material';
import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from '@angular/common/http';
import {AuthInterceptor} from './shared/interceptor/auth.interceptor';
import {OverlayModule} from '@angular/cdk/overlay';
import {ToastModule} from './components/toast/toast.module';
import {ToastComponent} from './components/toast/toast.component';
import {NgxWebstorageModule} from 'ngx-webstorage';
import {HashLocationStrategy, LocationStrategy} from '@angular/common';
import {CustomTranslateLoader} from './shared/services/translate-loader.service';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {NgxsModule, Store} from '@ngxs/store';
import {NgxsDispatchPluginModule} from '@ngxs-labs/dispatch-decorator';
import {NgxsReduxDevtoolsPluginModule} from '@ngxs/devtools-plugin';
import {ErrorInterceptor} from './shared/interceptor/error.interceptor';
import {ToastService} from './shared/services/toast.service';
import {LoadingService} from './shared/services/loading.service';
import {SocketIoModule} from 'ngx-socket-io';
import {AccountState} from './store/states/account.state';
import {CookieService} from 'ngx-cookie-service';
import {Router} from '@angular/router';


export function createTranslateLoader(http: HttpClient) {
  return new CustomTranslateLoader(http);
}

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    NgxWebstorageModule.forRoot(),
    MatProgressBarModule,
    OverlayModule,
    MatSnackBarModule,
    ToastModule,
    SocketIoModule,
    ConfirmationDialogModule,
    NgxsReduxDevtoolsPluginModule.forRoot(),
    NgxsModule.forRoot([AccountState]),
    NgxsDispatchPluginModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    })
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
      deps: [CookieService]
    }, {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorInterceptor,
      multi: true,
      deps: [ToastService, LoadingService, Router, Store]
    },
    {provide: LocationStrategy, useClass: HashLocationStrategy},
    CookieService
  ],
  entryComponents: [ToastComponent, ConfirmationDialogComponent],
  bootstrap: [AppComponent]
})
export class AppModule {
}
