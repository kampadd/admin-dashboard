import {Action, Selector, State, StateContext} from '@ngxs/store';
import {catchError, finalize, first, map, tap} from 'rxjs/operators';
import {of} from 'rxjs';
import {LoadingService} from '../../shared/services/loading.service';
import {ITableData} from '../../shared/interfaces/i-table-data';
import {ConfirmationDialogService} from '../../shared/services/confirmation-dialog.service';
import {ToastService} from '../../shared/services/toast.service';
import {ConfigItemService} from '../../pages/assets/config-item/config-item.service';
import {AddConfigItem, DeleteConfigItem, FetchConfigItems, UpdateConfigItem} from '../actions/config-item.action';

export class ConfigItemStateModel {
  data: ITableData;
} 

@State<ConfigItemStateModel>({
  name: 'configItems',
  defaults: {
    data: {
      totalCount: 0,
      entries: []
    }
  }
})

export class ConfigItemState {

  constructor(private configItemService: ConfigItemService,
              private loadingService: LoadingService,
              private confirmationDialogService: ConfirmationDialogService,
              private toastService: ToastService) {
  }

  @Selector()
  static getTableData(state: ConfigItemStateModel): ITableData {
    return state.data;
  }

  @Action(AddConfigItem)
  add(ctx: StateContext<ConfigItemStateModel>, action: AddConfigItem) {
    this.loadingService.showProgressBar();
    return this.configItemService.create(action.configItem).pipe(first(),
      tap(() =>
        this.toastService.open(`ConfigItem successfully created`, true, 'success', 5000)),
      map(() => ctx.dispatch(new FetchConfigItems())),
      finalize(() => this.loadingService.hideProgressBar()));
  }

  @Action(UpdateConfigItem)
  update(ctx: StateContext<ConfigItemStateModel>, action: UpdateConfigItem) {
    this.loadingService.showProgressBar();
    return this.configItemService.update(action.configItem).pipe(first(),
      tap(() =>
        this.toastService.open(`ConfigItem successfully updated`, true, 'success', 5000)),
      map(() => ctx.dispatch(new FetchConfigItems())),
      finalize(() => this.loadingService.hideProgressBar())
    );
  }

  @Action(DeleteConfigItem)
  remove(ctx: StateContext<ConfigItemStateModel>, {id}: DeleteConfigItem) {
    this.loadingService.showProgressBar();
    return this.configItemService.delete(id).pipe(
      tap(() =>
        this.toastService.open(`Entry successfully deleted`, true, 'success', 5000)),
      map(() => ctx.dispatch(new FetchConfigItems())),
      finalize(() => this.loadingService.hideProgressBar()));
  }

  @Action(FetchConfigItems)
  fetch({getState, patchState}: StateContext<any>, action: FetchConfigItems) {
    this.loadingService.showProgressBar();
    return this.configItemService.query({
      page: action.pageIndex,
      size: action.pageSize,
      sort: action.sort
    }).pipe(
      tap((_data: ITableData) => {
        patchState({
          data: _data
        });
      }),
      catchError(() => of({totalCount: 0, entries: []} as ITableData)),
      finalize(() => this.loadingService.hideProgressBar())
    );
  }
}
