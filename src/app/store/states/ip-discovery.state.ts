import {Action, Selector, State, StateContext} from '@ngxs/store';
import {catchError, finalize, first, map, tap} from 'rxjs/operators';
import {of} from 'rxjs';
import {LoadingService} from '../../shared/services/loading.service';
import {ITableData} from '../../shared/interfaces/i-table-data';
import {IpDiscoveryService} from '../../pages/ip-discovery/ip-discovery.service';
import {
  AddIpDiscoveryConfig,
  DeleteIpDiscoveryConfig,
  FetchIpDiscoveryListData,
  UpdateIpDiscoveryConfig
} from '../actions/ip-discovery.action';
import {ToastService} from '../../shared/services/toast.service';

export class IpDiscoveryStateModel {
  data: ITableData;
}

@State<IpDiscoveryStateModel>({
  name: 'ipDiscovery',
  defaults: {
    data: {
      totalCount: 0,
      entries: []
    }
  }
})
export class IpDiscoveryState {

  constructor(private ipDiscoveryService: IpDiscoveryService,
              private loadingService: LoadingService,
              private toastService: ToastService) {
  }

  @Selector()
  static getTableData(state: IpDiscoveryStateModel): ITableData {
    return state.data;
  }

  @Action(AddIpDiscoveryConfig)
  add(ctx: StateContext<IpDiscoveryStateModel>, action: AddIpDiscoveryConfig) {
    this.loadingService.showProgressBar();
    return this.ipDiscoveryService.create(action.config).pipe(
      tap(() =>
        this.toastService.open(`Entry successfully added`, true, 'success', 5000)),
      map(() => ctx.dispatch(new FetchIpDiscoveryListData())),
      catchError(() => of({totalCount: 0, entries: []} as ITableData)),
      finalize(() => this.loadingService.hideProgressBar())
    );
  }

  @Action(UpdateIpDiscoveryConfig)
  update(ctx: StateContext<IpDiscoveryStateModel>, action: UpdateIpDiscoveryConfig) {
    this.loadingService.showProgressBar();
    return this.ipDiscoveryService.update(action.config).pipe(
      tap(() =>
        this.toastService.open(`Entry successfully updated`, true, 'success', 5000)),
      map(() => ctx.dispatch(new FetchIpDiscoveryListData())),
      catchError(() => of({totalCount: 0, entries: []} as ITableData)),
      finalize(() => this.loadingService.hideProgressBar())
    );
  }

  @Action(DeleteIpDiscoveryConfig)
  remove(ctx: StateContext<IpDiscoveryStateModel>, {id}: DeleteIpDiscoveryConfig) {
    this.loadingService.showProgressBar();
    return this.ipDiscoveryService.delete(id).pipe(first(),
      tap(() =>
        this.toastService.open(`Entry successfully deleted`, true, 'success', 5000)),
      map(() => ctx.dispatch(new FetchIpDiscoveryListData())),
      finalize(() => this.loadingService.hideProgressBar()));
  }

  @Action(FetchIpDiscoveryListData)
  fetch({getState, patchState}: StateContext<IpDiscoveryStateModel>, action: FetchIpDiscoveryListData) {
    this.loadingService.showProgressBar();
    return this.ipDiscoveryService.query({
      page: action.pageIndex,
      size: action.pageSize
    }).pipe(
      tap((_data: ITableData) => {
        patchState({
          data: _data
        });
      }),
      catchError(() => of({totalCount: 0, entries: []} as ITableData)),
      finalize(() => this.loadingService.hideProgressBar())
    );
  }
}
