import {Action, Selector, State, StateContext} from '@ngxs/store';
import {catchError, finalize, first, map, tap} from 'rxjs/operators';
import {of} from 'rxjs';
import {LoadingService} from '../../shared/services/loading.service';
import {ITableData} from '../../shared/interfaces/i-table-data';
import {ConfirmationDialogService} from '../../shared/services/confirmation-dialog.service';
import {ToastService} from '../../shared/services/toast.service';
import {PrinterService} from '../../pages/assets/printer/printer.service';
import {AddPrinter, DeletePrinter, FetchPrinters, UpdatePrinter} from '../actions/printer.action';

export class PrinterStateModel {
  data: ITableData;
} 

@State<PrinterStateModel>({
  name: 'printers',
  defaults: {
    data: {
      totalCount: 0,
      entries: []
    }
  }
})

export class PrinterState {

  constructor(private printerService: PrinterService,
              private loadingService: LoadingService,
              private confirmationDialogService: ConfirmationDialogService,
              private toastService: ToastService) {
  }

  @Selector()
  static getTableData(state: PrinterStateModel): ITableData {
    return state.data;
  }

  @Action(AddPrinter)
  add(ctx: StateContext<PrinterStateModel>, action: AddPrinter) {
    this.loadingService.showProgressBar();
    return this.printerService.create(action.printer).pipe(first(),
      tap(() =>
        this.toastService.open(`Printer successfully created`, true, 'success', 5000)),
      map(() => ctx.dispatch(new FetchPrinters())),
      finalize(() => this.loadingService.hideProgressBar()));
  }

  @Action(UpdatePrinter)
  update(ctx: StateContext<PrinterStateModel>, action: UpdatePrinter) {
    this.loadingService.showProgressBar();
    return this.printerService.update(action.printer).pipe(first(),
      tap(() =>
        this.toastService.open(`Printer successfully updated`, true, 'success', 5000)),
      map(() => ctx.dispatch(new FetchPrinters())),
      finalize(() => this.loadingService.hideProgressBar())
    );
  }

  @Action(DeletePrinter)
  remove(ctx: StateContext<PrinterStateModel>, {id}: DeletePrinter) {
    this.loadingService.showProgressBar();
    return this.printerService.delete(id).pipe(
      tap(() =>
        this.toastService.open(`Entry successfully deleted`, true, 'success', 5000)),
      map(() => ctx.dispatch(new FetchPrinters())),
      finalize(() => this.loadingService.hideProgressBar()));
  }

  @Action(FetchPrinters)
  fetch({getState, patchState}: StateContext<any>, action: FetchPrinters) {
    this.loadingService.showProgressBar();
    return this.printerService.query({
      page: action.pageIndex,
      size: action.pageSize,
      sort: action.sort
    }).pipe(
      tap((_data: ITableData) => {
        patchState({
          data: _data
        });
      }),
      catchError(() => of({totalCount: 0, entries: []} as ITableData)),
      finalize(() => this.loadingService.hideProgressBar())
    );
  }
}
