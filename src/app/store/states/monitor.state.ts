import {ITableData} from 'src/app/shared/interfaces/i-table-data';
import {Action, Selector, State, StateContext} from '@ngxs/store';
import {MonitorService} from 'src/app/pages/assets/monitor/monitor.service';
import {LoadingService} from 'src/app/shared/services/loading.service';
import {ConfirmationDialogService} from 'src/app/shared/services/confirmation-dialog.service';
import {ToastService} from 'src/app/shared/services/toast.service';
import {AddMonitor, DeleteMonitor, FetchMonitors, UpdateMonitor} from '../actions/monitor.action';
import {catchError, finalize, first, map, tap} from 'rxjs/operators';
import {of} from 'rxjs';

export class MonitorStateModel {
  data: ITableData;
}

@State<MonitorStateModel>({
  name: 'monitors',
  defaults: {
    data: {
      totalCount: 0,
      entries: []
    }
  }
})

export class MonitorState {

  constructor(private monitorService: MonitorService,
              private loadingService: LoadingService,
              private confirmationDialogService: ConfirmationDialogService,
              private toastService: ToastService) {
  }

  @Selector()
  static getTableData(state: MonitorStateModel): ITableData {
    return state.data;
  }

  @Action(AddMonitor)
  add(ctx: StateContext<MonitorStateModel>, action: AddMonitor) {
    this.loadingService.showProgressBar();
    return this.monitorService.create(action.monitor).pipe(first(),
      tap(() =>
        this.toastService.open(`Monitor successfully created`, true, 'success', 5000)),
      map(() => ctx.dispatch(new FetchMonitors())),
      finalize(() => this.loadingService.hideProgressBar()));
  }

  @Action(UpdateMonitor)
  update(ctx: StateContext<MonitorStateModel>, action: UpdateMonitor) {
    this.loadingService.showProgressBar();
    return this.monitorService.update(action.monitor).pipe(first(),
      tap(() =>
        this.toastService.open(`Monitor successfully updated`, true, 'success', 5000)),
      map(() => ctx.dispatch(new FetchMonitors())),
      finalize(() => this.loadingService.hideProgressBar())
    );
  }

  @Action(DeleteMonitor)
  remove(ctx: StateContext<MonitorStateModel>, {id}: DeleteMonitor) {
    this.loadingService.showProgressBar();
    return this.monitorService.delete(id).pipe(
      tap(() =>
        this.toastService.open(`Entry successfully deleted`, true, 'success', 5000)),
      map(() => ctx.dispatch(new FetchMonitors())),
      finalize(() => this.loadingService.hideProgressBar()));
  }

  @Action(FetchMonitors)
  fetch({getState, patchState}: StateContext<any>, action: FetchMonitors) {
    this.loadingService.showProgressBar();
    return this.monitorService.query({
      page: action.pageIndex,
      size: action.pageSize,
      sort: action.sort
    }).pipe(
      tap((_data: ITableData) => {
        patchState({
          data: _data
        });
      }),
      catchError(() => of({totalCount: 0, entries: []} as ITableData)),
      finalize(() => this.loadingService.hideProgressBar())
    );
  }
}
