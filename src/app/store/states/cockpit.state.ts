import {Action, Selector, State, StateContext} from '@ngxs/store';
import {catchError, finalize, map, tap} from 'rxjs/operators';
import {EMPTY} from 'rxjs';
import {LoadingService} from '../../shared/services/loading.service';
import {FetchCockpit, UpdateCockpit} from '../actions/cockpit.action';
import {CockpitService} from '../../pages/cockpit/cockpit.service';
import {Cockpit} from '../../shared/models/cockpit.model';
import {ToastService} from '../../shared/services/toast.service';

export class CockpitStateModel {
  cockpits: Cockpit[];
}

@State<CockpitStateModel>({
  name: 'cockpit',
  defaults: {
    cockpits: []
  }
})
export class CockpitState {

  constructor(private cockpitService: CockpitService, private loadingService: LoadingService,
              private toastService: ToastService) {
  }

  @Selector()
  static getCockpits(state: CockpitStateModel): Cockpit[] {
    return state.cockpits;
  }

  @Action(FetchCockpit)
  fetch({patchState}: StateContext<CockpitStateModel>) {
    this.loadingService.showProgressBar();
    return this.cockpitService.fetch().pipe(
      tap((cockpits: Cockpit[]) => {
        patchState({
          cockpits: cockpits
        });
      }),
      catchError(() => EMPTY),
      finalize(() => this.loadingService.hideProgressBar())
    );
  }

  @Action(UpdateCockpit)
  update(ctx: StateContext<CockpitStateModel>, action: UpdateCockpit) {
    this.loadingService.showProgressBar();
    return this.cockpitService.update(action.cockpit).pipe(
      tap(() =>
        this.toastService.open(`Cockpit successfully updated`, true, 'success', 5000)),
      map(() => ctx.dispatch(new FetchCockpit())),
      finalize(() => this.loadingService.hideProgressBar())
    );
  }
}
