import {Action, Selector, State, StateContext} from '@ngxs/store';
import {catchError, finalize, first, map, tap} from 'rxjs/operators';
import {of} from 'rxjs';
import {LoadingService} from '../../shared/services/loading.service';
import {ITableData} from '../../shared/interfaces/i-table-data';
import {AssetService} from '../../pages/assets/asset/asset.service';
import {AddAsset, DeleteAsset, FetchAssets, UpdateAsset} from '../actions/asset.action';
import {ToastService} from 'src/app/shared/services/toast.service';
import {ConfirmationDialogService} from 'src/app/shared/services/confirmation-dialog.service';

export class AssetStateModel {
  data: ITableData;
}

@State<AssetStateModel>({
  name: 'asset',
  defaults: {
    data: {
      totalCount: 0,
      entries: []
    }
  }
})

export class AssetState {

  constructor(private assetService: AssetService,
              private loadingService: LoadingService,
              private confirmationDialogService: ConfirmationDialogService,
              private toastService: ToastService) {
  }

  @Selector()
  static getTableData(state: AssetStateModel): ITableData {
    return state.data;
  }

  @Action(AddAsset)
  add(ctx: StateContext<AssetStateModel>, action: AddAsset) {
    this.loadingService.showProgressBar();
    return this.assetService.create(action.asset).pipe(first(),
      tap(() =>
        this.toastService.open(`Asset successfully created`, true, 'success', 5000)),
      map(() => ctx.dispatch(new FetchAssets())),
      finalize(() => this.loadingService.hideProgressBar()));
  }

  @Action(UpdateAsset)
  update(ctx: StateContext<AssetStateModel>, action: UpdateAsset) {
    this.loadingService.showProgressBar();
    return this.assetService.update(action.asset).pipe(first(),
      tap(() =>
        this.toastService.open(`Asset successfully updated`, true, 'success', 5000)),
      map(() => ctx.dispatch(new FetchAssets())),
      finalize(() => this.loadingService.hideProgressBar())
    );
  }

  @Action(DeleteAsset)
  remove(ctx: StateContext<AssetStateModel>, {id}: DeleteAsset) {
    this.loadingService.showProgressBar();
    return this.assetService.delete(id).pipe(
      tap(() =>
        this.toastService.open(`Entry successfully deleted`, true, 'success', 5000)),
      map(() => ctx.dispatch(new FetchAssets())),
      finalize(() => this.loadingService.hideProgressBar()));
  }

  @Action(FetchAssets)
  fetch({getState, patchState}: StateContext<any>, action: FetchAssets) {
    this.loadingService.showProgressBar();
    return this.assetService.query({
      page: action.pageIndex,
      size: action.pageSize,
      sort: action.sort
    }).pipe(
      tap((_data: ITableData) => {
        patchState({
          data: _data
        });
      }),
      catchError(() => of({totalCount: 0, entries: []} as ITableData)),
      finalize(() => this.loadingService.hideProgressBar())
    );
  }
}
