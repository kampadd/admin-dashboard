import {Action, Selector, State, StateContext} from '@ngxs/store';
import {AddUser, DeleteUser, FetchListUserData, UpdateUser} from '../actions/user.action';
import {catchError, finalize, first, map, tap} from 'rxjs/operators';
import {of} from 'rxjs';
import {IUser} from '../../shared/models/user.model';
import {UserService} from '../../pages/user/user.service';
import {LoadingService} from '../../shared/services/loading.service';
import {ITableData} from '../../shared/interfaces/i-table-data';
import {ConfirmationDialogService} from '../../shared/services/confirmation-dialog.service';
import {ToastService} from '../../shared/services/toast.service';

export class UserStateModel {
  data: ITableData;
}

@State<UserStateModel>({
  name: 'users',
  defaults: {
    data: {
      totalCount: 0,
      entries: []
    }
  }
})
export class UserState {

  constructor(private userService: UserService,
              private loadingService: LoadingService,
              private confirmationDialogService: ConfirmationDialogService,
              private toastService: ToastService) {
  }

  @Selector()
  static getTableData(state: UserStateModel): ITableData {
    return state.data;
  }

  @Selector()
  static getUsers(state: UserStateModel): IUser[] {
    return state.data.entries;
  }

  @Action(AddUser)
  add(ctx: StateContext<UserStateModel>, action: AddUser) {
    this.loadingService.showProgressBar();
    return this.userService.create(action.user).pipe(first(),
      tap(() =>
        this.toastService.open(`User successfully created`, true, 'success', 5000)),
      map(() => ctx.dispatch(new FetchListUserData())),
      finalize(() => this.loadingService.hideProgressBar()));
  }

  @Action(UpdateUser)
  update(ctx: StateContext<UserStateModel>, action: UpdateUser) {
    this.loadingService.showProgressBar();
    return this.userService.update(action.user).pipe(first(),
      tap(() =>
        this.toastService.open(`User successfully updated`, true, 'success', 5000)),
      map(() => ctx.dispatch(new FetchListUserData())),
      finalize(() => this.loadingService.hideProgressBar())
    );
  }

  @Action(DeleteUser)
  remove(ctx: StateContext<UserStateModel>, {id}: DeleteUser) {
    this.loadingService.showProgressBar();
    return this.userService.delete(id).pipe(
      tap(() =>
        this.toastService.open(`Entry successfully deleted`, true, 'success', 5000)),
      map(() => ctx.dispatch(new FetchListUserData())),
      finalize(() => this.loadingService.hideProgressBar()));
  }

  @Action(FetchListUserData)
  fetch({getState, patchState}: StateContext<any>, action: FetchListUserData) {
    this.loadingService.showProgressBar();
    return this.userService.query({
      page: action.pageIndex,
      size: action.pageSize,
      sort: action.sort
    }).pipe(
      tap((_data: ITableData) => {
        patchState({
          data: _data
        });
      }),
      catchError(() => of({totalCount: 0, entries: []} as ITableData)),
      finalize(() => this.loadingService.hideProgressBar())
    );
  }
}
