import {ITableData} from 'src/app/shared/interfaces/i-table-data';
import {Action, Selector, State, StateContext} from '@ngxs/store';
import {LicenseService} from 'src/app/pages/assets/license/license.service';
import {LoadingService} from 'src/app/shared/services/loading.service';
import {ConfirmationDialogService} from 'src/app/shared/services/confirmation-dialog.service';
import {ToastService} from 'src/app/shared/services/toast.service';
import {AddLicense, DeleteLicense, FetchLicenses, UpdateLicense} from '../actions/license.action';
import {catchError, finalize, first, map, tap} from 'rxjs/operators';
import {of} from 'rxjs';

export class LicenseStateModel {
  data: ITableData;
}

@State<LicenseStateModel>({
  name: 'licenses',
  defaults: {
    data: {
      totalCount: 0,
      entries: []
    }
  }
})

export class LicenseState {

  constructor(private licenseService: LicenseService,
              private loadingService: LoadingService,
              private confirmationDialogService: ConfirmationDialogService,
              private toastService: ToastService) {
  }

  @Selector()
  static getTableData(state: LicenseStateModel): ITableData {
    return state.data;
  }

  @Action(AddLicense)
  add(ctx: StateContext<LicenseStateModel>, action: AddLicense) {
    this.loadingService.showProgressBar();
    return this.licenseService.create(action.license).pipe(first(),
      tap(() =>
        this.toastService.open(`License successfully created`, true, 'success', 5000)),
      map(() => ctx.dispatch(new FetchLicenses())),
      finalize(() => this.loadingService.hideProgressBar()));
  }

  @Action(UpdateLicense)
  update(ctx: StateContext<LicenseStateModel>, action: UpdateLicense) {
    this.loadingService.showProgressBar();
    return this.licenseService.update(action.license).pipe(first(),
      tap(() =>
        this.toastService.open(`License successfully updated`, true, 'success', 5000)),
      map(() => ctx.dispatch(new FetchLicenses())),
      finalize(() => this.loadingService.hideProgressBar())
    );
  }

  @Action(DeleteLicense)
  remove(ctx: StateContext<LicenseStateModel>, {id}: DeleteLicense) {
    this.loadingService.showProgressBar();
    return this.licenseService.delete(id).pipe(
      tap(() =>
        this.toastService.open(`Entry successfully deleted`, true, 'success', 5000)),
      map(() => ctx.dispatch(new FetchLicenses())),
      finalize(() => this.loadingService.hideProgressBar()));
  }

  @Action(FetchLicenses)
  fetch({getState, patchState}: StateContext<any>, action: FetchLicenses) {
    this.loadingService.showProgressBar();
    return this.licenseService.query({
      page: action.pageIndex,
      size: action.pageSize,
      sort: action.sort
    }).pipe(
      tap((_data: ITableData) => {
        patchState({
          data: _data
        });
      }),
      catchError(() => of({totalCount: 0, entries: []} as ITableData)),
      finalize(() => this.loadingService.hideProgressBar())
    );
  }
}
