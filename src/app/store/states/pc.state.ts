import {Action, Selector, State, StateContext} from '@ngxs/store';
import {catchError, finalize, first, map, tap} from 'rxjs/operators';
import {of} from 'rxjs';
import {LoadingService} from '../../shared/services/loading.service';
import {ITableData} from '../../shared/interfaces/i-table-data';
import {ConfirmationDialogService} from '../../shared/services/confirmation-dialog.service';
import {ToastService} from '../../shared/services/toast.service';
import {PCService} from '../../pages/assets/pc/pc.service';
import {AddPC, DeletePC, FetchPCs, UpdatePC} from '../actions/pc.action';

export class PCStateModel {
  data: ITableData;
}

@State<PCStateModel>({
  name: 'pcs',
  defaults: {
    data: {
      totalCount: 0,
      entries: []
    }
  }
})

export class PCState {

  constructor(private pcService: PCService,
              private loadingService: LoadingService,
              private confirmationDialogService: ConfirmationDialogService,
              private toastService: ToastService) {
  }

  @Selector()
  static getTableData(state: PCStateModel): ITableData {
    return state.data;
  }

  @Action(AddPC)
  add(ctx: StateContext<PCStateModel>, action: AddPC) {
    this.loadingService.showProgressBar();
    return this.pcService.create(action.pc).pipe(first(),
      tap(() =>
        this.toastService.open(`PC successfully created`, true, 'success', 5000)),
      map(() => ctx.dispatch(new FetchPCs())),
      finalize(() => this.loadingService.hideProgressBar()));
  }

  @Action(UpdatePC)
  update(ctx: StateContext<PCStateModel>, action: UpdatePC) {
    this.loadingService.showProgressBar();
    return this.pcService.update(action.pc).pipe(first(),
      tap(() =>
        this.toastService.open(`PC successfully updated`, true, 'success', 5000)),
      map(() => ctx.dispatch(new FetchPCs())),
      finalize(() => this.loadingService.hideProgressBar())
    );
  }

  @Action(DeletePC)
  remove(ctx: StateContext<PCStateModel>, {id}: DeletePC) {
    this.loadingService.showProgressBar();
    return this.pcService.delete(id).pipe(
      tap(() =>
        this.toastService.open(`Entry successfully deleted`, true, 'success', 5000)),
      map(() => ctx.dispatch(new FetchPCs())),
      finalize(() => this.loadingService.hideProgressBar()));
  }

  @Action(FetchPCs)
  fetch({getState, patchState}: StateContext<any>, action: FetchPCs) {
    this.loadingService.showProgressBar();
    return this.pcService.query({
      page: action.pageIndex,
      size: action.pageSize,
      sort: action.sort
    }).pipe(
      tap((_data: ITableData) => {
        patchState({
          data: _data
        });
      }),
      catchError(() => of({totalCount: 0, entries: []} as ITableData)),
      finalize(() => this.loadingService.hideProgressBar())
    );
  }
}
