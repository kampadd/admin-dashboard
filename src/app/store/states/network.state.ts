import {Action, Selector, State, StateContext} from '@ngxs/store';
import {catchError, finalize, map, tap} from 'rxjs/operators';
import {EMPTY} from 'rxjs';
import {LoadingService} from '../../shared/services/loading.service';
import {NetworkService} from '../../pages/ip-discovery/network.service';
import {Network} from '../../shared/models/network.model';
import {AddNetwork, FetchNetworks} from '../actions/network.action';
import {ToastService} from '../../shared/services/toast.service';

export class NetworkStateModel {
  networks: Network[];
}

@State<NetworkStateModel>({
  name: 'networks',
  defaults: {
    networks: []
  }
})
export class NetworkState {

  constructor(private networkService: NetworkService, private loadingService: LoadingService, private toastService: ToastService) {
  }

  @Selector()
  static getNetworks(state: NetworkStateModel): Network[] {
    return state.networks;
  }

  @Action(FetchNetworks)
  fetch({patchState}: StateContext<NetworkStateModel>) {
    return this.networkService.getAll().pipe(
      tap((networks: Network[]) => {
        patchState({
          networks: networks
        });
      }),
      catchError(() => EMPTY)
    );
  }

  @Action(AddNetwork)
  add(ctx: StateContext<NetworkStateModel>, action: AddNetwork) {
    this.loadingService.showProgressBar();
    return this.networkService.create(action.config).pipe(
      tap(() =>
        this.toastService.open(`New network successfully created`, true, 'success', 5000)),
      map(() => ctx.dispatch(new FetchNetworks())),
      finalize(() => this.loadingService.hideProgressBar())
    );
  }
}
