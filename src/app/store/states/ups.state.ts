import {Action, Selector, State, StateContext} from '@ngxs/store';
import {catchError, finalize, first, map, tap} from 'rxjs/operators';
import {of} from 'rxjs';
import {LoadingService} from '../../shared/services/loading.service';
import {ITableData} from '../../shared/interfaces/i-table-data';
import {ConfirmationDialogService} from '../../shared/services/confirmation-dialog.service';
import {ToastService} from '../../shared/services/toast.service';
import {UpsService} from '../../pages/assets/ups/ups.service';
import {AddUps, DeleteUps, FetchUps, UpdateUps} from '../actions/ups.action';

export class UpsStateModel {
  data: ITableData;
} 

@State<UpsStateModel>({
  name: 'upss',
  defaults: {
    data: {
      totalCount: 0,
      entries: []
    }
  }
})

export class UpsState {

  constructor(private upsService: UpsService,
              private loadingService: LoadingService,
              private confirmationDialogService: ConfirmationDialogService,
              private toastService: ToastService) {
  }

  @Selector()
  static getTableData(state: UpsStateModel): ITableData {
    return state.data;
  }

  @Action(AddUps)
  add(ctx: StateContext<UpsStateModel>, action: AddUps) {
    this.loadingService.showProgressBar();
    return this.upsService.create(action.ups).pipe(first(),
      tap(() =>
        this.toastService.open(`Ups successfully created`, true, 'success', 5000)),
      map(() => ctx.dispatch(new FetchUps())),
      finalize(() => this.loadingService.hideProgressBar()));
  }

  @Action(UpdateUps)
  update(ctx: StateContext<UpsStateModel>, action: UpdateUps) {
    this.loadingService.showProgressBar();
    return this.upsService.update(action.ups).pipe(first(),
      tap(() =>
        this.toastService.open(`Ups successfully updated`, true, 'success', 5000)),
      map(() => ctx.dispatch(new FetchUps())),
      finalize(() => this.loadingService.hideProgressBar())
    );
  }

  @Action(DeleteUps)
  remove(ctx: StateContext<UpsStateModel>, {id}: DeleteUps) {
    this.loadingService.showProgressBar();
    return this.upsService.delete(id).pipe(
      tap(() =>
        this.toastService.open(`Entry successfully deleted`, true, 'success', 5000)),
      map(() => ctx.dispatch(new FetchUps())),
      finalize(() => this.loadingService.hideProgressBar()));
  }

  @Action(FetchUps)
  fetch({getState, patchState}: StateContext<any>, action: FetchUps) {
    this.loadingService.showProgressBar();
    return this.upsService.query({
      page: action.pageIndex,
      size: action.pageSize,
      sort: action.sort
    }).pipe(
      tap((_data: ITableData) => {
        patchState({
          data: _data
        });
      }),
      catchError(() => of({totalCount: 0, entries: []} as ITableData)),
      finalize(() => this.loadingService.hideProgressBar())
    );
  }
}
