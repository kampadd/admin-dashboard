import {Action, Selector, State, StateContext} from '@ngxs/store';
import {catchError, finalize, first, map, tap} from 'rxjs/operators';
import {of} from 'rxjs';
import {LoadingService} from '../../shared/services/loading.service';
import {ToastService} from '../../shared/services/toast.service';
import {SnmpConfig} from '../../shared/models/snmp-config.model';
import {SnmpConfigService} from '../../pages/snmp-config/snmp-config.service';
import {AddSnmpConfig, DeleteSnmpConfig, FetchSnmpConfigs, UpdateSnmpConfig} from '../actions/snmp-config.action';
import {ITableData} from '../../shared/interfaces/i-table-data';

export class SnmpConfigStateModel {
  data: ITableData;
}

@State<SnmpConfigStateModel>({
  name: 'snmpConfig',
  defaults: {
    data: {
      totalCount: 0,
      entries: []
    }
  }
})
export class SnmpConfigState {

  constructor(private snmpConfigService: SnmpConfigService, private loadingService: LoadingService,
              private toastService: ToastService) {
  }

  @Selector()
  static getTableData(state: SnmpConfigStateModel): ITableData {
    return state.data;
  }

  @Selector()
  static getConfigs(state: SnmpConfigStateModel): SnmpConfig[] {
    return state.data.entries;
  }

  @Action(FetchSnmpConfigs)
  fetch({getState, patchState}: StateContext<SnmpConfigStateModel>, action: FetchSnmpConfigs) {
    this.loadingService.showProgressBar();
    return this.snmpConfigService.query({
      page: action.pageIndex,
      size: action.pageSize,
      sort: action.sort
    }).pipe(
      tap((_data: ITableData) => {
        patchState({
          data: _data
        });
      }),
      catchError(() => of({totalCount: 0, entries: []} as ITableData)),
      finalize(() => this.loadingService.hideProgressBar())
    );
  }

  @Action(AddSnmpConfig)
  add(ctx: StateContext<SnmpConfigStateModel>, action: AddSnmpConfig) {
    this.loadingService.showProgressBar();
    return this.snmpConfigService.create(action.config).pipe(first(),
      tap(() =>
        this.toastService.open(`Config successfully created`, true, 'success', 5000)),
      map(() => ctx.dispatch(new FetchSnmpConfigs())),
      finalize(() => this.loadingService.hideProgressBar()));
  }

  @Action(UpdateSnmpConfig)
  update(ctx: StateContext<SnmpConfigStateModel>, action: UpdateSnmpConfig) {
    this.loadingService.showProgressBar();
    return this.snmpConfigService.update(action.config).pipe(first(),
      tap(() =>
        this.toastService.open(`Config successfully updated`, true, 'success', 5000)),
      map(() => ctx.dispatch(new FetchSnmpConfigs())),
      finalize(() => this.loadingService.hideProgressBar())
    );
  }

  @Action(DeleteSnmpConfig)
  remove(ctx: StateContext<SnmpConfigStateModel>, {id}: DeleteSnmpConfig) {
    this.loadingService.showProgressBar();
    return this.snmpConfigService.delete(id).pipe(
      tap(() =>
        this.toastService.open(`Entry successfully deleted`, true, 'success', 5000)),
      map(() => ctx.dispatch(new FetchSnmpConfigs())),
      finalize(() => this.loadingService.hideProgressBar()));
  }
}
