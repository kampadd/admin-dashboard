import {Action, Selector, State, StateContext} from '@ngxs/store';
import {catchError, finalize, map, tap} from 'rxjs/operators';
import {EMPTY} from 'rxjs';
import {Minion} from '../../shared/models/minion.model';
import {DeleteMinion, FetchMinions, UpdateMinion} from '../actions/minion.action';
import {MinionService} from '../../pages/cockpit/minion.service';
import {LoadingService} from '../../shared/services/loading.service';
import {ToastService} from '../../shared/services/toast.service';

export class MinionStateModel {
  minions: Minion[];
}

@State<MinionStateModel>({
  name: 'minions',
  defaults: {
    minions: []
  }
})
export class MinionState {

  constructor(private minionService: MinionService, private loadingService: LoadingService,
              private toastService: ToastService) {
  }

  @Selector()
  static getMinions(state: MinionStateModel): Minion[] {
    return state.minions;
  }

  @Action(FetchMinions)
  fetch({patchState}: StateContext<any>) {
    return this.minionService.getAll().pipe(
      tap((minions: Minion[]) => {
        patchState({
          minions: minions
        });
      }),
      catchError(() => EMPTY)
    );
  }

  @Action(UpdateMinion)
  update(ctx: StateContext<MinionStateModel>, action: UpdateMinion) {
    this.loadingService.showProgressBar();
    return this.minionService.update(action.minion).pipe(
      tap(() =>
        this.toastService.open(`Minion successfully updated`, true, 'success', 5000)),
      map(() => ctx.dispatch(new FetchMinions())),
      finalize(() => this.loadingService.hideProgressBar())
    );
  }

  @Action(DeleteMinion)
  remove(ctx: StateContext<MinionStateModel>, {id}: DeleteMinion) {
    this.loadingService.showProgressBar();
    return this.minionService.delete(id).pipe(
      tap(() =>
        this.toastService.open(`Entry successfully deleted`, true, 'success', 5000)),
      map(() => ctx.dispatch(new FetchMinions())),
      finalize(() => this.loadingService.hideProgressBar()));
  }
}
