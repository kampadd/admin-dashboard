import {Action, Selector, State, StateContext} from '@ngxs/store';
import {catchError, finalize, first, map, tap} from 'rxjs/operators';
import {of} from 'rxjs';
import {LoadingService} from '../../shared/services/loading.service';
import {ITableData} from '../../shared/interfaces/i-table-data';
import {ConfirmationDialogService} from '../../shared/services/confirmation-dialog.service';
import {ToastService} from '../../shared/services/toast.service';
import {NotebookService} from '../../pages/assets/notebook/notebook.service';
import {AddNotebook, DeleteNotebook, FetchNotebooks, UpdateNotebook} from '../actions/notebook.action';

export class NotebookStateModel {
  data: ITableData;
} 

@State<NotebookStateModel>({
  name: 'notebooks',
  defaults: {
    data: {
      totalCount: 0,
      entries: []
    }
  }
})

export class NotebookState {

  constructor(private notebookService: NotebookService,
              private loadingService: LoadingService,
              private confirmationDialogService: ConfirmationDialogService,
              private toastService: ToastService) {
  }

  @Selector()
  static getTableData(state: NotebookStateModel): ITableData {
    return state.data;
  }

  @Action(AddNotebook)
  add(ctx: StateContext<NotebookStateModel>, action: AddNotebook) {
    this.loadingService.showProgressBar();
    return this.notebookService.create(action.notebook).pipe(first(),
      tap(() =>
        this.toastService.open(`Notebook successfully created`, true, 'success', 5000)),
      map(() => ctx.dispatch(new FetchNotebooks())),
      finalize(() => this.loadingService.hideProgressBar()));
  }

  @Action(UpdateNotebook)
  update(ctx: StateContext<NotebookStateModel>, action: UpdateNotebook) {
    this.loadingService.showProgressBar();
    return this.notebookService.update(action.notebook).pipe(first(),
      tap(() =>
        this.toastService.open(`Notebook successfully updated`, true, 'success', 5000)),
      map(() => ctx.dispatch(new FetchNotebooks())),
      finalize(() => this.loadingService.hideProgressBar())
    );
  }

  @Action(DeleteNotebook)
  remove(ctx: StateContext<NotebookStateModel>, {id}: DeleteNotebook) {
    this.loadingService.showProgressBar();
    return this.notebookService.delete(id).pipe(
      tap(() =>
        this.toastService.open(`Entry successfully deleted`, true, 'success', 5000)),
      map(() => ctx.dispatch(new FetchNotebooks())),
      finalize(() => this.loadingService.hideProgressBar()));
  }

  @Action(FetchNotebooks)
  fetch({getState, patchState}: StateContext<any>, action: FetchNotebooks) {
    this.loadingService.showProgressBar();
    return this.notebookService.query({
      page: action.pageIndex,
      size: action.pageSize,
      sort: action.sort
    }).pipe(
      tap((_data: ITableData) => {
        patchState({
          data: _data
        });
      }),
      catchError(() => of({totalCount: 0, entries: []} as ITableData)),
      finalize(() => this.loadingService.hideProgressBar())
    );
  }
}
