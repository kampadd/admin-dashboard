import {Action, Selector, State, StateContext} from '@ngxs/store';
import {tap} from 'rxjs/operators';
import {UserService} from '../../pages/user/user.service';
import {FetchAuthorities} from '../actions/authority.action';

export class AuthorityStateModel {
  authorities: string[];
}

@State<AuthorityStateModel>({
  name: 'authority',
  defaults: {
    authorities: []
  }
})
export class AuthorityState {

  constructor(private userService: UserService) {
  }

  @Selector()
  static getAuthorities(state: AuthorityStateModel): string[] {
    return state.authorities;
  }

  @Action(FetchAuthorities)
  fetch({patchState}: StateContext<AuthorityStateModel>) {
    return this.userService.authorities().pipe(
      tap((authorities: string[]) => {
        patchState({
          authorities: authorities
        });
      }));
  }
}

