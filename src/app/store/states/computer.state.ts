import {Action, Selector, State, StateContext} from '@ngxs/store';
import {catchError, finalize, first, map, tap} from 'rxjs/operators';
import {of} from 'rxjs';
import {LoadingService} from '../../shared/services/loading.service';
import {ITableData} from '../../shared/interfaces/i-table-data';
import {ConfirmationDialogService} from '../../shared/services/confirmation-dialog.service';
import {ToastService} from '../../shared/services/toast.service';
import {ComputerService} from '../../pages/assets/computer/computer.service';
import {AddComputer, DeleteComputer, FetchComputers, UpdateComputer} from '../actions/computer.action';

export class ComputerStateModel {
  data: ITableData;
} 

@State<ComputerStateModel>({
  name: 'computers',
  defaults: {
    data: {
      totalCount: 0,
      entries: []
    }
  }
})

export class ComputerState {

  constructor(private computerService: ComputerService,
              private loadingService: LoadingService,
              private confirmationDialogService: ConfirmationDialogService,
              private toastService: ToastService) {
  }

  @Selector()
  static getTableData(state: ComputerStateModel): ITableData {
    return state.data;
  }

  @Action(AddComputer)
  add(ctx: StateContext<ComputerStateModel>, action: AddComputer) {
    this.loadingService.showProgressBar();
    return this.computerService.create(action.computer).pipe(first(),
      tap(() =>
        this.toastService.open(`Computer successfully created`, true, 'success', 5000)),
      map(() => ctx.dispatch(new FetchComputers())),
      finalize(() => this.loadingService.hideProgressBar()));
  }

  @Action(UpdateComputer)
  update(ctx: StateContext<ComputerStateModel>, action: UpdateComputer) {
    this.loadingService.showProgressBar();
    return this.computerService.update(action.computer).pipe(first(),
      tap(() =>
        this.toastService.open(`Computer successfully updated`, true, 'success', 5000)),
      map(() => ctx.dispatch(new FetchComputers())),
      finalize(() => this.loadingService.hideProgressBar())
    );
  }

  @Action(DeleteComputer)
  remove(ctx: StateContext<ComputerStateModel>, {id}: DeleteComputer) {
    this.loadingService.showProgressBar();
    return this.computerService.delete(id).pipe(
      tap(() =>
        this.toastService.open(`Entry successfully deleted`, true, 'success', 5000)),
      map(() => ctx.dispatch(new FetchComputers())),
      finalize(() => this.loadingService.hideProgressBar()));
  }

  @Action(FetchComputers)
  fetch({getState, patchState}: StateContext<any>, action: FetchComputers) {
    this.loadingService.showProgressBar();
    return this.computerService.query({
      page: action.pageIndex,
      size: action.pageSize,
      sort: action.sort
    }).pipe(
      tap((_data: ITableData) => {
        patchState({
          data: _data
        });
      }),
      catchError(() => of({totalCount: 0, entries: []} as ITableData)),
      finalize(() => this.loadingService.hideProgressBar())
    );
  }
}
