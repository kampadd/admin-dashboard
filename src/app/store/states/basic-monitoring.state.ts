import {Action, Selector, State, StateContext} from '@ngxs/store';
import {catchError, finalize, first, map, tap} from 'rxjs/operators';
import {of} from 'rxjs';
import {LoadingService} from '../../shared/services/loading.service';
import {ToastService} from '../../shared/services/toast.service';
import {ITableData} from '../../shared/interfaces/i-table-data';
import {BasicMonitoringService} from '../../pages/basic-monitoring/basic-monitoring.service';
import {
  AddBasicMonitoring,
  DeleteBasicMonitoring,
  FetchBasicMonitoringList,
  UpdateBasicMonitoring
} from '../actions/basic-monitoring.action';

export class BasicMonitoringStateModel {
  data: ITableData;
}

@State<BasicMonitoringStateModel>({
  name: 'basicMonitoring',
  defaults: {
    data: {
      totalCount: 0,
      entries: []
    }
  }
})
export class BasicMonitoringState {

  constructor(private basicMonitoringService: BasicMonitoringService, private loadingService: LoadingService,
              private toastService: ToastService) {
  }

  @Selector()
  static getTableData(state: BasicMonitoringStateModel): ITableData {
    return state.data;
  }

  @Action(FetchBasicMonitoringList)
  fetch({getState, patchState}: StateContext<BasicMonitoringStateModel>, action: FetchBasicMonitoringList) {
    this.loadingService.showProgressBar();
    return this.basicMonitoringService.query({
      page: action.pageIndex,
      size: action.pageSize,
      sort: action.sort
    }).pipe(
      tap((_data: ITableData) => {
        patchState({
          data: _data
        });
      }),
      catchError(() => of({totalCount: 0, entries: []} as ITableData)),
      finalize(() => this.loadingService.hideProgressBar())
    );
  }

  @Action(AddBasicMonitoring)
  add(ctx: StateContext<BasicMonitoringStateModel>, {config}: AddBasicMonitoring) {
    this.loadingService.showProgressBar();
    return this.basicMonitoringService.create(config).pipe(first(),
      tap(() =>
        this.toastService.open(`Config successfully created`, true, 'success', 5000)),
      map(() => ctx.dispatch(new FetchBasicMonitoringList())),
      finalize(() => this.loadingService.hideProgressBar()));
  }

  @Action(UpdateBasicMonitoring)
  update(ctx: StateContext<BasicMonitoringStateModel>, {config}: UpdateBasicMonitoring) {
    this.loadingService.showProgressBar();
    return this.basicMonitoringService.update(config).pipe(first(),
      tap(() =>
        this.toastService.open(`Config successfully updated`, true, 'success', 5000)),
      map(() => ctx.dispatch(new FetchBasicMonitoringList())),
      finalize(() => this.loadingService.hideProgressBar())
    );
  }

  @Action(DeleteBasicMonitoring)
  remove(ctx: StateContext<BasicMonitoringStateModel>, {id}: DeleteBasicMonitoring) {
    this.loadingService.showProgressBar();
    return this.basicMonitoringService.delete(id).pipe(
      tap(() =>
        this.toastService.open(`Entry successfully deleted`, true, 'success', 5000)),
      map(() => ctx.dispatch(new FetchBasicMonitoringList())),
      finalize(() => this.loadingService.hideProgressBar()));
  }
}
