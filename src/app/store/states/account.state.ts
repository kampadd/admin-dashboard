import {Action, Selector, State, StateContext} from '@ngxs/store';
import {finalize, first, tap} from 'rxjs/operators';
import {LoadingService} from '../../shared/services/loading.service';
import {Account} from '../../shared/models/account.model';
import {AccountService} from '../../core/auth/account.service';
import {ChangePassword, FetchAccount, Login, Logout, ResetFinishPassword, ResetInitPassword} from '../actions/account.action';
import {AuthService} from '../../core/auth/auth.service';
import {AppRoles} from '../../shared/enums/roles.enum';

export class AccountStateModel {
  account: Account;
  isAdmin: boolean;
}

@State<AccountStateModel>({
  name: 'account',
  defaults: {
    account: null,
    isAdmin: false
  }
})
export class AccountState {

  constructor(private accountService: AccountService,
              private authService: AuthService,
              private loadingService: LoadingService) {
  }

  @Selector()
  static getAccount(state: AccountStateModel): Account {
    return state.account;
  }

  @Selector()
  static isAdmin(state: AccountStateModel): boolean {
    return state.isAdmin;
  }

  @Action(Login)
  login(ctx: StateContext<AccountStateModel>, action: Login) {
    this.loadingService.showProgressBar();
    return this.authService.login(action.credentials).pipe(
      tap(() => ctx.dispatch(new FetchAccount())),
      finalize(() => this.loadingService.hideProgressBar()));
  }

  @Action(FetchAccount)
  account({getState, patchState}: StateContext<AccountStateModel>) {
    this.loadingService.showProgressBar();
    return this.accountService.getAccount().pipe(
      tap((account: Account) => {
        patchState({
          ...getState(),
          account: account,
          isAdmin: account.authorities.includes(AppRoles.ADMIN)
        });
      }),
      finalize(() => this.loadingService.hideProgressBar())
    );
  }

  @Action(Logout)
  logout({patchState}: StateContext<AccountStateModel>) {
    this.loadingService.showProgressBar();
    return this.authService.logout().pipe(first(),
      tap(() => {
        patchState({
          account: null,
          isAdmin: false
        });
      }),
      finalize(() => this.loadingService.hideProgressBar())
    );
  }

  @Action(ChangePassword)
  changePassword(ctx: StateContext<AccountStateModel>, action: ChangePassword) {
    this.loadingService.showProgressBar();
    return this.accountService.changePassword(action.passwords).pipe(first(),
      tap(() => ctx.dispatch(new FetchAccount())),
      finalize(() => this.loadingService.hideProgressBar())
    );
  }

  @Action(ResetInitPassword)
  resetInitPassword(ctx: StateContext<AccountStateModel>, {email}: ResetInitPassword) {
    this.loadingService.showProgressBar();
    return this.accountService.resetPasswordInit(email).pipe(first(),
      finalize(() => this.loadingService.hideProgressBar())
    );
  }

  @Action(ResetFinishPassword)
  resetFinishPassword(ctx: StateContext<AccountStateModel>, action: ResetFinishPassword) {
    this.loadingService.showProgressBar();
    return this.accountService.resetPasswordFinish(action.keyAndPassword).pipe(first(),
      finalize(() => this.loadingService.hideProgressBar())
    );
  }
}
