import {IAsset} from '../../shared/models/asset.model';

export class AddAsset {
  static readonly type = '[ASSET] Add';

  constructor(public asset: IAsset) {
  }
}

export class UpdateAsset {
  static readonly type = '[ASSET] Update';

  constructor(public asset: IAsset) {
  }
}

export class DeleteAsset {
  static readonly type = '[ASSET] Delete';

  constructor(public id: string) {
  }
}

export class FetchAssets {
  static readonly type = '[ASSET] List';

  constructor(public sort: string[] = [], public pageIndex = 0, public pageSize = 10) {
  }
}
