import {Minion} from '../../shared/models/minion.model';

export class FetchMinions {
  static readonly type = '[MINION] List';

  constructor() {
  }
}

export class UpdateMinion {
  static readonly type = '[MINION] Update';

  constructor(public minion: Minion) {
  }
}

export class DeleteMinion {
  static readonly type = '[MINION] Delete';

  constructor(public id: string) {
  }
}
