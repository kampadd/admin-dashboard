import {IUPS} from '../../shared/models/ups.model';

export class AddUps {
  static readonly type = '[UPS] Add';

  constructor(public ups: IUPS) {
  }
}

export class UpdateUps {
  static readonly type = '[UPS] Update';

  constructor(public ups: IUPS) {
  }
}

export class DeleteUps {
  static readonly type = '[UPS] Delete';

  constructor(public id: string) {
  }
}

export class FetchUps {
    static readonly type = '[UPS] List';
  
    constructor(public sort: string[] = [], public pageIndex = 0, public pageSize = 10) {
    }
  }
  