import {Network} from '../../shared/models/network.model';

export class AddNetwork {
  static readonly type = '[NETWORK] Add';

  constructor(public config: Network) {
  }
}

export class FetchNetworks {
  static readonly type = '[NETWORK] List';

  constructor() {
  }
}
