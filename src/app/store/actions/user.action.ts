import {IUser} from '../../shared/models/user.model';

export class AddUser {
  static readonly type = '[USER] Add';

  constructor(public user: IUser) {
  }
}

export class UpdateUser {
  static readonly type = '[USER] Update';

  constructor(public user: IUser) {
  }
}

export class DeleteUser {
  static readonly type = '[USER] Delete';

  constructor(public id: string) {
  }
}

export class FetchListUserData {
  static readonly type = '[USER] List';

  constructor(public sort: string[] = [], public pageIndex = 0, public pageSize = 10) {
  }
}
