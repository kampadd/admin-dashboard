import {IPC} from '../../shared/models/pc.model';

export class AddPC {
  static readonly type = '[PC] Add';

  constructor(public pc: IPC) {
  }
}

export class UpdatePC {
  static readonly type = '[PC] Update';

  constructor(public pc: IPC) {
  }
}

export class DeletePC {
  static readonly type = '[PC] Delete';

  constructor(public id: string) {
  }
}

export class FetchPCs {
    static readonly type = '[PC] List';
  
    constructor(public sort: string[] = [], public pageIndex = 0, public pageSize = 10) {
    }
  }
  