import {IConfigItem} from '../../shared/models/config-item.model';

export class AddConfigItem {
  static readonly type = '[CONFIG_ITEM] Add';

  constructor(public configItem: IConfigItem) {
  }
}

export class UpdateConfigItem {
  static readonly type = '[CONFIG_ITEM] Update';

  constructor(public configItem: IConfigItem) {
  }
}

export class DeleteConfigItem {
  static readonly type = '[CONFIG_ITEM] Delete';

  constructor(public id: string) {
  }
}
 
export class FetchConfigItems {
  static readonly type = '[CONFIG_ITEM] List';

  constructor(public sort: string[] = [], public pageIndex = 0, public pageSize = 10) {
  }
}
