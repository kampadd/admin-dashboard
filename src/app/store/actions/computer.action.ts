import {IComputer} from '../../shared/models/computer.model';

export class AddComputer {
  static readonly type = '[COMPUTER] Add';

  constructor(public computer: IComputer) {
  }
}

export class UpdateComputer {
  static readonly type = '[COMPUTER] Update';

  constructor(public computer: IComputer) {
  }
}

export class DeleteComputer {
  static readonly type = '[COMPUTER] Delete';

  constructor(public id: string) {
  }
}
 
export class FetchComputers {
  static readonly type = '[COMPUTER] List';

  constructor(public sort: string[] = [], public pageIndex = 0, public pageSize = 10) {
  }
}
