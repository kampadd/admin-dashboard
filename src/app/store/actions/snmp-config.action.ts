import {SnmpConfig} from '../../shared/models/snmp-config.model';

export class FetchSnmpConfigs {
  static readonly type = '[SNMP] List';

  constructor(public sort: string[] = [], public pageIndex = 0, public pageSize = 10) {
  }
}

export class AddSnmpConfig {
  static readonly type = '[SNMP] Add';

  constructor(public config: SnmpConfig) {
  }
}

export class UpdateSnmpConfig {
  static readonly type = '[SNMP] Update';

constructor(public config: SnmpConfig) {
  }
}

export class DeleteSnmpConfig {
  static readonly type = '[SNMP] Delete';

  constructor(public id: string) {
  }
}
