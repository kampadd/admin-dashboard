import {BasicMonitoring} from '../../shared/models/basic-monitoring.model';

export class FetchBasicMonitoringList {
  static readonly type = '[BASIC MONITORING] List';

  constructor(public sort: string[] = [], public pageIndex = 0, public pageSize = 10) {
  }
}

export class AddBasicMonitoring {
  static readonly type = '[BASIC MONITORING] Add';

  constructor(public config: BasicMonitoring) {
  }
}

export class UpdateBasicMonitoring {
  static readonly type = '[BASIC MONITORING] Update';

  constructor(public config: BasicMonitoring) {
  }
}

export class DeleteBasicMonitoring {
  static readonly type = '[BASIC MONITORING] Delete';

  constructor(public id: string) {
  }
}
