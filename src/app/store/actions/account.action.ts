import {ICredentials} from '../../shared/interfaces/i-credentials';
import {IPasswords} from '../../shared/interfaces/i-passwords';
import {IPasswordKey} from '../../shared/interfaces/i-password-key';

export class Login {
  static readonly type = '[ACCOUNT] Login';

  constructor(public credentials: ICredentials) {
  }
}

export class FetchAccount {
  static readonly type = '[ACCOUNT] Get';
}

export class Logout {
  static readonly type = '[ACCOUNT] Logout';
}

export class ChangePassword {
  static readonly type = '[ACCOUNT] Change Password';

  constructor(public passwords: IPasswords) {
  }
}

export class ResetInitPassword {
  static readonly type = '[ACCOUNT] Reset Password';

  constructor(public email: string) {
  }
}

export class ResetFinishPassword {
  static readonly type = '[ACCOUNT] Reset Finish Password';

  constructor(public keyAndPassword: IPasswordKey) {
  }
}
