import {INotebook} from '../../shared/models/notebook.model';

export class AddNotebook {
  static readonly type = '[NOTEBOOK] Add';

  constructor(public notebook: INotebook) {
  }
}

export class UpdateNotebook {
  static readonly type = '[NOTEBOOK] Update';

  constructor(public notebook: INotebook) {
  }
}

export class DeleteNotebook {
  static readonly type = '[NOTEBOOK] Delete';

  constructor(public id: string) {
  }
}

export class FetchNotebooks {
    static readonly type = '[NOTEBOOK] List';
  
    constructor(public sort: string[] = [], public pageIndex = 0, public pageSize = 10) {
    }
  }
  