import {IMonitor} from '../../shared/models/monitor.model';

export class AddMonitor {
  static readonly type = '[MONITOR] Add';

  constructor(public monitor: IMonitor) {
  }
}

export class UpdateMonitor {
  static readonly type = '[MONITOR] Update';

  constructor(public monitor: IMonitor) {
  }
}

export class DeleteMonitor {
  static readonly type = '[MONITOR] Delete';

  constructor(public id: string) {
  }
}
 
export class FetchMonitors {
  static readonly type = '[MONITOR] List';

  constructor(public sort: string[] = [], public pageIndex = 0, public pageSize = 10) {
  }
}