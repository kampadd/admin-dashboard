import {ILicense} from '../../shared/models/license.model';

export class AddLicense {
  static readonly type = '[LICENSE] Add';

  constructor(public license: ILicense) {
  }
}

export class UpdateLicense {
  static readonly type = '[LICENSE] Update';

  constructor(public license: ILicense) {
  }
}

export class DeleteLicense {
  static readonly type = '[LICENSE] Delete';

  constructor(public id: string) {
  }
}
 
export class FetchLicenses {
  static readonly type = '[LICENSE] List';

  constructor(public sort: string[] = [], public pageIndex = 0, public pageSize = 10) {
  }
}
