import {Cockpit} from '../../shared/models/cockpit.model';

export class FetchCockpit {
  static readonly type = '[Cockpit] Fetch';
}

export class UpdateCockpit {
  static readonly type = '[Cockpit] Edit';

  public constructor(public cockpit: Cockpit) {
  }
}
