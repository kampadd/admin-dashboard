import {IpDiscoveryConfig} from '../../shared/models/ip-discovery.model';

export class AddIpDiscoveryConfig {
  static readonly type = '[IP_DISCOVERY] Add';

  constructor(public config: IpDiscoveryConfig) {
  }
}

export class UpdateIpDiscoveryConfig {
  static readonly type = '[IP_DISCOVERY] Update';

  constructor(public config: IpDiscoveryConfig) {
  }
}


export class DeleteIpDiscoveryConfig {
  static readonly type = '[IP_DISCOVERY] Delete';

  constructor(public id: string) {
  }
}

export class FetchIpDiscoveryListData {
  static readonly type = '[IP_DISCOVERY] List';

  constructor(public pageIndex = 0, public pageSize = 10) {
  }
}
