import {IPrinter} from '../../shared/models/printer.model';

export class AddPrinter {
  static readonly type = '[PRINTER] Add';

  constructor(public printer: IPrinter) {
  }
}

export class UpdatePrinter {
  static readonly type = '[PRINTER] Update';

  constructor(public printer: IPrinter) {
  }
}

export class DeletePrinter {
  static readonly type = '[PRINTER] Delete';

  constructor(public id: string) {
  }
}

export class FetchPrinters {
    static readonly type = '[PRINTER] List';
  
    constructor(public sort: string[] = [], public pageIndex = 0, public pageSize = 10) {
    }
  }
  