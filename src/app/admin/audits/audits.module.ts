import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AuditsRoutingModule} from './audits.routing-module';
import {AuditsComponent} from './audits.component';
import {TranslateModule} from '@ngx-translate/core';
import {
  MatButtonModule,
  MatCardModule,
  MatDatepickerModule,
  MatIconModule,
  MatInputModule,
  MatPaginatorModule,
  MatSortModule,
  MatTableModule
} from '@angular/material';
import {MatMomentDateModule} from '@angular/material-moment-adapter';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';


@NgModule({
  declarations: [AuditsComponent],
  imports: [
    CommonModule,
    AuditsRoutingModule,
    TranslateModule,
    MatButtonModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatIconModule,
    MatInputModule,
    MatDatepickerModule,
    MatMomentDateModule,
    MatCardModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class AuditsModule {
}
