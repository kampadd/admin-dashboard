import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';

import {Audit} from './audit.model';
import {createRequestOption} from '../../shared/utils/request-util';
import {ITableData} from '../../shared/interfaces/i-table-data';
import {map} from 'rxjs/operators';
import {UAA_SERVER_API} from '../../app.constants';

@Injectable({providedIn: 'root'})
export class AuditsService {
  constructor(private http: HttpClient) {
  }

  query(req?: any): Observable<ITableData> {
    const params = createRequestOption(req);
    params.set('fromDate', req.fromDate);
    params.set('toDate', req.toDate);

    const requestURL = UAA_SERVER_API + '/management/audits';

    return this.http.get<Audit[]>(requestURL, {params: params, observe: 'response'}).pipe(
      map((res: HttpResponse<Audit[]>) => {
        return {
          totalCount: parseInt(res.headers.get('X-Total-Count'), 10),
          entries: res.body
        };
      })
    );
  }
}
