import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';

import {AuditsService} from './audits.service';
import {first, mergeMap, tap} from 'rxjs/operators';
import {ITableData} from '../../shared/interfaces/i-table-data';
import {merge, Observable, Subscription} from 'rxjs';
import {MAT_DATE_FORMATS, MatPaginator, MatSort} from '@angular/material';
import {TableDataSource} from '../../shared/utils/table-datasource';
import {DATE_FORMAT, DD_MM_YYYY_Format} from '../../shared/utils/constants';
import {FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-audit',
  templateUrl: './audits.component.html',
  providers: [{provide: MAT_DATE_FORMATS, useValue: DD_MM_YYYY_Format}]
})
export class AuditsComponent implements OnInit, OnDestroy, AfterViewInit {

  private subscription: Subscription;
  private sortSubscription: Subscription;
  private filterSubscription: Subscription;
  public displayedColumns: string[] = ['timestamp', 'principal', 'type', 'data'];
  public dataSource = new TableDataSource();
  fromDateForm = new FormControl('', [Validators.required]);
  toDateForm = new FormControl('', [Validators.required]);

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private auditsService: AuditsService) {
  }

  ngOnInit() {
    this.loadData().pipe(first()).subscribe();
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.sortSubscription) {
      this.sortSubscription.unsubscribe();
    }
    if (this.filterSubscription) {
      this.filterSubscription.unsubscribe();
    }
  }

  ngAfterViewInit() {
    // reset the paginator after sorting
    this.sortSubscription = this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    this.subscription = merge(this.sort.sortChange, this.paginator.page).pipe(
      mergeMap(() => this.loadData([this.sort.active + ',' + this.sort.direction], this.paginator.pageIndex, this.paginator.pageSize)))
      .subscribe();

    this.changeFilters();
  }

  changeFilters(): void {
    this.filterSubscription = merge(this.fromDateForm.valueChanges, this.toDateForm.valueChanges).pipe(
      mergeMap(() => this.loadActive())).subscribe();
  }

  private loadActive(): Observable<any> {
    return this.loadData([this.sort.active + ',' + this.sort.direction], this.paginator.pageIndex, this.paginator.pageSize);
  }

  private loadData(sort = ['auditEventDate', 'desc'], index = 0, size = 10): Observable<any> {
    const fromDate = this.fromDateForm.value;
    const toDate = this.toDateForm.value;
    return this.auditsService.query({
      sort: sort,
      page: index,
      size: size,
      fromDate: fromDate && fromDate.isValid() ? fromDate.format(DATE_FORMAT) : null,
      toDate: toDate && toDate.isValid() ? toDate.format(DATE_FORMAT) : null
    }).pipe(tap((_data: ITableData) => this.dataSource.update(_data)));
  }
}
