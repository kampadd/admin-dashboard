import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuditsComponent} from './audits.component';

export const routes: Routes = [
  {
    path: '',
    component: AuditsComponent,
    data: {
      defaultSort: 'auditEventDate,desc'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuditsRoutingModule {
}

