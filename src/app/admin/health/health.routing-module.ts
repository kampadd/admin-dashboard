import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HealthComponent} from './health.component';

export const routes: Routes = [
  {
    path: '',
    component: HealthComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HealthRoutingModule {
}

