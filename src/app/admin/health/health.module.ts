import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TranslateModule} from '@ngx-translate/core';
import {MatButtonModule, MatCardModule, MatChipsModule, MatDialogModule, MatIconModule, MatTableModule} from '@angular/material';
import {HealthComponent} from './health.component';
import {HealthRoutingModule} from './health.routing-module';
import {HealthModalComponent} from './health-modal.component';

@NgModule({
  declarations: [HealthComponent, HealthModalComponent],
  imports: [
    CommonModule,
    HealthRoutingModule,
    TranslateModule,
    MatButtonModule,
    MatTableModule,
    MatIconModule,
    MatCardModule,
    MatChipsModule,
    MatDialogModule
  ],
  entryComponents: [HealthModalComponent]
})
export class HealthModule {
}
