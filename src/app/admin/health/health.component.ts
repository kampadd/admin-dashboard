import {Component, OnInit} from '@angular/core';
import {HealthService} from './health.service';
import {catchError, finalize, first, tap} from 'rxjs/operators';
import {EMPTY} from 'rxjs';
import {MatDialog} from '@angular/material';
import {HealthModalComponent} from './health-modal.component';

@Component({
  selector: 'app-health',
  templateUrl: './health.component.html'
})
export class HealthComponent implements OnInit {
  dataSource: any;
  updatingHealth: boolean;
  displayedColumns = ['service', 'status', 'details'];

  constructor(private healthService: HealthService, private dialog: MatDialog) {
  }

  ngOnInit() {
    this.refresh();
  }

  baseName(name: string) {
    return this.healthService.getBaseName(name);
  }

  getStatusColor(statusState) {
    if (statusState !== 'UP') {
      return 'warn';
    }
  }

  refresh() {
    this.updatingHealth = true;

    this.healthService.checkHealth().pipe(first(),
      tap(data => this.dataSource = this.healthService.transformHealthData(data)),
      catchError(error => {
        if (error.status === 503) {
          this.dataSource = this.healthService.transformHealthData(error.error);
          return EMPTY;
        }
      }),
      finalize(() => this.updatingHealth = false)
    ).subscribe();
  }

  showHealth(health: any) {
    setTimeout(() => {
      this.dialog.open(HealthModalComponent, {
        minWidth: '50vw',
        maxWidth: '95vw',
        maxHeight: '95vh',
        data: health
      });
    });
  }

  subSystemName(name: string) {
    return this.healthService.getSubSystemName(name);
  }
}
