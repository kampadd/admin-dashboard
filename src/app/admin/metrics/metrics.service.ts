import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {GATEWAY_SERVER_API} from '../../app.constants';


@Injectable({providedIn: 'root'})
export class MetricsService {
  constructor(private http: HttpClient) {
  }

  getMetrics(): Observable<any> {
    return this.http.get(GATEWAY_SERVER_API + '/management/jhimetrics');
  }

  threadDump(): Observable<any> {
    return this.http.get(GATEWAY_SERVER_API + '/management/threaddump');
  }
}
