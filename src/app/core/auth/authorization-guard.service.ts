import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {Store} from '@ngxs/store';
import {AccountState} from '../../store/states/account.state';
import {Account} from '../../shared/models/account.model';
import {map} from 'rxjs/operators';

@Injectable({providedIn: 'root'})
export class AuthorizationGuard implements CanActivate {

  constructor(private store: Store) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.store.select(AccountState.getAccount).pipe(
      map((account: Account) =>
        account.authorities.some(authority => (route.data.roles || []).includes(authority))));
  }
}
