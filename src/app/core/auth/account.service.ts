import {Account} from '../../shared/models/account.model';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {UAA_SERVER_API} from 'src/app/app.constants';
import {IPasswords} from '../../shared/interfaces/i-passwords';
import {IPasswordKey} from '../../shared/interfaces/i-password-key';

@Injectable({providedIn: 'root'})
export class AccountService {
  private resourceUrl = UAA_SERVER_API + '/api/account';

  constructor(private http: HttpClient) {
  }

  getAccount(): Observable<Account> {
    return this.http.get<Account>(this.resourceUrl);
  }

  saveAccount(account: Account): Observable<Account> {
    return this.http.post<Account>(this.resourceUrl, account);
  }

  changePassword(passwords: IPasswords): Observable<IPasswords> {
    return this.http.post<IPasswords>(this.resourceUrl + '/change-password', passwords);
  }

  resetPasswordFinish(keyAndPassword: IPasswordKey): Observable<Account> {
    return this.http.post<Account>(this.resourceUrl + '/reset-password/finish', keyAndPassword);
  }

  resetPasswordInit(email: string): Observable<Account> {
    return this.http.post<Account>(this.resourceUrl + '/reset-password/init', email);
  }
}
