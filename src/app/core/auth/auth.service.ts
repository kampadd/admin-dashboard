import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {ICredentials} from '../../shared/interfaces/i-credentials';
import {GATEWAY_SERVER_API} from '../../app.constants';
import {Account} from '../../shared/models/account.model';

@Injectable({providedIn: 'root'})
export class AuthService {

  constructor(private http: HttpClient) {
  }

  login(credentials: ICredentials): Observable<void> {
    return this.http.post<void>(GATEWAY_SERVER_API + '/auth/login', credentials);
  }

  register(account: Account): Observable<Account> {
    return this.http.post<Account>(GATEWAY_SERVER_API + '/api/register', account);
  }

  logout(): Observable<any> {
    return this.http.post(GATEWAY_SERVER_API + '/auth/logout', null);
  }
}
