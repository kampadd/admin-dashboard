import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable, of} from 'rxjs';
import {mergeMap} from 'rxjs/operators';
import {Store} from '@ngxs/store';
import {FetchAccount} from '../../store/actions/account.action';

@Injectable({providedIn: 'root'})
export class AuthenticationGuard implements CanActivate {

  constructor(private store: Store, private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.store.dispatch(new FetchAccount()).pipe(
      mergeMap(account => {
        if (!account) {
          this.router.navigate(['/auth/login']);
          return of(false);
        }
        return of(true);
      }));
  }
}
