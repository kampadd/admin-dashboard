import {Injectable} from '@angular/core';
import {MatIconRegistry} from '@angular/material';
import {DomSanitizer} from '@angular/platform-browser';

@Injectable({providedIn: 'root'})
export class IconsService {

  constructor(private matIconRegistry: MatIconRegistry,
              private domSanitizer: DomSanitizer) {
  }

  public registerSvgIcons(): void {
    this.matIconRegistry.addSvgIcon(
      'view',
      this.domSanitizer.bypassSecurityTrustResourceUrl('/assets/icons/view.svg')
    );
    this.matIconRegistry.addSvgIcon(
      'network',
      this.domSanitizer.bypassSecurityTrustResourceUrl('/assets/icons/network.svg')
    );
    this.matIconRegistry.addSvgIcon(
      'compass',
      this.domSanitizer.bypassSecurityTrustResourceUrl('/assets/icons/compass.svg')
    );
    this.matIconRegistry.addSvgIcon(
      'problem',
      this.domSanitizer.bypassSecurityTrustResourceUrl('/assets/icons/problem.svg')
    );
  }

}
