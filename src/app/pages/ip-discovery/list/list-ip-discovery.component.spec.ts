import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ListIpDiscoveryComponent} from './list-ip-discovery.component';


describe('ListIpDiscoveryComponent', () => {
  let component: ListIpDiscoveryComponent;
  let fixture: ComponentFixture<ListIpDiscoveryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ListIpDiscoveryComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListIpDiscoveryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
