import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {TableDataSource} from '../../../shared/utils/table-datasource';
import {MatDialog, MatPaginator} from '@angular/material';
import {Select} from '@ngxs/store';
import {Observable, Subscription} from 'rxjs';
import {ITableData} from '../../../shared/interfaces/i-table-data';
import {Dispatch} from '@ngxs-labs/dispatch-decorator';
import {first, tap} from 'rxjs/operators';
import {DeleteIpDiscoveryConfig, FetchIpDiscoveryListData} from '../../../store/actions/ip-discovery.action';
import {IpDiscoveryState} from '../../../store/states/ip-discovery.state';
import {IpDiscoveryConfig} from '../../../shared/models/ip-discovery.model';
import {CreateIpDiscoveryComponent} from '../create/create-ip-discovery.component';
import {ConfirmationDialogService} from '../../../shared/services/confirmation-dialog.service';

@Component({
  selector: 'app-list-ip-discovery',
  templateUrl: './list-ip-discovery.component.html',
  styleUrls: ['./list-ip-discovery.component.scss']
})
export class ListIpDiscoveryComponent implements OnInit, AfterViewInit, OnDestroy {
  private subscription: Subscription;
  public displayedColumns: string[] = ['name', 'network', 'minion', 'schedule', 'runStatus', 'actions'];
  public dataSource = new TableDataSource();

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  @Select(IpDiscoveryState.getTableData) ipDiscoveryTableData$: Observable<ITableData>;
  @Dispatch() private fetchTableData = () => new FetchIpDiscoveryListData(this.paginator.pageIndex, this.paginator.pageSize);
  @Dispatch() deleteConfig = (id: string) => new DeleteIpDiscoveryConfig(id);

  constructor(private dialog: MatDialog, private confirmationDialogService: ConfirmationDialogService) {
  }

  ngOnInit() {
    this.fetchTableData();
    this.ipDiscoveryTableData$.subscribe(data => {
      this.dataSource.update(data);
    });
  }

  ngAfterViewInit() {
    this.subscription = this.paginator.page.pipe(tap(() => this.fetchTableData())).subscribe();
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  editConfig(config: IpDiscoveryConfig): void {
    setTimeout(() => {
      this.dialog.open(CreateIpDiscoveryComponent, {
        minWidth: '50vw',
        maxWidth: '95vw',
        maxHeight: '95vh',
        disableClose: true,
        data: config
      });
    });
  }

  removeConfig(id: string): void {
    this.confirmationDialogService.open(`Do you want to proceed with action`);
    this.confirmationDialogService.dialogRef.afterClosed().pipe(
      first(),
      tap(value => {
        if (value) {
          this.deleteConfig(id);
        }
      })).subscribe();
  }
}
