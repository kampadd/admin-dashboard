import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TranslateModule} from '@ngx-translate/core';
import {
  MatButtonModule,
  MatCardModule,
  MatDatepickerModule,
  MatDialogModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatPaginatorModule,
  MatRadioModule,
  MatSelectModule,
  MatTableModule
} from '@angular/material';
import {ListIpDiscoveryComponent} from './list-ip-discovery.component';
import {NgxsModule} from '@ngxs/store';
import {IpDiscoveryState} from '../../../store/states/ip-discovery.state';
import {CreateIpDiscoveryComponent} from '../create/create-ip-discovery.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatMomentDateModule} from '@angular/material-moment-adapter';
import {MinionState} from '../../../store/states/minion.state';
import {NetworkState} from '../../../store/states/network.state';
import {CreateNetworkComponent} from '../create-network/create-network.component';

@NgModule({
  declarations: [ListIpDiscoveryComponent, CreateIpDiscoveryComponent, CreateNetworkComponent],
  imports: [
    CommonModule,
    TranslateModule,
    MatTableModule,
    MatIconModule,
    MatCardModule,
    MatButtonModule,
    MatPaginatorModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatRadioModule,
    MatDatepickerModule,
    MatMomentDateModule,
    NgxsModule.forFeature([IpDiscoveryState, MinionState, NetworkState])
  ],
  exports: [ListIpDiscoveryComponent],
  entryComponents: [CreateIpDiscoveryComponent, CreateNetworkComponent]
})
export class ListIpDiscoveryModule {
}
