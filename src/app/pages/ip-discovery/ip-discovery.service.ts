import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {createRequestOption} from 'src/app/shared/utils/request-util';
import {map} from 'rxjs/operators';
import {Injectable} from '@angular/core';
import {ITableData} from '../../shared/interfaces/i-table-data';
import {IpDiscoveryConfig} from '../../shared/models/ip-discovery.model';
import moment from 'moment';
import {DATE_TIME_FORMAT} from '../../shared/utils/constants';
import {CORE_SERVER_API} from '../../app.constants';

@Injectable({providedIn: 'root'})
export class IpDiscoveryService {
  private resourceUrl = CORE_SERVER_API + '/api/ip-discovery-configs';

  public constructor(private http: HttpClient) {
  }

  create(config: IpDiscoveryConfig): Observable<IpDiscoveryConfig> {
    const copy = this.convertDateFromClient(config);
    return this.http.post<IpDiscoveryConfig>(this.resourceUrl, copy)
      .pipe(map((value: IpDiscoveryConfig) => this.convertDateFromServer(value)));
  }

  update(config: IpDiscoveryConfig): Observable<IpDiscoveryConfig> {
    const copy = this.convertDateFromClient(config);
    return this.http.put<IpDiscoveryConfig>(this.resourceUrl, copy)
      .pipe(map((value: IpDiscoveryConfig) => this.convertDateFromServer(value)));
  }

  get(id: string): Observable<IpDiscoveryConfig> {
    return this.http
      .get<IpDiscoveryConfig>(`${this.resourceUrl}/${id}`)
      .pipe(map((config: IpDiscoveryConfig) => this.convertDateFromServer(config)));
  }

  query(req?: any): Observable<ITableData> {
    const options = createRequestOption(req);
    return this.http.get<IpDiscoveryConfig[]>(this.resourceUrl, {params: options, observe: 'response'}).pipe(
      map((res: HttpResponse<IpDiscoveryConfig[]>) => {
        return {
          totalCount: parseInt(res.headers.get('X-Total-Count'), 10),
          entries: this.convertDateArrayFromServer(res.body)
        };
      })
    );
  }

  delete(id: string): Observable<any> {
    return this.http.delete(`${this.resourceUrl}/${id}`);
  }

  private convertDateFromClient(config: IpDiscoveryConfig): IpDiscoveryConfig {
    return Object.assign({}, config, {
      schedule: config.schedule != null && config.schedule.isValid() ? config.schedule.format(DATE_TIME_FORMAT) : null
    });
  }

  private convertDateFromServer(config: IpDiscoveryConfig): IpDiscoveryConfig {
    config.schedule = config.schedule != null ? moment(config.schedule) : null;
    return config;
  }

  private convertDateArrayFromServer(configs: IpDiscoveryConfig[]): IpDiscoveryConfig[] {
    return configs.map((config: IpDiscoveryConfig) => {
      config.schedule = config.schedule != null ? moment(config.schedule) : null;
      return config;
    });
  }

}
