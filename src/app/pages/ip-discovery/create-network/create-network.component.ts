import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Component, OnInit} from '@angular/core';
import {MatDialogRef} from '@angular/material';
import {Store} from '@ngxs/store';
import {first, tap} from 'rxjs/operators';
import {AddNetwork} from '../../../store/actions/network.action';
import {Network} from '../../../shared/models/network.model';
import {validateIpCidr} from '../../../shared/utils/validators';

@Component({
  selector: 'app-create-network',
  templateUrl: './create-network.component.html'
})
export class CreateNetworkComponent implements OnInit {

  form: FormGroup;

  constructor(private store: Store,
              private fb: FormBuilder,
              private dialogRef: MatDialogRef<CreateNetworkComponent>) {
  }

  ngOnInit() {
    this.createForm();
  }

  private createForm(): void {
    this.form = this.fb.group({
      name: ['', [Validators.required]],
      cidr: ['', [Validators.required, validateIpCidr()]],
      gateway: ['', [Validators.required, validateIpCidr()]]
    });
  }

  onSubmit(): void {
    if (this.form.valid) {
      const config = this.form.getRawValue() as Network;
      this.store.dispatch(new AddNetwork(config)).pipe(
        first(),
        tap(() => this.dialogRef.close())).subscribe();
    }
  }
}
