import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Network} from '../../shared/models/network.model';
import {CORE_SERVER_API} from '../../app.constants';


@Injectable({providedIn: 'root'})
export class NetworkService {
  public resourceUrl = CORE_SERVER_API + '/api/network-items';

  constructor(protected http: HttpClient) {
  }

  create(network: Network): Observable<Network> {
    return this.http.post<Network>(this.resourceUrl, network);
  }

  getAll(): Observable<Network[]> {
    return this.http.get<Network[]>(this.resourceUrl);
  }
}
