import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {IpDiscoveryComponent} from './ip-discovery.component';
import {ListIpDiscoveryModule} from './list/list-ip-discovery.module';
import {IpDiscoveryRoutingModule} from './ip-discovery-routing.module';

@NgModule({
  declarations: [IpDiscoveryComponent],
  imports: [
    CommonModule,
    ListIpDiscoveryModule,
    IpDiscoveryRoutingModule
  ]
})
export class IpDiscoveryModule {
}
