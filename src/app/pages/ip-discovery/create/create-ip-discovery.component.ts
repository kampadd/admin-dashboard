import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {Dispatch} from '@ngxs-labs/dispatch-decorator';
import {AddIpDiscoveryConfig, UpdateIpDiscoveryConfig} from '../../../store/actions/ip-discovery.action';
import {IpDiscoveryConfig, RunState} from '../../../shared/models/ip-discovery.model';
import {createTimeDate} from '../../../shared/utils/conversion.utils';
import {validateTime} from '../../../shared/utils/validators';
import moment from 'moment';
import {MAT_DATE_FORMATS, MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {DD_MM_YYYY_Format} from '../../../shared/utils/constants';
import {Observable, Subscription} from 'rxjs';
import {Select, Store} from '@ngxs/store';
import {FetchMinions} from '../../../store/actions/minion.action';
import {MinionState} from '../../../store/states/minion.state';
import {Minion} from '../../../shared/models/minion.model';
import {first, tap} from 'rxjs/operators';
import {CreateNetworkComponent} from '../create-network/create-network.component';
import {NetworkState} from '../../../store/states/network.state';
import {Network} from '../../../shared/models/network.model';
import {FetchNetworks} from '../../../store/actions/network.action';

@Component({
  selector: 'app-create-ip-discovery',
  templateUrl: './create-ip-discovery.component.html',
  styleUrls: ['./create-ip-discovery.component.scss'],
  providers: [{provide: MAT_DATE_FORMATS, useValue: DD_MM_YYYY_Format}]
})
export class CreateIpDiscoveryComponent implements OnInit, OnDestroy {

  private runOptionSubscription: Subscription;

  ipDiscoveryForm: FormGroup;
  today = moment().toDate();
  runStatus = RunState;

  @Select(MinionState.getMinions) minions: Observable<Minion[]>;
  @Select(NetworkState.getNetworks) networks: Observable<Network[]>;
  @Dispatch() private fetchMinions = () => new FetchMinions();
  @Dispatch() private fetchNetworks = () => new FetchNetworks();

  compareFn = (arg1: Minion, arg2: Minion) => arg1.id === arg2.id;
  compareNetworksFn = (arg1: Network, arg2: Network) => (arg1.cidr === arg2.cidr) && (arg1.gateway === arg2.gateway);

  constructor(@Inject(MAT_DIALOG_DATA) public config: IpDiscoveryConfig,
              private store: Store,
              private fb: FormBuilder,
              private dialog: MatDialog,
              private dialogRef: MatDialogRef<CreateIpDiscoveryComponent>) {
  }

  ngOnInit() {
    this.fetchMinions();
    this.fetchNetworks();
    this.createForm();
    this.onRunOptionChanged();
    this.fillForm();
  }

  private createForm(): void {
    this.ipDiscoveryForm = this.fb.group({
      name: ['', [Validators.required]],
      minion: ['', [Validators.required]],
      network: ['', [Validators.required]],
      runStatus: [this.runStatus.SCHEDULED],
      scheduleDate: ['', [Validators.required]],
      scheduleTime: ['', [Validators.required, validateTime()]],
      description: ['']
    });
  }

  onSubmit(): void {
    if (this.ipDiscoveryForm.valid) {
      if (this.config && this.config.id) {
        const config: IpDiscoveryConfig = JSON.parse(JSON.stringify(this.config));
        config.name = this.ipDiscoveryForm.get('name').value;
        config.minion = this.ipDiscoveryForm.get('minion').value;
        config.network = (this.ipDiscoveryForm.get('network').value || {} as Network).cidr;
        config.runStatus = this.ipDiscoveryForm.get('runStatus').value;
        config.description = this.ipDiscoveryForm.get('description').value;
        config.schedule = this.computeRunDate();
        this.store.dispatch(new UpdateIpDiscoveryConfig(config)).pipe(first(),
          tap(() => this.dialogRef.close())).subscribe();
      } else {
        const config: IpDiscoveryConfig = this.ipDiscoveryForm.getRawValue();
        config.schedule = this.computeRunDate();
        config.network = (this.ipDiscoveryForm.get('network').value || {} as Network).cidr;
        this.store.dispatch(new AddIpDiscoveryConfig(config)).pipe(first(),
          tap(() => this.dialogRef.close())).subscribe();
      }
    }
  }

  ngOnDestroy() {
    if (this.runOptionSubscription) {
      this.runOptionSubscription.unsubscribe();
    }
  }

  addNetwork(): void {
    setTimeout(() => {
      this.dialog.open(CreateNetworkComponent, {
        minWidth: '10vw',
        maxWidth: '95vw',
        maxHeight: '95vh',
        disableClose: true
      });
    });
  }

  private onRunOptionChanged(): void {
    this.runOptionSubscription = this.ipDiscoveryForm.get('runStatus').valueChanges.subscribe((option: RunState) => {
      if (option === this.runStatus.RUNNING) {
        this.disableSchedule();
      } else {
        this.enableSchedule();
      }
    });
  }

  private fillForm(): void {
    if (this.config) {
      this.ipDiscoveryForm.patchValue({
        name: this.config.name,
        minion: this.config.minion.id,
        network: this.config.network,
        runStatus: this.config.runStatus,
        scheduleDate: moment(this.config.schedule),
        scheduleTime: moment(this.config.schedule).format('HH:mm'),
        description: this.config.description
      });
    }
  }

  private enableSchedule(): void {
    this.ipDiscoveryForm.get('scheduleDate').enable();
    this.ipDiscoveryForm.get('scheduleTime').enable();
  }

  private disableSchedule(): void {
    this.ipDiscoveryForm.get('scheduleDate').disable();
    this.ipDiscoveryForm.get('scheduleTime').disable();
  }

  private computeRunDate(): moment.Moment {
    if (this.ipDiscoveryForm.invalid) {
      return undefined;
    }
    return createTimeDate(this.ipDiscoveryForm.get('scheduleDate').value as moment.Moment,
      this.ipDiscoveryForm.get('scheduleTime').value);
  }
}
