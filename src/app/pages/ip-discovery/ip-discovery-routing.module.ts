import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {IpDiscoveryComponent} from './ip-discovery.component';


export const routes: Routes = [
  {
    path: '',
    component: IpDiscoveryComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IpDiscoveryRoutingModule {
}
