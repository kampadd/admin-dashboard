import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {BasicMonitoringComponent} from './basic-monitoring.component';


export const routes: Routes = [
  {
    path: '',
    component: BasicMonitoringComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BasicMonitoringRoutingModule {
}
