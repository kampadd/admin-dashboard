import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatSort} from '@angular/material';
import {Select, Store} from '@ngxs/store';
import {EMPTY, merge, Observable, Subject} from 'rxjs';

import {Dispatch} from '@ngxs-labs/dispatch-decorator';
import {first, switchMap, takeUntil, tap} from 'rxjs/operators';

import {TableDataSource} from '../../shared/utils/table-datasource';
import {ConfirmationDialogService} from '../../shared/services/confirmation-dialog.service';
import {ITableData} from '../../shared/interfaces/i-table-data';
import {DeleteSnmpConfig, FetchSnmpConfigs} from '../../store/actions/snmp-config.action';
import {BasicMonitoring} from '../../shared/models/basic-monitoring.model';
import {BasicMonitoringState} from '../../store/states/basic-monitoring.state';
import {CreateBasicMonitoringComponent} from './create/create-basic-monitoring.component';

@Component({
  selector: 'app-basic-monitoring',
  templateUrl: 'basic-monitoring.component.html'
})
export class BasicMonitoringComponent implements OnInit, AfterViewInit, OnDestroy {

  private destroy$ = new Subject<boolean>();

  public displayedColumns: string[] = [
    'name',
    'minion',
    'type',
    'enabled',
    'schedule',
    'ipv4',
    'ipv6',
    'mode',
    'actions'
  ];
  public dataSource = new TableDataSource();

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  @Select(BasicMonitoringState.getTableData) snmpTableData: Observable<ITableData>;
  @Dispatch() private fetchTableData = () => new FetchSnmpConfigs([this.sort.active + ',' + this.sort.direction],
    this.paginator.pageIndex, this.paginator.pageSize);

  constructor(private dialog: MatDialog, private store: Store, private confirmationDialogService: ConfirmationDialogService) {
  }

  ngOnInit() {
    this.fetchTableData();
    this.snmpTableData.subscribe(data => {
      this.dataSource.update(data);
    });
  }

  ngAfterViewInit() {
    this.sort.sortChange.pipe(takeUntil(this.destroy$)).subscribe(() => this.paginator.pageIndex = 0);

    merge(this.sort.sortChange, this.paginator.page).pipe(
      takeUntil(this.destroy$),
      tap(() => this.fetchTableData())).subscribe();
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  editConfig(config: BasicMonitoring): void {
    setTimeout(() => {
      this.dialog.open(CreateBasicMonitoringComponent, {
        minWidth: '10vw',
        maxWidth: '95vw',
        maxHeight: '95vh',
        disableClose: true,
        data: config
      });
    });
  }

  removeConfig(id: string): void {
    this.confirmationDialogService.open(`Do you want to proceed with action`);
    this.confirmationDialogService.dialogRef.afterClosed().pipe(
      first(),
      switchMap(value => {
        if (value) {
          return this.store.dispatch(new DeleteSnmpConfig(id));
        }
        return EMPTY;
      }), switchMap(() => this.store.dispatch(new FetchSnmpConfigs()))).subscribe();
  }
}
