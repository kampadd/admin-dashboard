import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TranslateModule} from '@ngx-translate/core';
import {
  MatButtonModule,
  MatCardModule, MatCheckboxModule, MatChipsModule,
  MatDialogModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatPaginatorModule,
  MatRadioModule,
  MatSelectModule,
  MatSortModule,
  MatTableModule
} from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgxsModule} from '@ngxs/store';
import {MinionState} from '../../store/states/minion.state';
import {BasicMonitoringState} from '../../store/states/basic-monitoring.state';
import {BasicMonitoringComponent} from './basic-monitoring.component';
import {BasicMonitoringRoutingModule} from './basic-monitoring-routing.module';
import {CreateBasicMonitoringComponent} from './create/create-basic-monitoring.component';

@NgModule({
  declarations: [BasicMonitoringComponent, CreateBasicMonitoringComponent],
  imports: [
    CommonModule,
    TranslateModule,
    MatTableModule,
    MatIconModule,
    MatCardModule,
    MatButtonModule,
    MatPaginatorModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    BasicMonitoringRoutingModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatRadioModule,
    NgxsModule.forFeature([BasicMonitoringState, MinionState]),
    MatSortModule,
    MatCheckboxModule,
    MatChipsModule
  ],
  entryComponents: [CreateBasicMonitoringComponent]
})
export class BasicMonitoringModule {
}
