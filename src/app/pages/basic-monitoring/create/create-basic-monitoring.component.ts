import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {Dispatch} from '@ngxs-labs/dispatch-decorator';
import {MAT_DIALOG_DATA, MatChipInputEvent, MatDialogRef} from '@angular/material';
import {Observable, Subject} from 'rxjs';
import {Select, Store} from '@ngxs/store';
import {FetchMinions} from '../../../store/actions/minion.action';
import {MinionState} from '../../../store/states/minion.state';
import {Minion} from '../../../shared/models/minion.model';
import {first, takeUntil, tap} from 'rxjs/operators';
import {BasicMonitoring, BasicMonitoringMode, BasicMonitoringType} from '../../../shared/models/basic-monitoring.model';
import {AddBasicMonitoring, UpdateBasicMonitoring} from '../../../store/actions/basic-monitoring.action';
import {COMMA, ENTER} from '@angular/cdk/keycodes';

@Component({
  selector: 'app-create-basic-monitoring',
  templateUrl: 'create-basic-monitoring.component.html'
})
export class CreateBasicMonitoringComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<boolean>();
  form: FormGroup;
  monitoringTypeEnum = BasicMonitoringType;
  monitoringModeEnum = BasicMonitoringMode;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];

  @Select(MinionState.getMinions) minions: Observable<Minion[]>;
  @Dispatch() private fetchMinions = () => new FetchMinions();

  compareFn = (arg1: Minion, arg2: Minion) => arg1.id === arg2.id;

  constructor(@Inject(MAT_DIALOG_DATA) public config: BasicMonitoring,
              private store: Store,
              private fb: FormBuilder,
              private dialogRef: MatDialogRef<CreateBasicMonitoringComponent>) {
  }

  ngOnInit() {
    this.fetchMinions();
    this.createForm();
    this.fillForm();
    this.onChangedType();
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  private createForm(): void {
    this.form = this.fb.group({
      name: ['', [Validators.required]],
      minion: [''],
      type: [''],
      enabled: [''],
      schedule: [''],
      ipv4: [''],
      ipv6: [''],
      mode: [''],
      timeout: [''],
      fields: [''],
      fieldsUnderRoot: [''],
      processors: [''],
      hosts: [[], [Validators.required]],
      wait: [''],
      ports: [[], [Validators.required]],
      check: [''],
      send: [''],
      receive: [''],
      urls: [[], [Validators.required]],
      proxyUrl: [''],
      username: [''],
      password: [''],
    });
  }

  onSubmit(): void {
    if (this.form.valid) {
      if (this.config && this.config.id) {
        const config = JSON.parse(JSON.stringify(this.config)) as BasicMonitoring;

        this.store.dispatch(new UpdateBasicMonitoring(config)).pipe(first(),
          tap(() => this.dialogRef.close())).subscribe();
      } else {
        const config = this.form.getRawValue() as BasicMonitoring;
        this.store.dispatch(new AddBasicMonitoring(config)).pipe(first(),
          tap(() => this.dialogRef.close())).subscribe();
      }
    }
  }

  private fillForm(): void {
    if (this.config) {
      this.form.patchValue(this.config || {});
    }
  }

  private onChangedType(): void {
    this.form.get('type').valueChanges.pipe(takeUntil(this.destroy$)).subscribe((value: BasicMonitoringType) => {
      const hostsControl = this.form.get('hosts');
      const portsControl = this.form.get('ports');
      const urlsControl = this.form.get('urls');
      if (BasicMonitoringType.ICMP === value) {
        hostsControl.enable();
        urlsControl.disable();
        portsControl.disable();
      } else if (BasicMonitoringType.TCP === value) {
        hostsControl.enable();
        portsControl.enable();
        urlsControl.disable();
      } else if (BasicMonitoringType.HTTP === value) {
        urlsControl.enable();
        hostsControl.disable();
        portsControl.disable();
      }
    });
  }

  addUrl(event: MatChipInputEvent): void {
    const value = event.value;
    const input = event.input;

    if (value.trim()) {
      const urlsControl = this.form.get('urls');
      urlsControl.patchValue([...(urlsControl.value || []), value.trim()]);
    }
    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  removeUrl(key: string): void {
    const urlsControl = this.form.get('urls');
    urlsControl.patchValue((urlsControl.value || []).filter(value => value !== key));
  }

  removeHost(key: string): void {
    const hostsControl = this.form.get('hosts');
    hostsControl.patchValue((hostsControl.value || []).filter(value => value !== key));
  }

  addHost(event: MatChipInputEvent): void {
    const value = event.value;
    const input = event.input;

    if (value.trim()) {
      const hostsControl = this.form.get('hosts');
      hostsControl.patchValue([...(hostsControl.value || []), value.trim()]);
    }
    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  removePort(key: string): void {
    const portsControl = this.form.get('ports');
    portsControl.patchValue((portsControl.value || []).filter(value => value !== key));
  }

  addPort(event: MatChipInputEvent): void {
    const value = event.value;
    const input = event.input;

    if (value.trim()) {
      const portsControl = this.form.get('ports');
      portsControl.patchValue([...(portsControl.value || []), value.trim()]);
    }
    // Reset the input value
    if (input) {
      input.value = '';
    }
  }
}
