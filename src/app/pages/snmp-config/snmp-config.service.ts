import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {createRequestOption} from 'src/app/shared/utils/request-util';
import {map} from 'rxjs/operators';
import {Injectable} from '@angular/core';
import {ITableData} from '../../shared/interfaces/i-table-data';
import {SnmpConfig} from '../../shared/models/snmp-config.model';
import {CORE_SERVER_API} from '../../app.constants';

@Injectable({providedIn: 'root'})
export class SnmpConfigService {
  private resourceUrl = CORE_SERVER_API + '/api/snmp-configs';

  public constructor(private http: HttpClient) {
  }

  create(config: SnmpConfig): Observable<SnmpConfig> {
    return this.http.post<SnmpConfig>(this.resourceUrl, config);
  }

  update(config: SnmpConfig): Observable<SnmpConfig> {
    return this.http.put<SnmpConfig>(this.resourceUrl, config);
  }

  get(id: string): Observable<SnmpConfig> {
    return this.http.get<SnmpConfig>(`${this.resourceUrl}/${id}`);
  }

  query(req?: any): Observable<ITableData> {
    const options = createRequestOption(req);
    return this.http.get<SnmpConfig[]>(this.resourceUrl, {params: options, observe: 'response'}).pipe(
      map((res: HttpResponse<SnmpConfig[]>) => {
        return {
          totalCount: parseInt(res.headers.get('X-Total-Count'), 10),
          entries: res.body
        };
      })
    );
  }

  delete(id: string): Observable<any> {
    return this.http.delete(`${this.resourceUrl}/${id}`);
  }
}
