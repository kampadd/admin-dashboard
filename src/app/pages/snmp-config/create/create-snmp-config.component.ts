import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Component, Inject, OnInit} from '@angular/core';
import {Dispatch} from '@ngxs-labs/dispatch-decorator';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Observable} from 'rxjs';
import {Select, Store} from '@ngxs/store';
import {FetchMinions} from '../../../store/actions/minion.action';
import {MinionState} from '../../../store/states/minion.state';
import {Minion} from '../../../shared/models/minion.model';
import {first, tap} from 'rxjs/operators';
import {Monitoring, SnmpConfig} from '../../../shared/models/snmp-config.model';
import {AddSnmpConfig, UpdateSnmpConfig} from '../../../store/actions/snmp-config.action';
import {validateIpCidr} from '../../../shared/utils/validators';

@Component({
  selector: 'app-create-snmp-config',
  templateUrl: './create-snmp-config.component.html'
})
export class CreateSnmpConfigComponent implements OnInit {

  monitoringEnums = Monitoring;

  form: FormGroup;

  @Select(MinionState.getMinions) minions: Observable<Minion[]>;
  @Dispatch() private fetchMinions = () => new FetchMinions();

  compareFn = (arg1: Minion, arg2: Minion) => arg1.id === arg2.id;

  constructor(@Inject(MAT_DIALOG_DATA) public config: SnmpConfig,
              private store: Store,
              private fb: FormBuilder,
              private dialogRef: MatDialogRef<CreateSnmpConfigComponent>) {
  } 

  ngOnInit() {
    this.fetchMinions();
    this.createForm();
    this.fillForm();
  }

  private createForm(): void {
    this.form = this.fb.group({
      port: ['', [Validators.required]],
      minions: [''],
      ipaddress: ['', [Validators.required, validateIpCidr()]],
      monitored: ['', [Validators.required]],
      community: ['', [Validators.required]]
    });
  }

  onSubmit(): void {
    if (this.form.valid) {
      if (this.config && this.config.id) {
        const config = JSON.parse(JSON.stringify(this.config)) as SnmpConfig;
        config.port = this.form.get('port').value;
        config.minions = this.form.get('minions').value;
        config.ipaddress = this.form.get('ipaddress').value;
        config.monitored = this.form.get('monitored').value;
        config.community = this.form.get('community').value;

        this.store.dispatch(new UpdateSnmpConfig(config)).pipe(first(),
          tap(() => this.dialogRef.close())).subscribe();
      } else {
        const config = this.form.getRawValue() as SnmpConfig;
        this.store.dispatch(new AddSnmpConfig(config)).pipe(first(),
          tap(() => this.dialogRef.close())).subscribe();
      }
    }
  }

  private fillForm(): void {
    if (this.config) {
      this.form.patchValue(this.config || {});
    }
  }
}
