import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatSort} from '@angular/material';
import {Select, Store} from '@ngxs/store';
import {EMPTY, merge, Observable, Subscription} from 'rxjs';

import {Dispatch} from '@ngxs-labs/dispatch-decorator';
import {first, switchMap, tap} from 'rxjs/operators';

import {TableDataSource} from '../../shared/utils/table-datasource';
import {ConfirmationDialogService} from '../../shared/services/confirmation-dialog.service';
import {ITableData} from '../../shared/interfaces/i-table-data';
import {SnmpConfig} from '../../shared/models/snmp-config.model';
import {DeleteSnmpConfig, FetchSnmpConfigs} from '../../store/actions/snmp-config.action';
import {CreateSnmpConfigComponent} from './create/create-snmp-config.component';
import {SnmpConfigState} from '../../store/states/snmp-config.state';

@Component({
  selector: 'app-snmp-config',
  templateUrl: 'snmp-config.component.html'
})
export class SnmpConfigComponent implements OnInit, AfterViewInit, OnDestroy {
  private sortSubscription: Subscription;
  private subscription: Subscription;

  public displayedColumns: string[] = ['ipaddress', 'minions', 'port', 'monitored', 'community', 'actions'];
  public dataSource = new TableDataSource();

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  @Select(SnmpConfigState.getTableData) snmpTableData: Observable<ITableData>;
  @Dispatch() private fetchTableData = () => new FetchSnmpConfigs([this.sort.active + ',' + this.sort.direction],
    this.paginator.pageIndex, this.paginator.pageSize);

  constructor(private dialog: MatDialog, private store: Store, private confirmationDialogService: ConfirmationDialogService) {
  }

  ngOnInit() {
    this.fetchTableData();
    this.snmpTableData.subscribe(data => {
      this.dataSource.update(data);
    });
  }

  ngAfterViewInit() {
    // reset the paginator after sorting
    this.sortSubscription = this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    this.subscription = merge(this.sort.sortChange, this.paginator.page).pipe(
      tap(() => this.fetchTableData())).subscribe();
  }

  ngOnDestroy() {
    if (this.sortSubscription) {
      this.sortSubscription.unsubscribe();
    }
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  editConfig(config: SnmpConfig): void {
    setTimeout(() => {
      this.dialog.open(CreateSnmpConfigComponent, {
        minWidth: '10vw',
        maxWidth: '95vw',
        maxHeight: '95vh',
        disableClose: true,
        data: config
      });
    });
  }

  removeConfig(id: string): void {
    this.confirmationDialogService.open(`Do you want to proceed with action`);
    this.confirmationDialogService.dialogRef.afterClosed().pipe(
      first(),
      switchMap(value => {
        if (value) {
          return this.store.dispatch(new DeleteSnmpConfig(id));
        }
        return EMPTY;
      }), switchMap(() => this.store.dispatch(new FetchSnmpConfigs()))).subscribe();
  }
}
