import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {SnmpConfigComponent} from './snmp-config.component';


export const routes: Routes = [
  {
    path: '',
    component: SnmpConfigComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SnmpConfigRoutingModule {
}
