import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TranslateModule} from '@ngx-translate/core';
import {
  MatButtonModule,
  MatCardModule,
  MatDatepickerModule,
  MatDialogModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatPaginatorModule,
  MatRadioModule,
  MatSelectModule,
  MatSortModule,
  MatTableModule
} from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatMomentDateModule} from '@angular/material-moment-adapter';
import {NgxsModule} from '@ngxs/store';
import {SnmpConfigRoutingModule} from './snmp-config-routing.module';
import {SnmpConfigComponent} from './snmp-config.component';
import {MinionState} from '../../store/states/minion.state';
import {CreateSnmpConfigComponent} from './create/create-snmp-config.component';
import {SnmpConfigState} from '../../store/states/snmp-config.state';

@NgModule({
  declarations: [SnmpConfigComponent, CreateSnmpConfigComponent],
  imports: [
    CommonModule,
    TranslateModule,
    MatTableModule,
    MatIconModule,
    MatCardModule,
    MatButtonModule,
    MatPaginatorModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    SnmpConfigRoutingModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatRadioModule,
    MatDatepickerModule,
    MatMomentDateModule,
    NgxsModule.forFeature([SnmpConfigState, MinionState]),
    MatSortModule
  ],
  entryComponents: [CreateSnmpConfigComponent]
})
export class SnmpConfigModule {
} 
