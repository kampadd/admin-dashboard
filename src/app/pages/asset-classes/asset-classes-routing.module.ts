import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AssetClassesComponent} from './asset-classes.component';

export const routes: Routes = [
  {
    path: '',
    component: AssetClassesComponent,
    children: [
      {
        path: 'assets',
        loadChildren: '../assets/asset/asset.module#AssetModule'
      }, {
        path: 'computers',
        loadChildren: '../assets/computer/computer.module#ComputerModule'
      }, {
        path: 'licenses',
        loadChildren: '../assets/license/license.module#LicenseModule'
      }, {
        path: 'monitors',
        loadChildren: '../assets/monitor/monitor.module#MonitorModule'
      }, {
        path: 'notebooks',
        loadChildren: '../assets/notebook/notebook.module#NotebookModule'
      }, {
        path: 'pcs',
        loadChildren: '../assets/pc/pc.module#PCModule'
      }, {
        path: 'printers',
        loadChildren: '../assets/printer/printer.module#PrinterModule'
      }, {
        path: 'ups',
        loadChildren: '../assets/ups/ups.module#UpsModule'
      },{
        path: 'network-devices',
        loadChildren: '../assets/config-item/config-item.module#ConfigItemModule'
      }, {
        path: '',
        redirectTo: 'assets',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AssetClassesRoutingModule {
}
