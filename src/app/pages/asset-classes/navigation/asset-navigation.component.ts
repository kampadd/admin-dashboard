import {AfterViewInit, Component, EventEmitter, Output, ViewChild} from '@angular/core';
import {FlatTreeControl} from '@angular/cdk/tree';
import {MatTree, MatTreeFlatDataSource, MatTreeFlattener} from '@angular/material';

const TREE_DATA: AssetTreeNode[] = [
  {
    name: 'Configuration Items',
    icon: 'folder',
    route: 'asset-classes',
    children: [
      {
        name: 'Computer',
        icon: 'folder',
        route: 'asset-classes/computers'
      },
      {
        name: 'PC',
        icon: 'insert_drive_file',
        route: 'asset-classes/pcs'
      },
      {
        name: 'Notebook',
        icon: 'insert_drive_file',
        route: 'asset-classes/notebooks'
      }, {
        name: 'Monitor',
        icon: 'insert_drive_file',
        route: 'asset-classes/monitors'
      },
      {
        name: 'Printer',
        icon: 'insert_drive_file',
        route: 'asset-classes/printers'
      },
      {
        name: 'Network Devices',
        icon: 'insert_drive_file',
        route: 'asset-classes/network-devices'
      },
      {
        name: 'UPS',
        icon: 'insert_drive_file',
        route: 'asset-classes/ups'
      },
      {
        name: 'License',
        icon: 'insert_drive_file',
        route: 'asset-classes/licenses'
      }
    ]
  }
];

export interface AssetTreeNode {
  name: string;
  icon: string;
  route: string;
  level?: number;
  expandable?: boolean;
  children?: AssetTreeNode[];
}


@Component({
  selector: 'app-asset-navigation',
  templateUrl: './asset-navigation.component.html',
  styleUrls: ['./asset-navigation.component.scss']
})

export class AssetNavigationComponent implements AfterViewInit {

  @ViewChild(MatTree, {static: true}) tree: MatTree<any>;
  @Output() isClicked = new EventEmitter<boolean>();

  treeControl = new FlatTreeControl<AssetTreeNode>(
    node => node.level, node => node.expandable);

  treeFlattener = new MatTreeFlattener(
    (node: AssetTreeNode, level: number) => {
      return {
        expandable: !!node.children && node.children.length > 0,
        name: node.name,
        level: level,
        icon: node.icon,
        route: node.route
      };
    }, node => node.level, node => node.expandable, node => node.children);

  dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

  constructor() {
    this.dataSource.data = TREE_DATA;
  }

  hasChild = (_: number, node: AssetTreeNode) => node.expandable;

  ngAfterViewInit(): void {
    this.tree.treeControl.expandAll();
  }
}
