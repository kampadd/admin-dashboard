import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MatButtonModule, MatIconModule, MatListModule, MatMenuModule, MatTreeModule} from '@angular/material';
import {TranslateModule} from '@ngx-translate/core';
import {AssetNavigationComponent} from './asset-navigation.component';
import {RouterModule} from '@angular/router';

@NgModule({
  declarations: [AssetNavigationComponent],
  imports: [
    CommonModule,
    TranslateModule,
    MatTreeModule,
    MatButtonModule,
    MatIconModule,
    MatMenuModule,
    MatListModule,
    RouterModule
  ],
  exports: [AssetNavigationComponent]
})
export class AssetNavigationModule {
}
