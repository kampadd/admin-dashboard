import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AssetClassesRoutingModule} from './asset-classes-routing.module';
import {AssetClassesComponent} from './asset-classes.component';
import {MatCardModule} from '@angular/material';


@NgModule({
  declarations: [AssetClassesComponent],
  imports: [
    CommonModule,
    AssetClassesRoutingModule,
    MatCardModule
  ]
})
export class AssetClassesModule {
}
