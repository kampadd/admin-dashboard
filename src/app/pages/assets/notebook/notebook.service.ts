import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {INotebook} from 'src/app/shared/models/notebook.model';
import moment from 'moment';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {createRequestOption} from 'src/app/shared/utils/request-util';
import {ITableData} from 'src/app/shared/interfaces/i-table-data';
import {CMDB_SERVER_API} from '../../../app.constants';

@Injectable({providedIn: 'root'})
export class NotebookService {
  public resourceUrl = CMDB_SERVER_API + '/api/notebooks';

  constructor(private http: HttpClient) {
  }

  create(notebook: INotebook): Observable<INotebook> {
    const copy = this.convertDateFromClient(notebook);
    return this.http
      .post<INotebook>(this.resourceUrl, copy)
      .pipe(map((result: INotebook) => this.convertDateFromServer(result)));
  }

  update(notebook: INotebook): Observable<INotebook> {
    const copy = this.convertDateFromClient(notebook);
    return this.http
      .put<INotebook>(this.resourceUrl, copy)
      .pipe(map((result: INotebook) => this.convertDateFromServer(result)));
  }

  query(req?: any): Observable<ITableData> {
    const options = createRequestOption(req);
    return this.http.get<INotebook[]>(this.resourceUrl, {params: options, observe: 'response'}).pipe(
      map((result: HttpResponse<INotebook[]>) => {
        return {
          totalCount: parseInt(result.headers.get('X-Total-Count'), 10),
          entries: this.convertDateArrayFromServer(result.body)
        };
      })
    );
  }

  delete(id: string): Observable<any> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`);
  }


  private convertDateFromClient(notebook: INotebook): INotebook {
    return Object.assign({}, notebook, {
      beginDate: notebook.beginDate != null && notebook.beginDate.isValid() ? notebook.beginDate.toJSON() : null,
      endDate: notebook.endDate != null && notebook.endDate.isValid() ? notebook.endDate.toJSON() : null,
      purchaseDate: notebook.purchaseDate != null && notebook.purchaseDate.isValid() ? notebook.purchaseDate.toJSON() : null,
      acceptanceDate: notebook.acceptanceDate != null && notebook.acceptanceDate.isValid() ? notebook.purchaseDate.toJSON() : null
    });
  }

  private convertDateFromServer(result: INotebook): INotebook {
    if (result) {
      result.beginDate = result.beginDate != null ? moment(result.beginDate) : null;
      result.endDate = result.endDate != null ? moment(result.endDate) : null;
      result.purchaseDate = result.purchaseDate != null ? moment(result.purchaseDate) : null;
      result.acceptanceDate = result.acceptanceDate != null ? moment(result.acceptanceDate) : null;
    }
    return result;
  }

  private convertDateArrayFromServer(result: INotebook[]): INotebook[] {
    if (result) {
      result.forEach((notebook: INotebook) => {
        notebook.beginDate = notebook.beginDate != null ? moment(notebook.beginDate) : null;
        notebook.endDate = notebook.endDate != null ? moment(notebook.endDate) : null;
        notebook.purchaseDate = notebook.purchaseDate != null ? moment(notebook.purchaseDate) : null;
        notebook.acceptanceDate = notebook.acceptanceDate != null ? moment(notebook.acceptanceDate) : null;
      });
    }
    return result;
  }
}
