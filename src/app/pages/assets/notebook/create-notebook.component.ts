import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { INotebook } from 'src/app/shared/models/notebook.model';
import { Store } from '@ngxs/store';
import { UpdateNotebook, AddNotebook } from 'src/app/store/actions/notebook.action';
import { first, tap } from 'rxjs/operators';

@Component({
  selector: 'app-create-notebook',
  templateUrl: './create-notebook.component.html'
})
export class CreateNotebookComponent implements OnInit {
  form: FormGroup;

  constructor(@Inject(MAT_DIALOG_DATA) public notebook: INotebook,
              private fb: FormBuilder,
              private store: Store,
              private dialogRef: MatDialogRef<CreateNotebookComponent>) { }

  ngOnInit() { 
    this.createForm();
    this.fillForm();
  }

  private createForm(): void {
    this.form = this.fb.group({
      idClass: [''],
      code: [''],
      description: [''],
      status: [''],
      user: [''],
      beginDate: ['', [Validators.required]],
      notes: [''],
      endDate: ['', [Validators.required]],
      currentId: [''],
      idTenant: [''],
      serialNumber: [''],
      supplier: [''],
      purchaseDate: ['', [Validators.required]],
      acceptanceDate: ['', [Validators.required]],
      finalCost: [''],
      brand: [''],
      model: [''],
      room: [''],
      assignee: [''],
      technicalReference: [''],
      workplace: [''],
      acceptanceNotes: [''],
      ram: [''],
      cpuNumber: [''],
      cpuSpeed: [''],
      hdSize: [''],
      ipAddress: [ null,
                    [ 
                      Validators.pattern(
                        '(^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])$)'
                        )
                    ] 
                  ],
      screenSize: ['']
    });
  }

  private fillForm(): void {
    if(this.notebook) {
      this.form.patchValue(this.notebook || {});
    }
  }

  onSubmit(): void {
    if(this.form.valid) {
      if(this.notebook && this.notebook.id) {
        const notebook = JSON.parse(JSON.stringify(this.notebook)) as INotebook;
        notebook.idClass = this.form.get('idClass').value;
        notebook.code = this.form.get('code').value;
        notebook.description = this.form.get('description').value;
        notebook.status = this.form.get('status').value;
        notebook.user = this.form.get('user').value;
        notebook.beginDate = this.form.get('beginDate').value;
        notebook.notes = this.form.get('notes').value;
        notebook.endDate = this.form.get('endDate').value;
        notebook.currentId = this.form.get('currentId').value;
        notebook.idTenant = this.form.get('idTenant').value;
        notebook.serialNumber = this.form.get('serialNumber').value;
        notebook.supplier = this.form.get('supplier').value;
        notebook.purchaseDate = this.form.get('purchaseDate').value;
        notebook.acceptanceDate = this.form.get('acceptanceDate').value;
        notebook.finalCost = this.form.get('finalCost').value;
        notebook.brand = this.form.get('brand').value;
        notebook.model = this.form.get('model').value;
        notebook.room = this.form.get('room').value;
        notebook.assignee = this.form.get('assignee').value;
        notebook.technicalReference = this.form.get('technicalReference').value;
        notebook.workplace = this.form.get('workplace').value;
        notebook.acceptanceNotes = this.form.get('acceptanceNotes').value;
        notebook.ram = this.form.get('ram').value;
        notebook.cpuNumber = this.form.get('cpuNumber').value;
        notebook.hdSize = this.form.get('hdSize').value;
        notebook.ipAddress = this.form.get('ipAddress').value;
        notebook.screenSize = this.form.get('screenSize').value;

        this.store.dispatch(new UpdateNotebook(notebook)).pipe(first(),
          tap(() => this.dialogRef.close())).subscribe();
      } else {
        const notebook = this.form.getRawValue() as INotebook;
        this.store.dispatch(new AddNotebook(notebook)).pipe(first(),
          tap(() => this.dialogRef.close())).subscribe();
      } 
    }
  }
}
