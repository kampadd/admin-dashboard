import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {ITableData} from '../../../shared/interfaces/i-table-data';
import {INotebook} from '../../../shared/models/notebook.model';
import {merge, Observable, Subscription, EMPTY} from 'rxjs';
import {TableDataSource} from '../../../shared/utils/table-datasource';
import {MatPaginator, MatSort, MatDialog} from '@angular/material';
import {tap, first, switchMap} from 'rxjs/operators';
import {Select, Store} from '@ngxs/store';
import {Dispatch} from '@ngxs-labs/dispatch-decorator';
import {NotebookState} from '../../../store/states/notebook.state';
import {FetchNotebooks, DeleteNotebook} from '../../../store/actions/notebook.action';
import { CreateNotebookComponent } from './create-notebook.component';
import { ConfirmationDialogService } from 'src/app/shared/services/confirmation-dialog.service';

@Component({
  selector: 'app-notebook',
  templateUrl: './notebook.component.html',
  styleUrls: ['./notebook.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ])
  ]
})

export class NotebookComponent implements OnInit, AfterViewInit, OnDestroy {

  private subscription: Subscription;
  private sortSubscription: Subscription;
  public dataSource = new TableDataSource();
  expandedElement: INotebook;
  columnsToDisplay: string[] = ['code', 'description', 'serialNumber', 'supplier', 'brand', 'model', 'assignee', 'technicalReference', 'actions'];

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  @Select(NotebookState.getTableData) notebookTableData$: Observable<ITableData>;
  @Dispatch() private fetchTableData = () => new FetchNotebooks([this.sort.active + ',' + this.sort.direction],
    this.paginator.pageIndex, this.paginator.pageSize);
  

  constructor(private dialog: MatDialog, private store: Store, private confirmationDialogService: ConfirmationDialogService) {}

  ngOnInit() {
    this.fetchTableData();
    this.notebookTableData$.subscribe(data => {
      this.dataSource.update(data);
    });
  }

  ngAfterViewInit() {
    // reset the paginator after sorting
    this.sortSubscription = this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    this.subscription = merge(this.sort.sortChange, this.paginator.page).pipe(
      tap(() => this.fetchTableData())).subscribe();
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.sortSubscription) {
      this.sortSubscription.unsubscribe();
    }
  }

  editNotebook(event: Event, notebook: INotebook): void {
    event.stopPropagation();
    setTimeout(() => {
      this.dialog.open(CreateNotebookComponent, {
        minWidth: '10vw',
        maxWidth: '95vw',
        maxHeight: '95vh',
        disableClose: true,
        data: notebook
      });
    });
  }

  deleteNotebook(event: Event, id: string): void {
    event.stopPropagation();
    this.confirmationDialogService.open(`Do you want to proceed with action`);
    this.confirmationDialogService.dialogRef.afterClosed().pipe(
      first(),
      switchMap(value => {
        if (value) {
          return this.store.dispatch(new DeleteNotebook(id));
        }
        return EMPTY;
      }), switchMap(() => this.store.dispatch(new FetchNotebooks()))).subscribe();
  }
  
  doNothing(event: Event, computer: INotebook): void {
    event.stopPropagation();
    console.log("Yet to be Implemented");
  }
}
