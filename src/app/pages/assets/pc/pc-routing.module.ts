import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PCComponent } from './pc.component';

const routes: Routes = [
  {
    path: '',
    component: PCComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PCRoutingModule { }
