import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {CMDB_SERVER_API} from 'src/app/app.constants';
import {IPC} from 'src/app/shared/models/pc.model';
import {Observable} from 'rxjs';
import moment from 'moment';
import {map} from 'rxjs/operators';
import {createRequestOption} from 'src/app/shared/utils/request-util';
import {ITableData} from '../../../shared/interfaces/i-table-data';

@Injectable({providedIn: 'root'})
export class PCService {
  public resourceUrl = CMDB_SERVER_API + '/api/pcs';

  constructor(private http: HttpClient) {
  }

  create(pc: IPC): Observable<IPC> {
    const copy = this.convertDateFromClient(pc);
    return this.http
      .post<IPC>(this.resourceUrl, copy)
      .pipe(map((result: IPC) => this.convertDateFromServer(result)));
  }

  update(pc: IPC): Observable<IPC> {
    const copy = this.convertDateFromClient(pc);
    return this.http
      .put<IPC>(this.resourceUrl, copy)
      .pipe(map((result: IPC) => this.convertDateFromServer(result)));
  }

  query(req?: any): Observable<ITableData> {
    const options = createRequestOption(req);
    return this.http.get<IPC[]>(this.resourceUrl, {params: options, observe: 'response'}).pipe(
      map((res: HttpResponse<IPC[]>) => {
        return {
          totalCount: parseInt(res.headers.get('X-Total-Count'), 10),
          entries: this.convertDateArrayFromServer(res.body)
        };
      })
    );
  }

  delete(id: string): Observable<any> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`);
  }

  private convertDateFromClient(pc: IPC): IPC {
    return Object.assign({}, pc, {
      beginDate: pc.beginDate != null && pc.beginDate.isValid() ? pc.beginDate.toJSON() : null,
      endDate: pc.endDate != null && pc.endDate.isValid() ? pc.endDate.toJSON() : null,
      purchaseDate: pc.purchaseDate != null && pc.purchaseDate.isValid() ? pc.purchaseDate.toJSON() : null,
      acceptanceDate: pc.acceptanceDate != null && pc.acceptanceDate.isValid() ? pc.purchaseDate.toJSON() : null
    });
  }

  private convertDateFromServer(result: IPC): IPC {
    if (result) {
      result.beginDate = result.beginDate != null ? moment(result.beginDate) : null;
      result.endDate = result.endDate != null ? moment(result.endDate) : null;
      result.purchaseDate = result.purchaseDate != null ? moment(result.purchaseDate) : null;
      result.acceptanceDate = result.acceptanceDate != null ? moment(result.acceptanceDate) : null;
    }
    return result;
  }

  private convertDateArrayFromServer(result: IPC[]): IPC[] {
    if (result) {
      result.forEach((pc: IPC) => {
        pc.beginDate = pc.beginDate != null ? moment(pc.beginDate) : null;
        pc.endDate = pc.endDate != null ? moment(pc.endDate) : null;
        pc.purchaseDate = pc.purchaseDate != null ? moment(pc.purchaseDate) : null;
        pc.acceptanceDate = pc.acceptanceDate != null ? moment(pc.acceptanceDate) : null;
      });
    }
    return result;
  }
}
