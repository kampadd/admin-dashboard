import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {IPC} from 'src/app/shared/models/pc.model';
import {AddPC, UpdatePC} from 'src/app/store/actions/pc.action';
import {first, tap} from 'rxjs/operators';
import {Store} from '@ngxs/store';
import moment from 'moment';

@Component({
  selector: 'app-create-pc',
  templateUrl: './create-pc.component.html'
})
export class CreatePcComponent implements OnInit {
  form: FormGroup;
  today = moment();

  constructor(@Inject(MAT_DIALOG_DATA) public pc: IPC,
              private fb: FormBuilder,
              private store: Store,
              private dialogRef: MatDialogRef<CreatePcComponent>) {
  }

  ngOnInit() {
    this.createForm();
    this.fillForm();
  }

  private createForm(): void {
    this.form = this.fb.group({
      idClass: [''],
      code: [''],
      description: [''],
      status: [''],
      user: [''],
      beginDate: ['', [Validators.required]],
      notes: [''],
      endDate: ['', [Validators.required]],
      currentId: [''],
      idTenant: [''],
      serialNumber: [''],
      supplier: [''],
      purchaseDate: ['', [Validators.required]],
      acceptanceDate: ['', [Validators.required]],
      finalCost: [''],
      brand: [''],
      model: [''],
      room: [''],
      assignee: [''],
      technicalReference: [''],
      workplace: [''],
      acceptanceNotes: [''],
      ram: [''],
      cpuNumber: [''],
      cpuSpeed: [''],
      hdSize: [''],
      ipAddress: [null,
        [
          Validators.pattern(
            '(^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])$)'
          )
        ]
      ],
      soundCard: [''],
      videoCard: ['']
    });
  }

  private fillForm(): void {
    if (this.pc) {
      this.form.patchValue(this.pc || {});
    }
  }

  onSubmit(): void {
    if (this.form.valid) {
      if (this.pc && this.pc.id) {
        const pc = JSON.parse(JSON.stringify(this.pc)) as IPC;
        pc.idClass = this.form.get('idClass').value;
        pc.code = this.form.get('code').value;
        pc.description = this.form.get('description').value;
        pc.status = this.form.get('status').value;
        pc.user = this.form.get('user').value;
        pc.beginDate = this.form.get('beginDate').value;
        pc.notes = this.form.get('notes').value;
        pc.endDate = this.form.get('endDate').value;
        pc.currentId = this.form.get('currentId').value;
        pc.idTenant = this.form.get('idTenant').value;
        pc.serialNumber = this.form.get('serialNumber').value;
        pc.supplier = this.form.get('supplier').value;
        pc.purchaseDate = this.form.get('purchaseDate').value;
        pc.acceptanceDate = this.form.get('acceptanceDate').value;
        pc.finalCost = this.form.get('finalCost').value;
        pc.brand = this.form.get('brand').value;
        pc.model = this.form.get('model').value;
        pc.room = this.form.get('room').value;
        pc.assignee = this.form.get('assignee').value;
        pc.technicalReference = this.form.get('technicalReference').value;
        pc.workplace = this.form.get('workplace').value;
        pc.acceptanceNotes = this.form.get('acceptanceNotes').value;
        pc.ram = this.form.get('ram').value;
        pc.cpuNumber = this.form.get('cpuNumber').value;
        pc.hdSize = this.form.get('hdSize').value;
        pc.ipAddress = this.form.get('ipAddress').value;
        pc.soundCard = this.form.get('soundCard').value;
        pc.videoCard = this.form.get('videoCard').value;

        this.store.dispatch(new UpdatePC(pc)).pipe(first(),
          tap(() => this.dialogRef.close())).subscribe();
      } else {
        const pc = this.form.getRawValue() as IPC;
        this.store.dispatch(new AddPC(pc)).pipe(first(),
          tap(() => this.dialogRef.close())).subscribe();
      }
    }
  }
}
