import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {IPC} from 'src/app/shared/models/pc.model';
import {EMPTY, merge, Observable, Subscription} from 'rxjs';
import {TableDataSource} from '../../../shared/utils/table-datasource';
import {MatDialog, MatPaginator, MatSort} from '@angular/material';
import {Select, Store} from '@ngxs/store';
import {ITableData} from '../../../shared/interfaces/i-table-data';
import {Dispatch} from '@ngxs-labs/dispatch-decorator';
import {ConfirmationDialogService} from '../../../shared/services/confirmation-dialog.service';
import {first, switchMap, tap} from 'rxjs/operators';
import {PCState} from '../../../store/states/pc.state';
import {DeletePC, FetchPCs} from '../../../store/actions/pc.action';
import { CreatePcComponent } from './create-pc.component';

/**
 * @title Table with expandable rows
 */
@Component({
  selector: 'app-pc',
  templateUrl: './pc.component.html',
  styleUrls: ['./pc.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class PCComponent implements OnInit, OnDestroy, AfterViewInit {
  private subscription: Subscription;
  private sortSubscription: Subscription;
  public dataSource = new TableDataSource();
  columnsToDisplay: string[] = ['code', 'description', 'serialNumber', 'supplier', 'brand', 'model', 'assignee', 'technicalReference', 'actions'];
  expandedElement: IPC;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  @Select(PCState.getTableData) pcTableData: Observable<ITableData>;
  @Dispatch() private fetchTableData = () => new FetchPCs([this.sort.active + ',' + this.sort.direction],
    this.paginator.pageIndex, this.paginator.pageSize);

  constructor(private dialog: MatDialog, private store: Store, private confirmationDialogService: ConfirmationDialogService) {
  }

  ngOnInit() {
    this.fetchTableData();
    this.pcTableData.subscribe(data => {
      this.dataSource.update(data);
    });
  }

  ngAfterViewInit() {
    // reset the paginator after sorting
    this.sortSubscription = this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    this.subscription = merge(this.sort.sortChange, this.paginator.page).pipe(
      tap(() => this.fetchTableData())).subscribe();
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.sortSubscription) {
      this.sortSubscription.unsubscribe();
    }
  }

  editPC(event: Event, pc: IPC): void {
    event.stopPropagation();
    setTimeout(() => {
      this.dialog.open(CreatePcComponent, {
        minWidth: '10vw',
        maxWidth: '95vw',
        maxHeight: '95vh',
        disableClose: true,
        data: pc 
      });
    });
  }

  deletePC(event: Event, id: string): void {
    event.stopPropagation();
    this.confirmationDialogService.open(`Do you want to proceed with action`);
    this.confirmationDialogService.dialogRef.afterClosed().pipe(
      first(),
      switchMap(value => {
        if (value) {
          return this.store.dispatch(new DeletePC(id));
        }
        return EMPTY;
      }), switchMap(() => this.store.dispatch(new FetchPCs()))).subscribe();
  }

  doNothing(event: Event, pc: IPC): void {
    event.stopPropagation();
    console.log("Yet to be Implemented");
  }
}
