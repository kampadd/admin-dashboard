import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {PCRoutingModule} from './pc-routing.module';
import {PCComponent} from './pc.component';
import {
  MatCardModule,
  MatGridListModule,
  MatIconModule,
  MatPaginatorModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatButtonModule,
  MatListModule,
  MatDialogModule,
  MatFormFieldModule,
  MatInputModule,
  MatDatepickerModule
} from '@angular/material';
import {TranslateModule} from '@ngx-translate/core';
import {NgxsModule} from '@ngxs/store';
import {PCState} from '../../../store/states/pc.state';
import { CreatePcComponent } from './create-pc.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatMomentDateModule } from '@angular/material-moment-adapter';

@NgModule({
  declarations: [PCComponent, CreatePcComponent],
  imports: [
    CommonModule,
    PCRoutingModule,
    MatTableModule,
    MatIconModule,
    MatCardModule,
    MatPaginatorModule,
    MatTabsModule,
    MatSortModule,
    MatGridListModule,
    MatButtonModule,
    TranslateModule,
    MatListModule,
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatMomentDateModule,
    NgxsModule.forFeature([
      PCState
    ])
  ], 
  entryComponents: [CreatePcComponent]
})
export class PCModule {
}
