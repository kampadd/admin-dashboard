import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {CMDB_SERVER_API} from 'src/app/app.constants';
import {IUPS} from 'src/app/shared/models/ups.model';
import {Observable} from 'rxjs';
import moment from 'moment';
import {map} from 'rxjs/operators';
import {createRequestOption} from 'src/app/shared/utils/request-util';
import {ITableData} from '../../../shared/interfaces/i-table-data';

@Injectable({providedIn: 'root'})
export class UpsService {
  public resourceUrl = CMDB_SERVER_API + '/api/ups';

  constructor(private http: HttpClient) {
  }

  create(ups: IUPS): Observable<IUPS> {
    const copy = this.convertDateFromClient(ups);
    return this.http
      .post<IUPS>(this.resourceUrl, copy)
      .pipe(map((result: IUPS) => this.convertDateFromServer(result)));
  }

  update(ups: IUPS): Observable<IUPS> {
    const copy = this.convertDateFromClient(ups);
    return this.http
      .put<IUPS>(this.resourceUrl, copy)
      .pipe(map((result: IUPS) => this.convertDateFromServer(result)));
  }

  query(req?: any): Observable<ITableData> {
    const options = createRequestOption(req);
    return this.http.get<IUPS[]>(this.resourceUrl, {params: options, observe: 'response'}).pipe(
      map((res: HttpResponse<IUPS[]>) => {
        return {
          totalCount: parseInt(res.headers.get('X-Total-Count'), 10),
          entries: this.convertDateArrayFromServer(res.body)
        };
      })
    );
  }

  delete(id: string): Observable<any> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`);
  }

  private convertDateFromClient(ups: IUPS): IUPS {
    return Object.assign({}, ups, {
      beginDate: ups.beginDate != null && ups.beginDate.isValid() ? ups.beginDate.toJSON() : null,
      endDate: ups.endDate != null && ups.endDate.isValid() ? ups.endDate.toJSON() : null,
      purchaseDate: ups.purchaseDate != null && ups.purchaseDate.isValid() ? ups.purchaseDate.toJSON() : null,
      acceptanceDate: ups.acceptanceDate != null && ups.acceptanceDate.isValid() ? ups.purchaseDate.toJSON() : null
    });
  }

  private convertDateFromServer(result: IUPS): IUPS {
    if (result) {
      result.beginDate = result.beginDate != null ? moment(result.beginDate) : null;
      result.endDate = result.endDate != null ? moment(result.endDate) : null;
      result.purchaseDate = result.purchaseDate != null ? moment(result.purchaseDate) : null;
      result.acceptanceDate = result.acceptanceDate != null ? moment(result.acceptanceDate) : null;
    }
    return result;
  }

  private convertDateArrayFromServer(result: IUPS[]): IUPS[] {
    if (result) {
      result.forEach((ups: IUPS) => {
        ups.beginDate = ups.beginDate != null ? moment(ups.beginDate) : null;
        ups.endDate = ups.endDate != null ? moment(ups.endDate) : null;
        ups.purchaseDate = ups.purchaseDate != null ? moment(ups.purchaseDate) : null;
        ups.acceptanceDate = ups.acceptanceDate != null ? moment(ups.acceptanceDate) : null;
      });
    }
    return result;
  }
}
