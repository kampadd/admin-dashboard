import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {IUPS} from 'src/app/shared/models/ups.model';
import {Store} from '@ngxs/store';
import moment from 'moment';

@Component({
  selector: 'app-create-ups',
  templateUrl: './create-ups.component.html'
})
export class CreateUpsComponent implements OnInit {
  form: FormGroup;
  today = moment().toDate();

  constructor(@Inject(MAT_DIALOG_DATA) public ups: IUPS,
              private fb: FormBuilder,
              private store: Store,
              private dialogRef: MatDialogRef<CreateUpsComponent>) {
  }

  ngOnInit() {
    this.createForm();
    this.fillForm();
  }

  private createForm(): void {
    this.form = this.fb.group({
      idClass: [''],
      code: [''],
      description: [''],
      status: [''],
      user: [''],
      beginDate: ['', [Validators.required]],
      notes: [''],
      endDate: ['', [Validators.required]],
      currentId: [''],
      idTenant: [''],
      serialNumber: [''],
      supplier: [''],
      purchaseDate: ['', [Validators.required]],
      acceptanceDate: ['', [Validators.required]],
      finalCost: [''],
      brand: [''],
      model: [''],
      room: [''],
      assignee: [''],
      technicalReference: [''],
      workplace: [''],
      acceptanceNotes: [''],
      power: ['']
    });
  }

  private fillForm(): void {
    if (this.ups) {
      this.form.patchValue(this.ups || {});
    }
  }

  onSubmit(): void {
    if (this.form.valid) {
      if (this.ups && this.ups.id) {
        const ups = JSON.parse(JSON.stringify(this.ups)) as IUPS;
        ups.idClass = this.form.get('idClass').value;
        ups.code = this.form.get('code').value;
        ups.description = this.form.get('description').value;
        ups.status = this.form.get('status').value;
        ups.user = this.form.get('user').value;
        ups.beginDate = this.form.get('beginDate').value;
        ups.notes = this.form.get('notes').value;
        ups.endDate = this.form.get('endDate').value;
        ups.currentId = this.form.get('currentId').value;
        ups.idTenant = this.form.get('idTenant').value;
        ups.serialNumber = this.form.get('serialNumber').value;
        ups.supplier = this.form.get('supplier').value;
        ups.purchaseDate = this.form.get('purchaseDate').value;
        ups.acceptanceDate = this.form.get('acceptanceDate').value;
        ups.finalCost = this.form.get('finalCost').value;
        ups.brand = this.form.get('brand').value;
        ups.model = this.form.get('model').value;
        ups.room = this.form.get('room').value;
        ups.assignee = this.form.get('assignee').value;
        ups.technicalReference = this.form.get('technicalReference').value;
        ups.workplace = this.form.get('workplace').value;
        ups.acceptanceNotes = this.form.get('acceptanceNotes').value;
        ups.power = this.form.get('power').value;
      } else {
      }
    }
  }

}
