import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UpsRoutingModule } from './ups-routing.module';
import { UpsComponent } from './ups.component';
import {MatCardModule, MatIconModule, 
  MatPaginatorModule, MatSortModule, 
  MatTableModule, MatTabsModule, MatGridListModule, MatButtonModule, MatListModule, MatDialogModule, MatFormFieldModule, MatInputModule, MatDatepickerModule} from '@angular/material';
import {NgxsModule} from '@ngxs/store';
import {UpsState} from '../../../store/states/ups.state';
import { TranslateModule } from '@ngx-translate/core';
import { CreateUpsComponent } from './create-ups.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatMomentDateModule } from '@angular/material-moment-adapter';

@NgModule({
  declarations: [UpsComponent, CreateUpsComponent],
  imports: [
    CommonModule,
    UpsRoutingModule,
    TranslateModule,
    MatTableModule,
    MatIconModule,
    MatCardModule,
    MatPaginatorModule,
    MatTabsModule,
    MatSortModule,
    MatButtonModule,
    MatGridListModule,
    MatListModule,
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatMomentDateModule,
    NgxsModule.forFeature([
      UpsState
    ])
  ],
  entryComponents: [CreateUpsComponent]
})
export class UpsModule { }
