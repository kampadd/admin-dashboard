import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UpsComponent } from './ups.component';

const routes: Routes = [
  {
    path: '',
    component: UpsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UpsRoutingModule { }
