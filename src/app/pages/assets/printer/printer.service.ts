import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {IPrinter} from 'src/app/shared/models/printer.model';
import {Observable} from 'rxjs';
import moment from 'moment';
import {map} from 'rxjs/operators';
import {createRequestOption} from 'src/app/shared/utils/request-util';
import {ITableData} from '../../../shared/interfaces/i-table-data';
import {CMDB_SERVER_API} from '../../../app.constants';

@Injectable({providedIn: 'root'})
export class PrinterService {
  public resourceUrl = CMDB_SERVER_API + '/api/printers';

  constructor(private http: HttpClient) {
  }

  create(printer: IPrinter): Observable<IPrinter> {
    const copy = this.convertDateFromClient(printer);
    return this.http
      .post<IPrinter>(this.resourceUrl, copy)
      .pipe(map((result: IPrinter) => this.convertDateFromServer(result)));
  }

  update(printer: IPrinter): Observable<IPrinter> {
    const copy = this.convertDateFromClient(printer);
    return this.http
      .put<IPrinter>(this.resourceUrl, copy)
      .pipe(map((result: IPrinter) => this.convertDateFromServer(result)));
  }

  query(req?: any): Observable<ITableData> {
    const options = createRequestOption(req);
    return this.http.get<IPrinter[]>(this.resourceUrl, {params: options, observe: 'response'}).pipe(
      map((res: HttpResponse<IPrinter[]>) => {
        return {
          totalCount: parseInt(res.headers.get('X-Total-Count'), 10),
          entries: this.convertDateArrayFromServer(res.body)
        };
      })
    );
  }

  delete(id: string): Observable<any> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`);
  }

  private convertDateFromClient(printer: IPrinter): IPrinter {
    return Object.assign({}, printer, {
      beginDate: printer.beginDate != null && printer.beginDate.isValid() ? printer.beginDate.toJSON() : null,
      endDate: printer.endDate != null && printer.endDate.isValid() ? printer.endDate.toJSON() : null,
      purchaseDate: printer.purchaseDate != null && printer.purchaseDate.isValid() ? printer.purchaseDate.toJSON() : null,
      acceptanceDate: printer.acceptanceDate != null && printer.acceptanceDate.isValid() ? printer.purchaseDate.toJSON() : null
    });
  }

  private convertDateFromServer(result: IPrinter): IPrinter {
    if (result) {
      result.beginDate = result.beginDate != null ? moment(result.beginDate) : null;
      result.endDate = result.endDate != null ? moment(result.endDate) : null;
      result.purchaseDate = result.purchaseDate != null ? moment(result.purchaseDate) : null;
      result.acceptanceDate = result.acceptanceDate != null ? moment(result.acceptanceDate) : null;
    }
    return result;
  }

  private convertDateArrayFromServer(result: IPrinter[]): IPrinter[] {
    if (result) {
      result.forEach((printer: IPrinter) => {
        printer.beginDate = printer.beginDate != null ? moment(printer.beginDate) : null;
        printer.endDate = printer.endDate != null ? moment(printer.endDate) : null;
        printer.purchaseDate = printer.purchaseDate != null ? moment(printer.purchaseDate) : null;
        printer.acceptanceDate = printer.acceptanceDate != null ? moment(printer.acceptanceDate) : null;
      });
    }
    return result;
  }
}
