import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Store} from '@ngxs/store';
import {IPrinter} from 'src/app/shared/models/printer.model';
import {AddPrinter, UpdatePrinter} from 'src/app/store/actions/printer.action';
import {first, tap} from 'rxjs/operators';
import moment from 'moment';

@Component({
  selector: 'app-create-printer',
  templateUrl: './create-printer.component.html'
})
export class CreatePrinterComponent implements OnInit {
  form: FormGroup;
  today = moment();

  constructor(@Inject(MAT_DIALOG_DATA) public printer: IPrinter,
              private fb: FormBuilder,
              private store: Store,
              private dialogRef: MatDialogRef<CreatePrinterComponent>) {
  }

  ngOnInit() {
    this.createForm();
    this.fillForm();
  }

  private createForm(): void {
    this.form = this.fb.group({
      idClass: [''],
      code: [''],
      description: [''],
      status: [''],
      user: [''],
      beginDate: ['', [Validators.required]],
      notes: [''],
      endDate: ['', [Validators.required]],
      currentId: [''],
      idTenant: [''],
      serialNumber: [''],
      supplier: [''],
      purchaseDate: ['', [Validators.required]],
      acceptanceDate: ['', [Validators.required]],
      finalCost: [''],
      brand: [''],
      model: [''],
      room: [''],
      assignee: [''],
      technicalReference: [''],
      workplace: [''],
      acceptanceNotes: [''],
      type: [''],
      paperSize: [''],
      color: [''],
      usage: ['']
    });
  }

  private fillForm(): void {
    if (this.printer) {
      this.form.patchValue(this.printer || {});
    }
  }

  onSubmit(): void {
    if (this.form.valid) {
      if (this.printer && this.printer.id) {
        const printer = JSON.parse(JSON.stringify(this.printer)) as IPrinter;
        printer.idClass = this.form.get('idClass').value;
        printer.code = this.form.get('code').value;
        printer.description = this.form.get('description').value;
        printer.status = this.form.get('status').value;
        printer.user = this.form.get('user').value;
        printer.beginDate = this.form.get('beginDate').value;
        printer.notes = this.form.get('notes').value;
        printer.endDate = this.form.get('endDate').value;
        printer.currentId = this.form.get('currentId').value;
        printer.idTenant = this.form.get('idTenant').value;
        printer.serialNumber = this.form.get('serialNumber').value;
        printer.supplier = this.form.get('supplier').value;
        printer.purchaseDate = this.form.get('purchaseDate').value;
        printer.acceptanceDate = this.form.get('acceptanceDate').value;
        printer.finalCost = this.form.get('finalCost').value;
        printer.brand = this.form.get('brand').value;
        printer.model = this.form.get('model').value;
        printer.room = this.form.get('room').value;
        printer.assignee = this.form.get('assignee').value;
        printer.technicalReference = this.form.get('technicalReference').value;
        printer.workplace = this.form.get('workplace').value;
        printer.acceptanceNotes = this.form.get('acceptanceNotes').value;
        printer.type = this.form.get('type').value;
        printer.paperSize = this.form.get('paperSize').value;
        printer.color = this.form.get('color').value;
        printer.usage = this.form.get('usage').value;

        this.store.dispatch(new UpdatePrinter(printer)).pipe(first(),
          tap(() => this.dialogRef.close())).subscribe();
      } else {
        const printer = this.form.getRawValue() as IPrinter;
        this.store.dispatch(new AddPrinter(printer)).pipe(first(),
          tap(() => this.dialogRef.close())).subscribe();
      }
    }
  }

}
