import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {PrinterRoutingModule} from './printer-routing.module';
import {PrinterComponent} from './printer.component';
import {
  MatCardModule,
  MatGridListModule,
  MatIconModule,
  MatPaginatorModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatButtonModule,
  MatListModule,
  MatFormFieldModule,
  MatInputModule,
  MatDialogModule,
  MatDatepickerModule
} from '@angular/material';
import {NgxsModule} from '@ngxs/store';
import {PrinterState} from '../../../store/states/printer.state';
import { TranslateModule } from '@ngx-translate/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { CreatePrinterComponent } from './create-printer.component';

@NgModule({
  declarations: [PrinterComponent, CreatePrinterComponent],
  imports: [
    CommonModule,
    PrinterRoutingModule,
    MatTableModule,
    MatIconModule,
    MatCardModule,
    MatPaginatorModule,
    MatTabsModule,
    MatSortModule,
    MatGridListModule,
    TranslateModule,
    MatButtonModule,
    MatListModule,
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatMomentDateModule,
    NgxsModule.forFeature([
      PrinterState
    ])
  ],
  entryComponents: [CreatePrinterComponent]
})
export class PrinterModule {
}
