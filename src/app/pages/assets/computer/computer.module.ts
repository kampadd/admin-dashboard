import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ComputerRoutingModule} from './computer-routing.module';
import {ComputerComponent} from './computer.component';
import {
  MatButtonModule,
  MatCardModule,
  MatGridListModule,
  MatIconModule,
  MatPaginatorModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatListModule,
  MatDialogModule,
  MatFormFieldModule,
  MatInputModule,
  MatDatepickerModule
} from '@angular/material';
import {NgxsModule} from '@ngxs/store';
import {ComputerState} from '../../../store/states/computer.state';
import {TranslateModule} from '@ngx-translate/core';
import { CreateComputerComponent } from './create-computer.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatMomentDateModule } from '@angular/material-moment-adapter';

@NgModule({
  declarations: [ComputerComponent, CreateComputerComponent],
  imports: [
    CommonModule,
    ComputerRoutingModule,
    MatButtonModule,
    TranslateModule,
    MatTableModule,
    MatIconModule,
    MatCardModule,
    MatPaginatorModule,
    MatTabsModule,
    MatSortModule,
    MatGridListModule,
    MatListModule,
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatMomentDateModule,
    NgxsModule.forFeature([
      ComputerState
    ])
  ],
  entryComponents: [CreateComputerComponent]
})
export class ComputerModule {
} 
