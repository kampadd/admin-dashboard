import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {CMDB_SERVER_API} from 'src/app/app.constants';
import {IComputer} from 'src/app/shared/models/computer.model';
import {Observable} from 'rxjs';
import moment from 'moment';
import {map} from 'rxjs/operators';
import {createRequestOption} from 'src/app/shared/utils/request-util';
import {ITableData} from '../../../shared/interfaces/i-table-data';

@Injectable({providedIn: 'root'})
export class ComputerService {
  public resourceUrl = CMDB_SERVER_API + '/api/computers';

  constructor(private http: HttpClient) {
  }

  create(computer: IComputer): Observable<IComputer> {
    const copy = this.convertDateFromClient(computer);
    return this.http
      .post<IComputer>(this.resourceUrl, copy)
      .pipe(map((result: IComputer) => this.convertDateFromServer(result)));
  }

  update(computer: IComputer): Observable<IComputer> {
    const copy = this.convertDateFromClient(computer);
    return this.http
      .put<IComputer>(this.resourceUrl, copy)
      .pipe(map((result: IComputer) => this.convertDateFromServer(result)));
  }

  query(req?: any): Observable<ITableData> {
    const options = createRequestOption(req);
    return this.http.get<IComputer[]>(this.resourceUrl, {params: options, observe: 'response'}).pipe(
      map((res: HttpResponse<IComputer[]>) => {
        return {
          totalCount: parseInt(res.headers.get('X-Total-Count'), 10),
          entries: this.convertDateArrayFromServer(res.body)
        };
      })
    );
  }

  delete(id: string): Observable<any> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`);
  }

  private convertDateFromClient(computer: IComputer): IComputer {
    return Object.assign({}, computer, {
      beginDate: computer.beginDate != null && computer.beginDate.isValid() ? computer.beginDate.toJSON() : null,
      endDate: computer.endDate != null && computer.endDate.isValid() ? computer.endDate.toJSON() : null,
      purchaseDate: computer.purchaseDate != null && computer.purchaseDate.isValid() ? computer.purchaseDate.toJSON() : null,
      acceptanceDate: computer.acceptanceDate != null && computer.acceptanceDate.isValid() ? computer.purchaseDate.toJSON() : null
    });
  }

  private convertDateFromServer(result: IComputer): IComputer {
    if (result) {
      result.beginDate = result.beginDate != null ? moment(result.beginDate) : null;
      result.endDate = result.endDate != null ? moment(result.endDate) : null;
      result.purchaseDate = result.purchaseDate != null ? moment(result.purchaseDate) : null;
      result.acceptanceDate = result.acceptanceDate != null ? moment(result.acceptanceDate) : null;
    }
    return result;
  }

  private convertDateArrayFromServer(result: IComputer[]): IComputer[] {
    if (result) {
      result.forEach((computer: IComputer) => {
        computer.beginDate = computer.beginDate != null ? moment(computer.beginDate) : null;
        computer.endDate = computer.endDate != null ? moment(computer.endDate) : null;
        computer.purchaseDate = computer.purchaseDate != null ? moment(computer.purchaseDate) : null;
        computer.acceptanceDate = computer.acceptanceDate != null ? moment(computer.acceptanceDate) : null;
      });
    }
    return result;
  }
}
