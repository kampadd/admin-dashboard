import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {IComputer} from 'src/app/shared/models/computer.model';
import {AddComputer, UpdateComputer} from 'src/app/store/actions/computer.action';
import {Store} from '@ngxs/store';
import {first, tap} from 'rxjs/operators';
import moment from 'moment';

@Component({
  selector: 'app-create-computer',
  templateUrl: './create-computer.component.html'
})
export class CreateComputerComponent implements OnInit {
  form: FormGroup;
  today = moment();

  constructor(@Inject(MAT_DIALOG_DATA) public computer: IComputer,
              private fb: FormBuilder,
              private store: Store,
              private dialogRef: MatDialogRef<CreateComputerComponent>) {
  }

  ngOnInit() {
    this.createForm();
    this.fillForm();
  }

  private createForm(): void {
    this.form = this.fb.group({
      idClass: [''],
      code: [''],
      description: [''],
      status: [''],
      user: [''],
      beginDate: ['', [Validators.required]],
      notes: [''],
      endDate: ['', [Validators.required]],
      currentId: [''],
      idTenant: [''],
      serialNumber: [''],
      supplier: [''],
      purchaseDate: ['', [Validators.required]],
      acceptanceDate: ['', [Validators.required]],
      finalCost: [''],
      brand: [''],
      model: [''],
      room: [''],
      assignee: [''],
      technicalReference: [''],
      workplace: [''],
      acceptanceNotes: [''],
      ram: [''],
      cpuNumber: [''],
      cpuSpeed: [''],
      hdSize: [''],
      ipAddress: [null,
        [
          Validators.pattern(
            '(^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])$)'
          )
        ]
      ]
    });
  }

  private fillForm(): void {
    if (this.computer) {
      this.form.patchValue(this.computer || {});
    }
  }

  onSubmit(): void {
    if (this.form.valid) {
      if (this.computer && this.computer.id) {
        const computer = JSON.parse(JSON.stringify(this.computer)) as IComputer;
        computer.idClass = this.form.get('idClass').value;
        computer.code = this.form.get('code').value;
        computer.description = this.form.get('description').value;
        computer.status = this.form.get('status').value;
        computer.user = this.form.get('user').value;
        computer.beginDate = this.form.get('beginDate').value;
        computer.notes = this.form.get('notes').value;
        computer.endDate = this.form.get('endDate').value;
        computer.currentId = this.form.get('currentId').value;
        computer.idTenant = this.form.get('idTenant').value;
        computer.serialNumber = this.form.get('serialNumber').value;
        computer.supplier = this.form.get('supplier').value;
        computer.purchaseDate = this.form.get('purchaseDate').value;
        computer.acceptanceDate = this.form.get('acceptanceDate').value;
        computer.finalCost = this.form.get('finalCost').value;
        computer.brand = this.form.get('brand').value;
        computer.model = this.form.get('model').value;
        computer.room = this.form.get('room').value;
        computer.assignee = this.form.get('assignee').value;
        computer.technicalReference = this.form.get('technicalReference').value;
        computer.workplace = this.form.get('workplace').value;
        computer.acceptanceNotes = this.form.get('acceptanceNotes').value;
        computer.ram = this.form.get('ram').value;
        computer.cpuNumber = this.form.get('cpuNumber').value;
        computer.hdSize = this.form.get('hdSize').value;
        computer.ipAddress = this.form.get('ipAddress').value;

        this.store.dispatch(new UpdateComputer(computer)).pipe(first(),
          tap(() => this.dialogRef.close())).subscribe();
      } else {
        const computer = this.form.getRawValue() as IComputer;
        this.store.dispatch(new AddComputer(computer)).pipe(first(),
          tap(() => this.dialogRef.close())).subscribe();
      }
    }
  }

}
