import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { IAsset } from 'src/app/shared/models/asset.model';
import { Store } from '@ngxs/store';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UpdateAsset, AddAsset } from 'src/app/store/actions/asset.action';
import { first, tap } from 'rxjs/operators';

@Component({
  selector: 'app-create-asset',
  templateUrl: './create-asset.component.html'
})
export class CreateAssetComponent implements OnInit {

  form: FormGroup;


  constructor(@Inject(MAT_DIALOG_DATA) public asset: IAsset,
              private store: Store,
              private fb:FormBuilder,
              private dialogRef: MatDialogRef<CreateAssetComponent>) { }

  ngOnInit() {
    this.createForm();
    this.fillForm();
  }

  private createForm(): void {
    this.form = this.fb.group({
      idClass: [''],
      code: [''],
      description: [''],
      status: [''],
      user: [''],
      beginDate: ['', [Validators.required]],
      notes: [''],
      endDate: ['', [Validators.required]],
      currentId: [''],
      idTenant: [''],
      serialNumber: [''],
      supplier: [''],
      purchaseDate: ['', [Validators.required]],
      acceptanceDate: ['', [Validators.required]],
      finalCost: [''],
      brand: [''],
      model: [''],
      room: [''],
      assignee: [''],
      technicalReference: [''],
      workplace: [''],
      acceptanceNotes: ['']
    });
  }

  onSubmit(): void {
    if (this.form.valid) {
      if (this.asset && this.asset.id) {
        const asset = JSON.parse(JSON.stringify(this.asset)) as IAsset;
        asset.idClass = this.form.get('idClass').value;
        asset.code = this.form.get('code').value;
        asset.description = this.form.get('description').value;
        asset.status = this.form.get('status').value;
        asset.user = this.form.get('user').value;
        asset.beginDate = this.form.get('beginDate').value;
        asset.notes = this.form.get('notes').value;
        asset.endDate = this.form.get('endDate').value;
        asset.currentId = this.form.get('currentId').value;
        asset.idTenant = this.form.get('idTenant').value;
        asset.serialNumber = this.form.get('serialNumber').value;
        asset.supplier = this.form.get('supplier').value;
        asset.purchaseDate = this.form.get('purchaseDate').value;
        asset.acceptanceDate = this.form.get('acceptanceDate').value;
        asset.finalCost = this.form.get('finalCost').value;
        asset.brand = this.form.get('brand').value;
        asset.model = this.form.get('model').value;
        asset.room = this.form.get('room').value;
        asset.assignee = this.form.get('assignee').value;
        asset.technicalReference = this.form.get('technicalReference').value;
        asset.workplace = this.form.get('workplace').value;
        asset.acceptanceNotes = this.form.get('acceptanceNotes').value;


        this.store.dispatch(new UpdateAsset(asset)).pipe(first(),
          tap(() => this.dialogRef.close())).subscribe();
      } else {
        const asset = this.form.getRawValue() as IAsset;
        this.store.dispatch(new AddAsset(asset)).pipe(first(),
          tap(() => this.dialogRef.close())).subscribe();
      }
    }
  }

  private fillForm(): void {
    if (this.asset) {
      this.form.patchValue(this.asset || {});
    }
  }
}
