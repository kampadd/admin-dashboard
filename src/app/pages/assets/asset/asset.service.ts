import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {CMDB_SERVER_API} from 'src/app/app.constants';
import {IAsset} from 'src/app/shared/models/asset.model';
import moment from 'moment';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {createRequestOption} from 'src/app/shared/utils/request-util';
import {ITableData} from 'src/app/shared/interfaces/i-table-data';


@Injectable({providedIn: 'root'})
export class AssetService {
  public resourceUrl = CMDB_SERVER_API + '/api/assets';

  constructor(private http: HttpClient) {
  }

  create(asset: IAsset): Observable<IAsset> {
    const copy = this.convertDateFromClient(asset);
    return this.http
      .post<IAsset>(this.resourceUrl, copy)
      .pipe(map((result: IAsset) => this.convertDateFromServer(result)));
  }

  update(asset: IAsset): Observable<IAsset> {
    const copy = this.convertDateFromClient(asset);
    return this.http
      .put<IAsset>(this.resourceUrl, copy)
      .pipe(map((result: IAsset) => this.convertDateFromServer(result)));
  }

  query(req?: any): Observable<ITableData> {
    const options = createRequestOption(req);
    return this.http.get<IAsset[]>(this.resourceUrl, {params: options, observe: 'response'}).pipe(
      map((result: HttpResponse<IAsset[]>) => {
        return {
          totalCount: parseInt(result.headers.get('X-Total-Count'), 10),
          entries: this.convertDateArrayFromServer(result.body)
        };
      })
    );
  }

  delete(id: string): Observable<any> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`);
  }


  private convertDateFromClient(asset: IAsset): IAsset {
    return Object.assign({}, asset, {
      beginDate: asset.beginDate != null && asset.beginDate.isValid() ? asset.beginDate.toJSON() : null,
      endDate: asset.endDate != null && asset.endDate.isValid() ? asset.endDate.toJSON() : null,
      purchaseDate: asset.purchaseDate != null && asset.purchaseDate.isValid() ? asset.purchaseDate.toJSON() : null,
      acceptanceDate: asset.acceptanceDate != null && asset.acceptanceDate.isValid() ? asset.purchaseDate.toJSON() : null
    });
  }

  private convertDateFromServer(result: IAsset): IAsset {
    if (result) {
      result.beginDate = result.beginDate != null ? moment(result.beginDate) : null;
      result.endDate = result.endDate != null ? moment(result.endDate) : null;
      result.purchaseDate = result.purchaseDate != null ? moment(result.purchaseDate) : null;
      result.acceptanceDate = result.acceptanceDate != null ? moment(result.acceptanceDate) : null;
    }
    return result;
  }

  private convertDateArrayFromServer(result: IAsset[]): IAsset[] {
    if (result) {
      result.forEach((asset: IAsset) => {
        asset.beginDate = asset.beginDate != null ? moment(asset.beginDate) : null;
        asset.endDate = asset.endDate != null ? moment(asset.endDate) : null;
        asset.purchaseDate = asset.purchaseDate != null ? moment(asset.purchaseDate) : null;
        asset.acceptanceDate = asset.acceptanceDate != null ? moment(asset.acceptanceDate) : null;
      });
    }
    return result;
  }
}
