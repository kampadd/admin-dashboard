import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {AssetRoutingModule} from './asset-routing.module';
import {AssetComponent} from './asset.component';
import {
  MatButtonModule,
  MatCardModule,
  MatGridListModule,
  MatIconModule,
  MatPaginatorModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatFormFieldModule,
  MatInputModule
} from '@angular/material';
import {TranslateModule} from '@ngx-translate/core';
import {NgxsModule} from '@ngxs/store';
import {AssetState} from '../../../store/states/asset.state';
import { CreateAssetComponent } from './create-asset.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatMomentDateModule } from '@angular/material-moment-adapter';


@NgModule({
  declarations: [AssetComponent, CreateAssetComponent],
  imports: [
    CommonModule,
    AssetRoutingModule,
    TranslateModule,
    FormsModule,
    ReactiveFormsModule,
    MatMomentDateModule,
    MatTableModule,
    MatIconModule,
    MatCardModule,
    MatPaginatorModule,
    MatSortModule,
    MatButtonModule,
    MatTabsModule,
    MatGridListModule,
    MatDatepickerModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    NgxsModule.forFeature([AssetState])
  ],
  entryComponents: [CreateAssetComponent]
})
export class AssetModule {
}
