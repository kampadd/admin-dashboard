import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {MonitorRoutingModule} from './monitor-routing.module';
import {MonitorComponent} from './monitor.component';
import {
  MatCardModule,
  MatGridListModule,
  MatIconModule,
  MatPaginatorModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatButtonModule,
  MatDialogModule,
  MatFormFieldModule,
  MatListModule,
  MatInputModule,
  MatDatepickerModule
} from '@angular/material';
import {NgxsModule} from '@ngxs/store';
import {MonitorState} from '../../../store/states/monitor.state';
import { TranslateModule } from '@ngx-translate/core';
import { CreateMonitorComponent } from './create-monitor.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatMomentDateModule } from '@angular/material-moment-adapter';

@NgModule({
  declarations: [MonitorComponent, CreateMonitorComponent],
  imports: [
    CommonModule,
    MonitorRoutingModule,
    MatTableModule,
    MatIconModule,
    MatCardModule,
    MatPaginatorModule,
    MatTabsModule,
    MatSortModule,
    MatGridListModule,
    TranslateModule,
    MatButtonModule,
    MatListModule,
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatMomentDateModule,
    NgxsModule.forFeature([
      MonitorState
    ])
  ],
  entryComponents: [CreateMonitorComponent]
})
export class MonitorModule {
}
