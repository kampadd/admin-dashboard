import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {IMonitor} from 'src/app/shared/models/monitor.model';
import {AddMonitor, UpdateMonitor} from 'src/app/store/actions/monitor.action';
import {first, tap} from 'rxjs/operators';
import {Store} from '@ngxs/store';
import moment from 'moment';

@Component({
  selector: 'app-create-monitor',
  templateUrl: './create-monitor.component.html'
})

export class CreateMonitorComponent implements OnInit {
  form: FormGroup;
  today = moment();

  constructor(@Inject(MAT_DIALOG_DATA) public monitor: IMonitor,
              private fb: FormBuilder,
              private store: Store,
              private dialogRef: MatDialogRef<CreateMonitorComponent>) {
  }

  ngOnInit() {
    this.createForm();
    this.fillForm();
  }

  private createForm(): void {
    this.form = this.fb.group({
      idClass: [''],
      code: [''],
      description: [''],
      status: [''],
      user: [''],
      beginDate: ['', [Validators.required]],
      notes: [''],
      endDate: ['', [Validators.required]],
      currentId: [''],
      idTenant: [''],
      serialNumber: [''],
      supplier: [''],
      purchaseDate: ['', [Validators.required]],
      acceptanceDate: ['', [Validators.required]],
      finalCost: [''],
      brand: [''],
      model: [''],
      room: [''],
      assignee: [''],
      technicalReference: [''],
      workplace: [''],
      acceptanceNotes: [''],
      type: [''],
      screenSize: ['']
    });
  }

  private fillForm(): void {
    if (this.monitor) {
      this.form.patchValue(this.monitor || {});
    }
  }

  onSubmit(): void {
    if (this.form.valid) {
      if (this.monitor && this.monitor.id) {
        const monitor = JSON.parse(JSON.stringify(this.monitor)) as IMonitor;
        monitor.idClass = this.form.get('idClass').value;
        monitor.code = this.form.get('code').value;
        monitor.description = this.form.get('description').value;
        monitor.status = this.form.get('status').value;
        monitor.user = this.form.get('user').value;
        monitor.beginDate = this.form.get('beginDate').value;
        monitor.notes = this.form.get('notes').value;
        monitor.endDate = this.form.get('endDate').value;
        monitor.currentId = this.form.get('currentId').value;
        monitor.idTenant = this.form.get('idTenant').value;
        monitor.serialNumber = this.form.get('serialNumber').value;
        monitor.supplier = this.form.get('supplier').value;
        monitor.purchaseDate = this.form.get('purchaseDate').value;
        monitor.acceptanceDate = this.form.get('acceptanceDate').value;
        monitor.finalCost = this.form.get('finalCost').value;
        monitor.brand = this.form.get('brand').value;
        monitor.model = this.form.get('model').value;
        monitor.room = this.form.get('room').value;
        monitor.assignee = this.form.get('assignee').value;
        monitor.technicalReference = this.form.get('technicalReference').value;
        monitor.workplace = this.form.get('workplace').value;
        monitor.acceptanceNotes = this.form.get('acceptanceNotes').value;
        monitor.type = this.form.get('type').value;
        monitor.screenSize = this.form.get('screenSize').value;

        this.store.dispatch(new UpdateMonitor(monitor)).pipe(first(),
          tap(() => this.dialogRef.close())).subscribe();
      } else {
        const monitor = this.form.getRawValue() as IMonitor;
        this.store.dispatch(new AddMonitor(monitor)).pipe(first(),
          tap(() => this.dialogRef.close())).subscribe();
      }
    }
  }


}
