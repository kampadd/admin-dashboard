import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {IMonitor} from 'src/app/shared/models/monitor.model';
import {Observable} from 'rxjs';
import moment from 'moment';
import {map} from 'rxjs/operators';
import {createRequestOption} from 'src/app/shared/utils/request-util';
import {ITableData} from '../../../shared/interfaces/i-table-data';
import {CMDB_SERVER_API} from '../../../app.constants';

@Injectable({providedIn: 'root'})
export class MonitorService {
  public resourceUrl = CMDB_SERVER_API + '/api/monitors';

  constructor(private http: HttpClient) {
  }

  create(monitor: IMonitor): Observable<IMonitor> {
    const copy = this.convertDateFromClient(monitor);
    return this.http
      .post<IMonitor>(this.resourceUrl, copy)
      .pipe(map((result: IMonitor) => this.convertDateFromServer(result)));
  }

  update(monitor: IMonitor): Observable<IMonitor> {
    const copy = this.convertDateFromClient(monitor);
    return this.http
      .put<IMonitor>(this.resourceUrl, copy)
      .pipe(map((result: IMonitor) => this.convertDateFromServer(result)));
  }

  query(req?: any): Observable<ITableData> {
    const options = createRequestOption(req);
    return this.http.get<IMonitor[]>(this.resourceUrl, {params: options, observe: 'response'}).pipe(
      map((res: HttpResponse<IMonitor[]>) => {
        return {
          totalCount: parseInt(res.headers.get('X-Total-Count'), 10),
          entries: this.convertDateArrayFromServer(res.body)
        };
      })
    );
  }

  delete(id: string): Observable<any> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`);
  }

  private convertDateFromClient(monitor: IMonitor): IMonitor {
    return Object.assign({}, monitor, {
      beginDate: monitor.beginDate != null && monitor.beginDate.isValid() ? monitor.beginDate.toJSON() : null,
      endDate: monitor.endDate != null && monitor.endDate.isValid() ? monitor.endDate.toJSON() : null,
      purchaseDate: monitor.purchaseDate != null && monitor.purchaseDate.isValid() ? monitor.purchaseDate.toJSON() : null,
      acceptanceDate: monitor.acceptanceDate != null && monitor.acceptanceDate.isValid() ? monitor.purchaseDate.toJSON() : null
    });
  }

  private convertDateFromServer(result: IMonitor): IMonitor {
    if (result) {
      result.beginDate = result.beginDate != null ? moment(result.beginDate) : null;
      result.endDate = result.endDate != null ? moment(result.endDate) : null;
      result.purchaseDate = result.purchaseDate != null ? moment(result.purchaseDate) : null;
      result.acceptanceDate = result.acceptanceDate != null ? moment(result.acceptanceDate) : null;
    }
    return result;
  }

  private convertDateArrayFromServer(result: IMonitor[]): IMonitor[] {
    if (result) {
      result.forEach((monitor: IMonitor) => {
        monitor.beginDate = monitor.beginDate != null ? moment(monitor.beginDate) : null;
        monitor.endDate = monitor.endDate != null ? moment(monitor.endDate) : null;
        monitor.purchaseDate = monitor.purchaseDate != null ? moment(monitor.purchaseDate) : null;
        monitor.acceptanceDate = monitor.acceptanceDate != null ? moment(monitor.acceptanceDate) : null;
      });
    }
    return result;
  }
}
