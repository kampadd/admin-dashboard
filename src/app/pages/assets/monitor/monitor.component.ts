import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {EMPTY, merge, Observable, Subscription} from 'rxjs';
import {TableDataSource} from 'src/app/shared/utils/table-datasource';
import {IMonitor} from 'src/app/shared/models/monitor.model';
import {MatDialog, MatPaginator, MatSort} from '@angular/material';
import {Select, Store} from '@ngxs/store';
import {MonitorState} from 'src/app/store/states/monitor.state';
import {ITableData} from 'src/app/shared/interfaces/i-table-data';
import {Dispatch} from '@ngxs-labs/dispatch-decorator';
import {DeleteMonitor, FetchMonitors} from 'src/app/store/actions/monitor.action';
import {ConfirmationDialogService} from 'src/app/shared/services/confirmation-dialog.service';
import {first, switchMap, tap} from 'rxjs/operators';
import { CreateMonitorComponent } from './create-monitor.component';


@Component({
  selector: 'app-monitor',
  templateUrl: './monitor.component.html',
  styleUrls: ['./monitor.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)'))
    ])
  ]
})
export class MonitorComponent implements OnInit, AfterViewInit, OnDestroy {
  private subscription: Subscription;
  private sortSubscription: Subscription;
  public dataSource = new TableDataSource();

  columnsToDisplay: string[] = ['code', 'description', 'serialNumber', 'supplier', 'brand', 'model', 'assignee', 'technicalReference', 'actions'];
  expandedElement: IMonitor;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  @Select(MonitorState.getTableData) monitorTableData: Observable<ITableData>;
  @Dispatch() private fetchTableData = () => new FetchMonitors([this.sort.active + ',' + this.sort.direction],
    this.paginator.pageIndex, this.paginator.pageSize);

  constructor(private dialog: MatDialog, private store: Store, private confirmationDialogService: ConfirmationDialogService) {
  }

  ngOnInit() {
    this.fetchTableData();
    this.monitorTableData.subscribe(data => {
      this.dataSource.update(data);
    });
  }

  ngAfterViewInit() {
    // reset the paginator after sorting
    this.sortSubscription = this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    this.subscription = merge(this.sort.sortChange, this.paginator.page).pipe(
      tap(() => this.fetchTableData())).subscribe();
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.sortSubscription) {
      this.sortSubscription.unsubscribe();
    }
  }

  editMonitor(event: Event, monitor: IMonitor): void {
    event.stopPropagation();
    setTimeout(() => {
      this.dialog.open(CreateMonitorComponent, {
        minWidth: '10vw',
        maxWidth: '95vw',
        maxHeight: '95vh',
        disableClose: true,
        data: monitor
      });
    });
  }

  deleteMonitor(event: Event, id: string): void {
    event.stopPropagation();
    this.confirmationDialogService.open(`Do you want to proceed with action`);
    this.confirmationDialogService.dialogRef.afterClosed().pipe(
      first(),
      switchMap(value => {
        if (value) {
          return this.store.dispatch(new DeleteMonitor(id));
        }
        return EMPTY;
      }), switchMap(() => this.store.dispatch(new FetchMonitors()))).subscribe();
  }

  doNothing(event: Event, computer: IMonitor): void {
    event.stopPropagation();
    console.log("Yet to be Implemented");
  }

}

