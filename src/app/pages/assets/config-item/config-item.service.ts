import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {CMDB_SERVER_API} from 'src/app/app.constants';
import {IConfigItem} from 'src/app/shared/models/config-item.model';
import {Observable} from 'rxjs';
import moment from 'moment';
import {map} from 'rxjs/operators';
import {createRequestOption} from 'src/app/shared/utils/request-util';
import {ITableData} from '../../../shared/interfaces/i-table-data';

@Injectable({providedIn: 'root'})
export class ConfigItemService {
  public resourceUrl = CMDB_SERVER_API + '/api/config-items';

  constructor(private http: HttpClient) {
  }

  create(configItem: IConfigItem): Observable<IConfigItem> {
    const copy = this.convertDateFromClient(configItem);
    return this.http
      .post<IConfigItem>(this.resourceUrl, copy)
      .pipe(map((result: IConfigItem) => this.convertDateFromServer(result)));
  }

  update(configItem: IConfigItem): Observable<IConfigItem> {
    const copy = this.convertDateFromClient(configItem);
    return this.http
      .put<IConfigItem>(this.resourceUrl, copy)
      .pipe(map((result: IConfigItem) => this.convertDateFromServer(result)));
  }

  query(req?: any): Observable<ITableData> {
    const options = createRequestOption(req);
    return this.http.get<IConfigItem[]>(this.resourceUrl, {params: options, observe: 'response'}).pipe(
      map((res: HttpResponse<IConfigItem[]>) => {
        return {
          totalCount: parseInt(res.headers.get('X-Total-Count'), 10),
          entries: this.convertDateArrayFromServer(res.body)
        };
      })
    );
  }

  delete(id: string): Observable<any> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`);
  }

  private convertDateFromClient(configItem: IConfigItem): IConfigItem {
    return Object.assign({}, configItem, {
      beginDate: configItem.beginDate != null && configItem.beginDate.isValid() ? configItem.beginDate.toJSON() : null,
      endDate: configItem.endDate != null && configItem.endDate.isValid() ? configItem.endDate.toJSON() : null
    });
  }

  private convertDateFromServer(result: IConfigItem): IConfigItem {
    if (result) {
      result.beginDate = result.beginDate != null ? moment(result.beginDate) : null;
      result.endDate = result.endDate != null ? moment(result.endDate) : null;
    }
    return result;
  }

  private convertDateArrayFromServer(result: IConfigItem[]): IConfigItem[] {
    if (result) {
      result.forEach((configItem: IConfigItem) => {
        configItem.beginDate = configItem.beginDate != null ? moment(configItem.beginDate) : null;
        configItem.endDate = configItem.endDate != null ? moment(configItem.endDate) : null;
      });
    }
    return result;
  }
}
