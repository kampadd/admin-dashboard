import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConfigItemRoutingModule } from './config-item-routing.module';
import { ConfigItemComponent } from './config-item.component';
import { CreateConfigItemComponent } from './create-config-item.component';
import { MatButtonModule, MatTableModule, MatIconModule, MatCardModule, MatTabsModule, MatSortModule, MatPaginatorModule, MatGridListModule, MatListModule, MatFormFieldModule, MatInputModule, MatDatepickerModule, MatDialogModule } from '@angular/material';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { NgxsModule } from '@ngxs/store';
import { ConfigItemState } from 'src/app/store/states/config-item.state';

@NgModule({
  declarations: [ConfigItemComponent, CreateConfigItemComponent],
  imports: [
    CommonModule,
    ConfigItemRoutingModule,
    MatButtonModule,
    TranslateModule,
    MatTableModule,
    MatIconModule,
    MatCardModule,
    MatPaginatorModule,
    MatTabsModule,
    MatSortModule,
    MatGridListModule,
    MatListModule,
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatMomentDateModule,
    NgxsModule.forFeature([
      ConfigItemState
    ])
  ],
  entryComponents: [CreateConfigItemComponent]
})
export class ConfigItemModule { }
