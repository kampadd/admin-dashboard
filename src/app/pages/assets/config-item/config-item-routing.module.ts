import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConfigItemComponent } from './config-item.component';

const routes: Routes = [
  {
    path: '',
    component: ConfigItemComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfigItemRoutingModule { }
