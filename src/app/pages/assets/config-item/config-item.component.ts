import {Component, OnInit, ViewChild} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {EMPTY, merge, Observable, Subscription} from 'rxjs';
import {TableDataSource} from 'src/app/shared/utils/table-datasource';
import {IConfigItem} from 'src/app/shared/models/config-item.model';
import {MatDialog, MatPaginator, MatSort} from '@angular/material';
import {Select, Store} from '@ngxs/store';
import {Dispatch} from '@ngxs-labs/dispatch-decorator';
import {ConfigItemState} from 'src/app/store/states/config-item.state';
import {DeleteConfigItem, FetchConfigItems} from 'src/app/store/actions/config-item.action';
import {ConfirmationDialogService} from 'src/app/shared/services/confirmation-dialog.service';
import {ITableData} from 'src/app/shared/interfaces/i-table-data';
import {first, switchMap, tap} from 'rxjs/operators';
import {CreateConfigItemComponent} from './create-config-item.component';

@Component({
  selector: 'app-config-item',
  templateUrl: './config-item.component.html',
  styleUrls: ['./config-item.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)'))
    ])
  ]
})
export class ConfigItemComponent implements OnInit {
  private subscription: Subscription;
  private sortSubscription: Subscription;
  public dataSource = new TableDataSource();
  columnsToDisplay: string[] = ['code', 'description', 'serialNumber', 'supplier', 'brand', 'model', 'assignee', 'technicalReference', 'actions'];
  expandedElement: IConfigItem;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  @Select(ConfigItemState.getTableData) configItemTableData: Observable<ITableData>;
  @Dispatch() private fetchTableData = () => new FetchConfigItems([this.sort.active + ',' + this.sort.direction],
    this.paginator.pageIndex, this.paginator.pageSize);

  constructor(private dialog: MatDialog, private store: Store, private confirmationDialogService: ConfirmationDialogService) {

  }

  ngOnInit() {
    this.fetchTableData();
    this.configItemTableData.subscribe(data => {
      this.dataSource.update(data);
    });
  }

  ngAfterViewInit() {
    // reset the paginator after sorting
    this.sortSubscription = this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    this.subscription = merge(this.sort.sortChange, this.paginator.page).pipe(
      tap(() => this.fetchTableData())).subscribe();
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.sortSubscription) {
      this.sortSubscription.unsubscribe();
    }
  }

  editConfigItem(event: Event, configItem: IConfigItem): void {
    event.stopPropagation();
    setTimeout(() => {
      this.dialog.open(CreateConfigItemComponent, {
        minWidth: '10vw',
        maxWidth: '95vw',
        maxHeight: '95vh',
        disableClose: true,
        data: configItem
      });
    });
  }

  deleteConfigItem(event: Event, id: string): void {
    event.stopPropagation();
    this.confirmationDialogService.open(`Do you want to proceed with action`);
    this.confirmationDialogService.dialogRef.afterClosed().pipe(
      first(),
      switchMap(value => {
        if (value) {
          return this.store.dispatch(new DeleteConfigItem(id));
        }
        return EMPTY;
      }), switchMap(() => this.store.dispatch(new FetchConfigItems()))).subscribe();
  }

  doNothing(event: Event, configItem: IConfigItem): void {
    event.stopPropagation();
    console.log('Yet to be Implemented');
  }

}
