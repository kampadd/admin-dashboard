import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { IConfigItem } from 'src/app/shared/models/config-item.model';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Store } from '@ngxs/store';
import { UpdateConfigItem, AddConfigItem } from 'src/app/store/actions/config-item.action';
import { first, tap } from 'rxjs/operators';

@Component({
    selector: "app-create-config-item",
    templateUrl: "create-config-item.component.html"
})
export class CreateConfigItemComponent implements OnInit {
    form: FormGroup;

    constructor(@Inject(MAT_DIALOG_DATA) public configItem: IConfigItem,
              private fb:FormBuilder,
              private store: Store,
              private dialogRef: MatDialogRef<CreateConfigItemComponent>) {}

    ngOnInit() {
        this.createForm();
        this.fillForm();
    }

    private createForm(): void {
        this.form = this.fb.group({
            idClass: [''],
            code: [''],
            description: [''],
            status: [''],
            user: [''],
            beginDate: ['', [Validators.required]],
            notes: [''],
            endDate: ['', [Validators.required]],
            currentId: [''],
            idTenant: ['']
        })
    }

    private fillForm(): void {
        if (this.configItem) {
            this.form.patchValue(this.configItem || {});
        }
    }

    onSubmit(): void {
        if(this.form.valid) {
            if(this.configItem && this.configItem.id) {
                const configItem = JSON.parse(JSON.stringify(this.configItem)) as IConfigItem;
                configItem.idClass = this.form.get('idClass').value;
                configItem.code = this.form.get('code').value;
                configItem.description = this.form.get('description').value;
                configItem.status = this.form.get('status').value;
                configItem.user = this.form.get('user').value;
                configItem.beginDate = this.form.get('beginDate').value;
                configItem.notes = this.form.get('notes').value;
                configItem.endDate = this.form.get('endDate').value;
                configItem.currentId = this.form.get('currentId').value;
                configItem.idTenant = this.form.get('idTenant').value;

                this.store.dispatch(new UpdateConfigItem(configItem)).pipe(first(),
                    tap(() => this.dialogRef.close())).subscribe();
            } else {
                const configItem = this.form.getRawValue() as IConfigItem;
                this.store.dispatch(new AddConfigItem(configItem)).pipe(first(),
                    tap(() => this.dialogRef.close())).subscribe();
            }
        }
    }
}