import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {LicenseRoutingModule} from './license-routing.module';
import {LicenseComponent} from './license.component';
import {
  MatCardModule,
  MatGridListModule,
  MatIconModule,
  MatPaginatorModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatListModule,
  MatButtonModule,
  MatFormFieldModule,
  MatDialogModule,
  MatInputModule,
  MatDatepickerModule,
} from '@angular/material';
import {NgxsModule} from '@ngxs/store';
import {LicenseState} from '../../../store/states/license.state';
import { TranslateModule } from '@ngx-translate/core';
import { CreateLicenseComponent } from './create-license.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatMomentDateModule } from '@angular/material-moment-adapter';


@NgModule({
  declarations: [LicenseComponent, CreateLicenseComponent],
  imports: [
    CommonModule,
    LicenseRoutingModule,
    TranslateModule,
    MatTableModule,
    MatIconModule,
    MatCardModule,
    MatPaginatorModule,
    MatTabsModule,
    MatSortModule,
    MatGridListModule,
    MatButtonModule,
    MatListModule,
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatMomentDateModule,
    NgxsModule.forFeature([
      LicenseState
    ])
  ],
  entryComponents: [CreateLicenseComponent]
})
export class LicenseModule {
}
