import {Injectable} from '@angular/core';
import {CMDB_SERVER_API} from 'src/app/app.constants';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {ILicense} from 'src/app/shared/models/license.model';
import moment from 'moment';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {ITableData} from 'src/app/shared/interfaces/i-table-data';
import {createRequestOption} from 'src/app/shared/utils/request-util';

@Injectable({providedIn: 'root'})
export class LicenseService {
  public resourceUrl = CMDB_SERVER_API + '/api/licenses';

  constructor(private http: HttpClient) {
  }

  create(computer: ILicense): Observable<ILicense> {
    const copy = this.convertDateFromClient(computer);
    return this.http
      .post<ILicense>(this.resourceUrl, copy)
      .pipe(map((result: ILicense) => this.convertDateFromServer(result)));
  }

  update(computer: ILicense): Observable<ILicense> {
    const copy = this.convertDateFromClient(computer);
    return this.http
      .put<ILicense>(this.resourceUrl, copy)
      .pipe(map((result: ILicense) => this.convertDateFromServer(result)));
  }

  query(req?: any): Observable<ITableData> {
    const options = createRequestOption(req);
    return this.http.get<ILicense[]>(this.resourceUrl, {params: options, observe: 'response'}).pipe(
      map((res: HttpResponse<ILicense[]>) => {
        return {
          totalCount: parseInt(res.headers.get('X-Total-Count'), 10),
          entries: this.convertDateArrayFromServer(res.body)
        };
      })
    );
  }

  delete(id: string): Observable<any> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`);
  }

  private convertDateFromClient(license: ILicense): ILicense {
    return Object.assign({}, license, {
      beginDate: license.beginDate != null && license.beginDate.isValid() ? license.beginDate.toJSON() : null,
      endDate: license.endDate != null && license.endDate.isValid() ? license.endDate.toJSON() : null,
      purchaseDate: license.purchaseDate != null && license.purchaseDate.isValid() ? license.purchaseDate.toJSON() : null,
      acceptanceDate: license.acceptanceDate != null && license.acceptanceDate.isValid() ? license.purchaseDate.toJSON() : null
    });
  }

  private convertDateFromServer(result: ILicense): ILicense {
    if (result) {
      result.beginDate = result.beginDate != null ? moment(result.beginDate) : null;
      result.endDate = result.endDate != null ? moment(result.endDate) : null;
      result.purchaseDate = result.purchaseDate != null ? moment(result.purchaseDate) : null;
      result.acceptanceDate = result.acceptanceDate != null ? moment(result.acceptanceDate) : null;
    }
    return result;
  }

  private convertDateArrayFromServer(result: ILicense[]): ILicense[] {
    if (result) {
      result.forEach((license: ILicense) => {
        license.beginDate = license.beginDate != null ? moment(license.beginDate) : null;
        license.endDate = license.endDate != null ? moment(license.endDate) : null;
        license.purchaseDate = license.purchaseDate != null ? moment(license.purchaseDate) : null;
        license.acceptanceDate = license.acceptanceDate != null ? moment(license.acceptanceDate) : null;
      });
    }
    return result;
  }
}
