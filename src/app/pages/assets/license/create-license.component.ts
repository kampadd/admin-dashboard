import { OnInit, Component, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { ILicense } from 'src/app/shared/models/license.model';
import { Store } from '@ngxs/store';
import { UpdateLicense, AddLicense } from 'src/app/store/actions/license.action';
import { tap, first } from 'rxjs/operators';

@Component({
   selector: 'app-create-license',
   templateUrl: './create-license.component.html'
})

export class CreateLicenseComponent implements OnInit {
    form: FormGroup;

    constructor(@Inject(MAT_DIALOG_DATA) public license: ILicense,
              private fb:FormBuilder,
              private store: Store,
              private dialogRef: MatDialogRef<CreateLicenseComponent>) { }

    ngOnInit() {
        this.createForm();
        this.fillForm();
    }

    private createForm(): void {
        this.form = this.fb.group({
          idClass: [''],
          code: [''],
          description: [''],
          status: [''],
          user: [''],
          beginDate: ['', [Validators.required]],
          notes: [''],
          endDate: ['', [Validators.required]],
          currentId: [''],
          idTenant: [''],
          serialNumber: [''],
          supplier: [''],
          purchaseDate: ['', [Validators.required]],
          acceptanceDate: ['', [Validators.required]],
          finalCost: [''],
          brand: [''],
          model: [''],
          room: [''],
          assignee: [''],
          technicalReference: [''],
          workplace: [''],
          acceptanceNotes: [''],
          category: [''],
          version: ['']
        });
      }
    
      private fillForm(): void {
        if(this.license) {
          this.form.patchValue(this.license || {});
        }
      }
    
      onSubmit(): void {
        if(this.form.valid) {
          if(this.license && this.license.id) {
            const license = JSON.parse(JSON.stringify(this.license)) as ILicense;
            license.idClass = this.form.get('idClass').value;
            license.code = this.form.get('code').value;
            license.description = this.form.get('description').value;
            license.status = this.form.get('status').value;
            license.user = this.form.get('user').value;
            license.beginDate = this.form.get('beginDate').value;
            license.notes = this.form.get('notes').value;
            license.endDate = this.form.get('endDate').value;
            license.currentId = this.form.get('currentId').value;
            license.idTenant = this.form.get('idTenant').value;
            license.serialNumber = this.form.get('serialNumber').value;
            license.supplier = this.form.get('supplier').value;
            license.purchaseDate = this.form.get('purchaseDate').value;
            license.acceptanceDate = this.form.get('acceptanceDate').value;
            license.finalCost = this.form.get('finalCost').value;
            license.brand = this.form.get('brand').value;
            license.model = this.form.get('model').value;
            license.room = this.form.get('room').value;
            license.assignee = this.form.get('assignee').value;
            license.technicalReference = this.form.get('technicalReference').value;
            license.workplace = this.form.get('workplace').value;
            license.acceptanceNotes = this.form.get('acceptanceNotes').value;
            license.category = this.form.get('category').value;
            license.version = this.form.get('version').value;
    
            this.store.dispatch(new UpdateLicense(license)).pipe(first(),
              tap(() => this.dialogRef.close())).subscribe();
          } else {
            const license = this.form.getRawValue() as ILicense;
            this.store.dispatch(new AddLicense(license)).pipe(first(),
              tap(() => this.dialogRef.close())).subscribe();
          } 
        }
      }
}