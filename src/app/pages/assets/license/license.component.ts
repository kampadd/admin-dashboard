import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {EMPTY, merge, Observable, Subscription} from 'rxjs';
import {TableDataSource} from 'src/app/shared/utils/table-datasource';
import {ILicense} from 'src/app/shared/models/license.model';
import {MatDialog, MatPaginator, MatSort} from '@angular/material';
import {Select, Store} from '@ngxs/store';
import {LicenseState} from 'src/app/store/states/license.state';
import {ITableData} from 'src/app/shared/interfaces/i-table-data';
import {Dispatch} from '@ngxs-labs/dispatch-decorator';
import {DeleteLicense, FetchLicenses} from 'src/app/store/actions/license.action';
import {ConfirmationDialogService} from 'src/app/shared/services/confirmation-dialog.service';
import {first, switchMap, tap} from 'rxjs/operators';
import { CreateLicenseComponent } from './create-license.component';

@Component({
  selector: 'app-license',
  templateUrl: './license.component.html',
  styleUrls: ['./license.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ]
})
export class LicenseComponent implements OnInit, AfterViewInit, OnDestroy {
  private subscription: Subscription;
  private sortSubscription: Subscription;
  public dataSource = new TableDataSource();

  columnsToDisplay: string[] = ['code', 'description', 'serialNumber', 'supplier', 'brand', 'model', 'assignee', 'technicalReference', 'actions'];
  expandedElement: ILicense;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  @Select(LicenseState.getTableData) licenseTableData: Observable<ITableData>;
  @Dispatch() private fetchTableData = () => new FetchLicenses([this.sort.active + ',' + this.sort.direction],
    this.paginator.pageIndex, this.paginator.pageSize);

  constructor(private dialog: MatDialog, private store: Store, private confirmationDialogService: ConfirmationDialogService) {
  }

  ngOnInit() {
    this.fetchTableData();
    this.licenseTableData.subscribe(data => {
      this.dataSource.update(data);
    });
  }

  ngAfterViewInit() {
    // reset the paginator after sorting
    this.sortSubscription = this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    this.subscription = merge(this.sort.sortChange, this.paginator.page).pipe(
      tap(() => this.fetchTableData())).subscribe();
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.sortSubscription) {
      this.sortSubscription.unsubscribe();
    }
  }

  editLicense(event: Event, license: ILicense): void {
    event.stopPropagation();
    setTimeout(() => {
      this.dialog.open(CreateLicenseComponent, {
        minWidth: '10vw',
        maxWidth: '95vw',
        maxHeight: '95vh',
        disableClose: true,
        data: license
      });
    });
  }

  deleteLicense(event: Event, id: string): void {
    event.stopPropagation();
    this.confirmationDialogService.open(`Do you want to proceed with action`);
    this.confirmationDialogService.dialogRef.afterClosed().pipe(
      first(),
      switchMap(value => {
        if (value) {
          return this.store.dispatch(new DeleteLicense(id));
        }
        return EMPTY;
      }), switchMap(() => this.store.dispatch(new FetchLicenses()))).subscribe();
  }
}
