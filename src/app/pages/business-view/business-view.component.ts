import {Component, OnInit} from '@angular/core';
import {BUSINESS_VIEW_IFRAME_URL} from '../../app.constants';

@Component({
  selector: 'app-business-view',
  templateUrl: './business-view.component.html',
  styleUrls: ['./business-view.component.scss']
})
export class BusinessViewComponent implements OnInit {

  iframeUrl = BUSINESS_VIEW_IFRAME_URL;

  constructor() {
  }

  ngOnInit() {
  }

}
