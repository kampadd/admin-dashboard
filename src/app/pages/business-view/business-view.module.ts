import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {BusinessViewRoutingModule} from './business-view-routing.module';
import {BusinessViewComponent} from './business-view.component';
import {NgMagicIframeModule} from '@sebgroup/ng-magic-iframe';
import {MatProgressSpinnerModule} from '@angular/material';


@NgModule({
  declarations: [BusinessViewComponent],
  imports: [
    CommonModule,
    BusinessViewRoutingModule,
    NgMagicIframeModule,
    MatProgressSpinnerModule
  ]
})
export class BusinessViewModule {
}
