import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {BusinessViewComponent} from './business-view.component';


const routes: Routes = [{
  path: '',
  component: BusinessViewComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BusinessViewRoutingModule {
}
