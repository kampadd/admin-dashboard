import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './home.component';
import {AppRoles} from '../../shared/enums/roles.enum';
import {AuthorizationGuard} from '../../core/auth/authorization-guard.service';

export const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: [
      {
        path: 'cockpit',
        loadChildren: '../cockpit/cockpit.module#CockpitModule'
      }, {
        path: 'ip-discovery',
        loadChildren: '../ip-discovery/ip-discovery.module#IpDiscoveryModule'
      }, {
        path: 'snmp-config',
        loadChildren: '../snmp-config/snmp-config.module#SnmpConfigModule'
      }, {
        path: 'basic-monitoring',
        loadChildren: '../basic-monitoring/basic-monitoring.module#BasicMonitoringModule'
      }, {
        path: 'network',
        loadChildren: '../network/network.module#NetworkModule'
      }, {
        path: 'business-view',
        loadChildren: '../business-view/business-view.module#BusinessViewModule'
      }, {
        path: 'audits',
        loadChildren: '../../admin/audits/audits.module#AuditsModule',
        canActivate: [AuthorizationGuard],
        data: {
          roles: [AppRoles.ADMIN]
        }
      }, {
        path: 'metrics',
        loadChildren: '../../admin/metrics/metrics.module#MetricsModule',
        canActivate: [AuthorizationGuard],
        data: {
          roles: [AppRoles.ADMIN]
        }
      }, {
        path: 'health',
        loadChildren: '../../admin/health/health.module#HealthModule',
        canActivate: [AuthorizationGuard],
        data: {
          roles: [AppRoles.ADMIN]
        }
      }, {
        path: 'users',
        loadChildren: '../user/user.module#UserModule',
        canActivate: [AuthorizationGuard],
        data: {
          roles: [AppRoles.ADMIN]
        }
      }, {
        path: 'account',
        loadChildren: '../settings/account/account.module#AccountModule'
      }, {
        path: 'change-password',
        loadChildren: '../settings/change-password/change-password.module#ChangePasswordModule'
      }, {
        path: 'asset-classes',
        loadChildren: '../asset-classes/asset-classes.module#AssetClassesModule'
      }, {
        path: '',
        redirectTo: 'business-view',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule {
}
