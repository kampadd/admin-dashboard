import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HomeRoutingModule} from './home-routing.module';
import {HomeComponent} from './home.component';
import {MatButtonModule, MatIconModule, MatMenuModule, MatSidenavModule, MatSnackBarModule, MatToolbarModule} from '@angular/material';
import {TranslateModule} from '@ngx-translate/core';
import {AssetNavigationModule} from '../asset-classes/navigation/asset-navigation.module';
import {HomeNavigationModule} from './navigation/home-navigation.module';


@NgModule({
  declarations: [HomeComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    MatMenuModule,
    MatSidenavModule,
    MatToolbarModule,
    MatIconModule,
    HomeNavigationModule,
    MatButtonModule,
    MatSnackBarModule,
    TranslateModule,
    AssetNavigationModule
  ]
})
export class HomeModule {
}
