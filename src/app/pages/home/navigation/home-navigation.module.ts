import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MatIconModule, MatListModule} from '@angular/material';
import {TranslateModule} from '@ngx-translate/core';
import {HomeNavigationComponent} from './home-navigation.component';
import {RouterModule} from '@angular/router';


@NgModule({
  declarations: [HomeNavigationComponent],
  imports: [
    CommonModule,
    MatIconModule,
    MatListModule,
    TranslateModule,
    RouterModule
  ],
  exports: [HomeNavigationComponent]
})
export class HomeNavigationModule {
}
