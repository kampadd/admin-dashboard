import {Component, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'app-home-navigation',
  templateUrl: './home-navigation.component.html'
})
export class HomeNavigationComponent {
  @Output() isClicked = new EventEmitter<boolean>();
}
