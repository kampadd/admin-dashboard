import {Component, OnInit, ViewChild} from '@angular/core';
import {BreakpointObserver, Breakpoints} from '@angular/cdk/layout';
import {Observable} from 'rxjs';
import {filter, map} from 'rxjs/operators';
import {NavigationEnd, Router} from '@angular/router';
import {MatSidenav} from '@angular/material';
import {TranslateService} from '@ngx-translate/core';
import {Select} from '@ngxs/store';
import {AccountState} from '../../store/states/account.state';
import {Dispatch} from '@ngxs-labs/dispatch-decorator';
import {Logout} from '../../store/actions/account.action';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  private assetPath = '/home/asset-classes';
  version = environment.VERSION;

  @Select(AccountState.isAdmin) isAdmin$: Observable<boolean>;

  @ViewChild('mainSidenav', {static: false}) mainSideNav: MatSidenav;
  @ViewChild('assetSidenav', {static: false}) assetSideNav: MatSidenav;
  langKey = 'en';
  isAsset: boolean;

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  @Dispatch() dispatchLogout = () => new Logout();

  constructor(private breakpointObserver: BreakpointObserver,
              private router: Router,
              private translateService: TranslateService) {
  }

  closeSideNavMobile(): void {
    if (this.mainSideNav && this.mainSideNav.mode === 'over') {
      this.mainSideNav.close();
    }
    if (this.assetSideNav && this.assetSideNav.mode === 'over') {
      this.assetSideNav.close();
    }
  }

  toggleSidenav(): void {
    if (this.mainSideNav) {
      this.mainSideNav.toggle();
    }
    if (this.assetSideNav) {
      this.assetSideNav.toggle();
    }
  }

  ngOnInit(): void {
    this.initIsAsset();
    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd)
    ).subscribe((event: NavigationEnd) => {
      this.isAsset = event.url.includes(this.assetPath);
    });
  }

  public logout(): void {
    this.router.navigate(['/auth/login']);
    this.dispatchLogout();
  }

  private initIsAsset(): void {
    this.isAsset = this.router.url.includes(this.assetPath);
  }

  changeLanguage(langKey: string): void {
    this.translateService.use(langKey);
    this.langKey = this.translateService.currentLang || this.translateService.defaultLang;
  }
}
