import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';
import {Minion} from '../../../../shared/models/minion.model';

@Component({
  selector: 'app-view-minion',
  templateUrl: './view-minion.component.html'
})
export class ViewMinionComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public minion: Minion) {
  }

  ngOnInit() {
  }

}
