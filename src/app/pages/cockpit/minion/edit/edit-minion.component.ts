import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Store} from '@ngxs/store';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {first, tap} from 'rxjs/operators';
import {Subscription} from 'rxjs';
import {Minion} from '../../../../shared/models/minion.model';
import {UpdateMinion} from '../../../../store/actions/minion.action';
import {Dispatch} from '@ngxs-labs/dispatch-decorator';
import {FetchCockpit} from '../../../../store/actions/cockpit.action';

@Component({
  selector: 'app-edit-minion',
  templateUrl: './edit-minion.component.html'
})
export class EditMinionComponent implements OnInit, OnDestroy {

  subscription: Subscription;
  minionForm: FormGroup;
  @Dispatch() private fetchCockpits = () => new FetchCockpit();

  constructor(@Inject(MAT_DIALOG_DATA) public minion: Minion,
              private store: Store,
              private fb: FormBuilder,
              private dialogRef: MatDialogRef<EditMinionComponent>) {
  }

  ngOnInit() {
    this.createForm();
    this.fillForm();
  }

  private createForm(): void {
    this.minionForm = this.fb.group({
      name: ['', [Validators.required]],
      command: [''],
      url: ['']
    });
  }

  onSubmit(): void {
    if (this.minionForm.valid) {
      const minion = JSON.parse(JSON.stringify(this.minion || {})) as Minion;
      minion.name = this.minionForm.get('name').value;
      minion.command = this.minionForm.get('command').value;
      minion.url = this.minionForm.get('url').value;
      this.subscription = this.store.dispatch(new UpdateMinion(minion)).pipe(first(),
        tap(() => {
          this.fetchCockpits();
          this.dialogRef.close();
        })).subscribe();
    }
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  private fillForm(): void {
    if (this.minion) {
      this.minionForm.patchValue({
        name: this.minion.name,
        command: this.minion.command,
        url: this.minion.url,
      });
    }
  }
}
