import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {CockpitRoutingModule} from './cockpit-routing.module';
import {CockpitComponent} from './cockpit.component';
import {MatButtonModule, MatCardModule, MatDialogModule, MatIconModule, MatInputModule, MatMenuModule} from '@angular/material';
import {TranslateModule} from '@ngx-translate/core';
import {NgxsModule} from '@ngxs/store';
import {CockpitState} from '../../store/states/cockpit.state';
import {EditCockpitComponent} from './edit/edit-cockpit.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ViewCockpitComponent} from './view/view-cockpit.component';
import {EditMinionComponent} from './minion/edit/edit-minion.component';
import {ViewMinionComponent} from './minion/view/view-minion.component';
import {MinionState} from '../../store/states/minion.state';
import {CockpitSocketService} from './cockpit-socket.service';
import {MinionSocketService} from './monion-socket.service';
import {SocketIoModule} from 'ngx-socket-io';

@NgModule({
  declarations: [CockpitComponent, EditCockpitComponent, ViewCockpitComponent, EditMinionComponent, ViewMinionComponent],
  imports: [
    CommonModule,
    CockpitRoutingModule,
    MatCardModule,
    TranslateModule,
    MatButtonModule,
    MatIconModule,
    MatMenuModule,
    MatDialogModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    SocketIoModule,
    NgxsModule.forFeature([CockpitState, MinionState])
  ],
  providers: [CockpitSocketService, MinionSocketService],
  entryComponents: [EditCockpitComponent, ViewCockpitComponent, EditMinionComponent, ViewMinionComponent]
})
export class CockpitModule {
}
