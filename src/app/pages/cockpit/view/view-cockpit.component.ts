import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';
import {Cockpit} from '../../../shared/models/cockpit.model';

@Component({
  selector: 'app-view-cockpit',
  templateUrl: './view-cockpit.component.html'
})
export class ViewCockpitComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public cockpit: Cockpit) {
  }

  ngOnInit() {
  }

}
