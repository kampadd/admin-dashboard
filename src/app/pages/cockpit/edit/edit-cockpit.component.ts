import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Store} from '@ngxs/store';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {first, tap} from 'rxjs/operators';
import {Cockpit} from '../../../shared/models/cockpit.model';
import {UpdateCockpit} from '../../../store/actions/cockpit.action';

@Component({
  selector: 'app-edit-cockpit',
  templateUrl: './edit-cockpit.component.html',
  styleUrls: ['./edit-cockpit.component.scss']
})
export class EditCockpitComponent implements OnInit {

  cockpitForm: FormGroup;

  constructor(@Inject(MAT_DIALOG_DATA) public cockpit: Cockpit,
              private store: Store,
              private fb: FormBuilder,
              private dialogRef: MatDialogRef<EditCockpitComponent>) {
  }

  ngOnInit() {
    this.createForm();
    this.fillForm();
  }

  private createForm(): void {
    this.cockpitForm = this.fb.group({
      name: ['', [Validators.required]],
      description: ['']
    });
  }

  onSubmit(): void {
    if (this.cockpitForm.valid) {
      const cockpit = JSON.parse(JSON.stringify(this.cockpit || {})) as Cockpit;
      cockpit.name = this.cockpitForm.get('name').value;
      cockpit.description = this.cockpitForm.get('description').value;
      this.store.dispatch(new UpdateCockpit(cockpit)).pipe(first(),
        tap(() => this.dialogRef.close())).subscribe();
    }
  }

  private fillForm(): void {
    if (this.cockpit) {
      this.cockpitForm.patchValue({
        name: this.cockpit.name,
        description: this.cockpit.description
      });
    }
  }
}
