import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Cockpit} from '../../shared/models/cockpit.model';
import {CORE_SERVER_API} from '../../app.constants';


@Injectable({providedIn: 'root'})
export class CockpitService {
  public resourceUrl = CORE_SERVER_API + '/api/cockpits';

  constructor(private http: HttpClient) {
  }

  fetch(): Observable<Cockpit[]> {
    return this.http.get<Cockpit[]>(this.resourceUrl);
  }

  update(cockpit: Cockpit): Observable<Cockpit> {
    return this.http.put<Cockpit>(this.resourceUrl, cockpit);
  }
}
