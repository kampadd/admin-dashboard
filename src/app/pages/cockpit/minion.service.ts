import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {CORE_SERVER_API} from '../../app.constants';
import {Minion} from '../../shared/models/minion.model';
import {createRequestOption} from '../../shared/utils/request-util';


@Injectable({providedIn: 'root'})
export class MinionService {
  public resourceUrl = CORE_SERVER_API + '/api/minions';

  constructor(protected http: HttpClient) {
  }

  create(minion: Minion): Observable<Minion> {
    return this.http.post<Minion>(this.resourceUrl, minion);
  }

  update(minion: Minion): Observable<Minion> {
    return this.http.put<Minion>(this.resourceUrl, minion);
  }

  find(id: string): Observable<Minion> {
    return this.http.get<Minion>(`${this.resourceUrl}/${id}`);
  }

  query(req?: any): Observable<Minion[]> {
    const options = createRequestOption(req);
    return this.http.get<Minion[]>(this.resourceUrl, {params: options});
  }

  getAll(): Observable<Minion[]> {
    return this.http.get<Minion[]>(this.resourceUrl);
  }

  delete(id: string): Observable<Minion> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`);
  }
}
