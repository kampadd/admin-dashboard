import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CockpitComponent} from './cockpit.component';
import {CockpitSocketService} from './cockpit-socket.service';

const routes: Routes = [{
  path: '',
  component: CockpitComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [CockpitSocketService]
})
export class CockpitRoutingModule {
}
