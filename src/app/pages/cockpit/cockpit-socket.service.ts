import {Injectable} from '@angular/core';
import {Socket} from 'ngx-socket-io';
import {CORE_SERVER_API} from '../../app.constants';


@Injectable()
export class CockpitSocketService extends Socket {

  constructor() {
    const resourceUrl = CORE_SERVER_API + '/websocket/cockpit';
    super({url: resourceUrl, options: {autoConnect: false}});
  }
}
