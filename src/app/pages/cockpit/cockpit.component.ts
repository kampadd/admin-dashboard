import {Component, OnDestroy, OnInit} from '@angular/core';
import {Select, Store} from '@ngxs/store';
import {EMPTY, Observable} from 'rxjs';
import {Cockpit, CockpitConnectedEnum} from '../../shared/models/cockpit.model';
import {FetchCockpit} from '../../store/actions/cockpit.action';
import {CockpitState} from '../../store/states/cockpit.state';
import {Minion, MinionConnectedEnum} from '../../shared/models/minion.model';
import {MatDialog} from '@angular/material';
import {EditCockpitComponent} from './edit/edit-cockpit.component';
import {ViewCockpitComponent} from './view/view-cockpit.component';
import {EditMinionComponent} from './minion/edit/edit-minion.component';
import {ViewMinionComponent} from './minion/view/view-minion.component';
import {first, flatMap, map, switchMap, tap, toArray} from 'rxjs/operators';
import {ConfirmationDialogService} from '../../shared/services/confirmation-dialog.service';
import {DeleteMinion} from '../../store/actions/minion.action';
import {CockpitSocketService} from './cockpit-socket.service';
import {MinionSocketService} from './monion-socket.service';

export interface ChangedStatus {
  id: string;
  status: CockpitConnectedEnum | MinionConnectedEnum;
}

@Component({
  selector: 'app-cockpit',
  templateUrl: './cockpit.component.html',
  styleUrls: ['./cockpit.component.scss']
})
export class CockpitComponent implements OnInit, OnDestroy {

  connectedState = CockpitConnectedEnum;
  minionConnectedState = MinionConnectedEnum;

  private cockpitWebsocketListener = '/topic/cockpit';
  private minionWebsocketListener = '/topic/minion';

  @Select(CockpitState.getCockpits) cockpits: Observable<Cockpit[]>;

  constructor(private dialog: MatDialog,
              private store: Store,
              private cockpitSocketService: CockpitSocketService,
              private minionSocketService: MinionSocketService,
              private confirmationDialogService: ConfirmationDialogService) {
  }

  private toggleCockpitsStatus() {
    this.cockpitSocketService
      .on(this.cockpitWebsocketListener, (changedStatus: ChangedStatus) => {
        this.cockpits = this.cockpits.pipe(
          flatMap(cockpits => cockpits),
          map((entry: Cockpit) => {
            if (entry.id === changedStatus.id) {
              entry.state = <CockpitConnectedEnum>changedStatus.status;
            }
            return entry;
          }), toArray());
      });
  }

  private toggleMinionsStatus() {
    this.minionSocketService
      .on(this.minionWebsocketListener, (changedStatus: ChangedStatus) => {
        this.cockpits = this.cockpits.pipe(
          tap((cockpits: Cockpit[]) => {
            return cockpits.map(cockpit => {
              cockpit.minions = (cockpit.minions || []).map(minion => {
                if (minion.id === changedStatus.id) {
                  minion.state = <MinionConnectedEnum>changedStatus.status;
                }
                return minion;
              });
              return cockpit;
            });
          }));
      });
  }

  ngOnInit(): void {
    this.store.dispatch(new FetchCockpit());
    this.toggleCockpitsStatus();
    this.toggleMinionsStatus();
  }

  ngOnDestroy(): void {
    this.cockpitSocketService.removeListener(this.cockpitWebsocketListener);
    this.minionSocketService.removeListener(this.minionWebsocketListener);
  }

  editMinion(minion: Minion): void {
    setTimeout(() => {
      this.dialog.open(EditMinionComponent, {
        minWidth: '50vw',
        maxWidth: '95vw',
        maxHeight: '95vh',
        disableClose: true,
        data: minion
      });
    });
  }

  deleteMinion(minion: Minion): void {
    this.confirmationDialogService.open(`Do you want to proceed with action`);
    this.confirmationDialogService.dialogRef.afterClosed().pipe(
      first(),
      switchMap(value => {
        if (value) {
          return this.store.dispatch(new DeleteMinion(minion.id));
        }
        return EMPTY;
      }), switchMap(() => this.store.dispatch(new FetchCockpit()))).subscribe();
  }

  viewMinion(minion: Minion): void {
    setTimeout(() => {
      this.dialog.open(ViewMinionComponent, {
        minWidth: '50vw',
        maxWidth: '95vw',
        maxHeight: '95vh',
        data: minion
      });
    });
  }

  viewCockpitAsset(cockpit: Cockpit): void {
    console.log(cockpit);
  }

  editCockpit(cockpit: Cockpit): void {
    setTimeout(() => {
      this.dialog.open(EditCockpitComponent, {
        minWidth: '50vw',
        maxWidth: '95vw',
        maxHeight: '95vh',
        disableClose: true,
        data: cockpit
      });
    });
  }

  viewCockpit(cockpit: Cockpit): void {
    setTimeout(() => {
      this.dialog.open(ViewCockpitComponent, {
        minWidth: '50vw',
        maxWidth: '95vw',
        maxHeight: '95vh',
        data: cockpit
      });
    });
  }
}
