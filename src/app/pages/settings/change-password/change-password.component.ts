import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {Component, OnInit} from '@angular/core';
import {first, tap} from 'rxjs/operators';
import {ToastService} from '../../../shared/services/toast.service';
import {AccountService} from '../../../core/auth/account.service';
import {Account} from '../../../shared/models/account.model';
import {mustMatch, validatePassword} from '../../../shared/utils/validators';
import {IPasswords} from '../../../shared/interfaces/i-passwords';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

  account: Account;
  passwordForm: FormGroup;
  passwordRepeatError = 'New Passwords do not match';

  constructor(private accountService: AccountService,
              private activatedRoute: ActivatedRoute,
              private fb: FormBuilder,
              private toastService: ToastService) {
  }

  ngOnInit() {
    this.createForm();

    this.accountService.getAccount().pipe(
      first(),
      tap(account => {
        this.account = account;
      })).subscribe();
  }

  updatePassword(): void {
    if (this.passwordForm.valid) {
      const passwords: IPasswords = this.passwordForm.value;
      this.accountService.changePassword(passwords).pipe(first()).subscribe(() => {
        this.toastService.open('Password successfully updated', false, 'success', 5000);
      });
    }
  }

  private createForm(): void {
    this.passwordForm = this.fb.group({
      currentPassword: ['', [Validators.required, validatePassword]],
      newPassword: ['', [Validators.required, validatePassword]],
      newPasswordRepeat: ['']
    }, {validator: mustMatch});
  }
}
