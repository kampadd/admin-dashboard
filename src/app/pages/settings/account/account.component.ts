import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {Component, OnInit} from '@angular/core';
import {first, tap} from 'rxjs/operators';
import {validateEmail, validatePhone} from 'src/app/shared/utils/validators';
import {ToastService} from '../../../shared/services/toast.service';
import {AccountService} from '../../../core/auth/account.service';
import {Account} from '../../../shared/models/account.model';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {

  account: Account;
  accountForm: FormGroup;

  languages = ['en', 'de'];

  constructor(private accountService: AccountService,
              private activatedRoute: ActivatedRoute,
              private fb: FormBuilder,
              private toastService: ToastService) {
  }

  ngOnInit() {
    this.createForm();

    this.accountService.getAccount().pipe(
      first(),
      tap(account => {
        this.account = account;
        this.setAccount();
      })).subscribe();
  }

  updateAccount(): void {
    if (this.accountForm.valid) {
      const account: Account = this.accountForm.value;
      this.account.phone = account.phone;
      this.account.firstName = account.firstName;
      this.account.lastName = account.lastName;
      this.account.langKey = account.langKey;
      this.accountService.saveAccount(this.account).pipe(first()).subscribe(() => {
        this.toastService.open('User successfully updated', false, 'success', 5000);
      });
    }
  }

  private createForm(): void {
    this.accountForm = this.fb.group({
      firstName: [''],
      lastName: ['', [Validators.required]],
      email: [{value: '', disabled: true}, [Validators.required, validateEmail()]],
      phone: ['', [Validators.required, validatePhone()]],
      langKey: ['en'],
      authorities: [{value: '', disabled: true}]
    });
  }

  private setAccount(): void {
    this.accountForm.patchValue(this.account);
  }
}
