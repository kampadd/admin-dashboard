import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';

import {IUser} from '../../shared/models/user.model';
import {createRequestOption} from 'src/app/shared/utils/request-util';
import {map} from 'rxjs/operators';
import {Injectable} from '@angular/core';
import {UAA_SERVER_API} from '../../app.constants';
import {ITableData} from '../../shared/interfaces/i-table-data';

@Injectable({providedIn: 'root'})
export class UserService {
  private resourceUrl = UAA_SERVER_API + '/api/users';

  public constructor(private http: HttpClient) {
  }

  create(user: IUser): Observable<IUser> {
    return this.http.post<IUser>(this.resourceUrl, user);
  }

  update(user: IUser): Observable<IUser> {
    return this.http.put<IUser>(this.resourceUrl, user);
  }

  query(req?: any): Observable<ITableData> {
    const options = createRequestOption(req);
    return this.http.get<IUser[]>(this.resourceUrl, {params: options, observe: 'response'}).pipe(
      map((res: HttpResponse<IUser[]>) => {
        return {
          totalCount: parseInt(res.headers.get('X-Total-Count'), 10),
          entries: res.body
        };
      })
    );
  }

  delete(login: string): Observable<any> {
    return this.http.delete(`${this.resourceUrl}/${login}`);
  }

  authorities(): Observable<string[]> {
    return this.http.get<string[]>(this.resourceUrl + '/authorities');
  }
}
