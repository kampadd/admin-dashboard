import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Component, Inject, OnInit} from '@angular/core';
import {first, tap} from 'rxjs/operators';
import {validateEmail} from 'src/app/shared/utils/validators';
import {IUser} from '../../../shared/models/user.model';
import {Observable} from 'rxjs';
import {Select, Store} from '@ngxs/store';
import {AuthorityState} from '../../../store/states/authority.state';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {AddUser, UpdateUser} from '../../../store/actions/user.action';
import {Dispatch} from '@ngxs-labs/dispatch-decorator';
import {FetchAuthorities} from '../../../store/actions/authority.action';

@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.scss']
})
export class UpdateUserComponent implements OnInit {
  updateForm: FormGroup;
  languages = ['en', 'de'];

  @Select(AuthorityState.getAuthorities) authorities: Observable<string[]>;
  @Dispatch() private fetchAuthorities = () => new FetchAuthorities();

  constructor(@Inject(MAT_DIALOG_DATA) public user: IUser,
              private store: Store,
              private fb: FormBuilder,
              private dialogRef: MatDialogRef<UpdateUserComponent>) {
  }

  ngOnInit() {
    this.createForm();
    this.setUser();
    this.fetchAuthorities();
    this.setLoginDisabled();
  }

  submit(): void {
    if (this.updateForm.valid) {
      const user: IUser = this.updateForm.value;
      if (this.user && this.user.id) {
        user.email = this.user.email;
        user.login = this.user.login;
        this.store.dispatch(new UpdateUser(user)).pipe(first(),
          tap(() => this.dialogRef.close())).subscribe();
      } else {
        this.store.dispatch(new AddUser(user)).pipe(first(),
          tap(() => this.dialogRef.close())).subscribe();
      }
    }
  }

  private createForm(): void {
    this.updateForm = this.fb.group({
      login: ['', [Validators.required]],
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      email: ['', [Validators.required, validateEmail()]],
      activated: [false],
      langKey: ['en'],
      authorities: ['']
    });
  }

  private setLoginDisabled(): void {
    if (this.user && this.user.id) {
      this.updateForm.get('login').disable({emitEvent: false});
    }
  }

  private setUser(): void {
    if (this.user) {
      this.updateForm.patchValue(this.user);
    }
  }
}
