import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {EMPTY, merge, Observable, Subscription} from 'rxjs';
import {TableDataSource} from '../../shared/utils/table-datasource';
import {MatDialog, MatPaginator, MatSort} from '@angular/material';
import {Select, Store} from '@ngxs/store';
import {UserState} from '../../store/states/user.state';
import {ITableData} from '../../shared/interfaces/i-table-data';
import {Dispatch} from '@ngxs-labs/dispatch-decorator';
import {DeleteUser, FetchListUserData} from '../../store/actions/user.action';
import {ConfirmationDialogService} from '../../shared/services/confirmation-dialog.service';
import {first, switchMap, tap} from 'rxjs/operators';
import {IUser} from '../../shared/models/user.model';
import {UpdateUserComponent} from './update/update-user.component';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit, OnDestroy, AfterViewInit {
  private subscription: Subscription;
  private sortSubscription: Subscription;
  public displayedColumns: string[] = ['login', 'email', 'firstName', 'lastName', 'authorities', 'activated', 'actions'];
  public dataSource = new TableDataSource();

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  @Select(UserState.getTableData) userTableData$: Observable<ITableData>;
  @Dispatch() private fetchTableData =
    () => new FetchListUserData([this.sort.active + ',' + this.sort.direction],
      this.paginator.pageIndex, this.paginator.pageSize);

  constructor(private dialog: MatDialog, private store: Store, private confirmationDialogService: ConfirmationDialogService) {

  }

  ngOnInit() {
    this.fetchTableData();
    this.userTableData$.subscribe(data => {
      this.dataSource.update(data);
    });
  }

  ngAfterViewInit() {
    // reset the paginator after sorting
    this.sortSubscription = this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    this.subscription = merge(this.sort.sortChange, this.paginator.page).pipe(
      tap(() => this.fetchTableData())).subscribe();
  }

  editUser(user: IUser): void {
    setTimeout(() => {
      this.dialog.open(UpdateUserComponent, {
        minWidth: '50vw',
        maxWidth: '95vw',
        maxHeight: '95vh',
        disableClose: true,
        data: user
      });
    });
  }

  createUser(): void {
    setTimeout(() => {
      this.dialog.open(UpdateUserComponent, {
        minWidth: '50vw',
        maxWidth: '95vw',
        maxHeight: '95vh',
        disableClose: true,
        data: null
      });
    });
  }

  deleteUser(user: IUser): void {
    this.confirmationDialogService.open(`Do you want to proceed with action`);
    this.confirmationDialogService.dialogRef.afterClosed().pipe(
      first(),
      switchMap(value => {
        if (value) {
          return this.store.dispatch(new DeleteUser(user.login));
        }
        return EMPTY;
      }), switchMap(() => this.store.dispatch(new FetchListUserData()))).subscribe();
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.sortSubscription) {
      this.sortSubscription.unsubscribe();
    }
  }
}
