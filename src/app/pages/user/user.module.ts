import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UserComponent} from './user.component';
import {UpdateUserComponent} from './update/update-user.component';
import {TranslateModule} from '@ngx-translate/core';
import {
  MatButtonModule,
  MatCardModule,
  MatCheckboxModule,
  MatDialogModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatPaginatorModule,
  MatSelectModule,
  MatSortModule,
  MatTableModule
} from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgxsModule} from '@ngxs/store';
import {UserState} from '../../store/states/user.state';
import {AuthorityState} from '../../store/states/authority.state';
import {UserRoutingModule} from './user-routing.module';


@NgModule({
  declarations: [UserComponent, UpdateUserComponent],
  imports: [
    CommonModule,
    TranslateModule,
    MatButtonModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatIconModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    MatCheckboxModule,
    MatSelectModule,
    UserRoutingModule,
    MatDialogModule,
    NgxsModule.forFeature([
      UserState,
      AuthorityState
    ])
  ],
  entryComponents: [UpdateUserComponent]
})
export class UserModule {
}
