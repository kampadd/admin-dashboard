import {Component, OnInit} from '@angular/core';
import {NETWORK_IFRAME_URL} from '../../app.constants';

@Component({
  selector: 'app-network',
  templateUrl: './network.component.html',
  styleUrls: ['./network.component.scss']
})
export class NetworkComponent implements OnInit {

  iframeUrl = NETWORK_IFRAME_URL;

  constructor() {
  }

  ngOnInit() {
  }

}
