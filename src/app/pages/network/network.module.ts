import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {NetworkRoutingModule} from './network-routing.module';
import {NetworkComponent} from './network.component';
import {NgMagicIframeModule} from '@sebgroup/ng-magic-iframe';
import {MatProgressSpinnerModule} from '@angular/material';


@NgModule({
  declarations: [NetworkComponent],
  imports: [
    CommonModule,
    NetworkRoutingModule,
    NgMagicIframeModule,
    MatProgressSpinnerModule
  ]
})
export class NetworkModule {
}
