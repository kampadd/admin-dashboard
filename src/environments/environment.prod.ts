export const environment = {
  production: true,
  GATEWAY_SERVER_API: '',
  UAA_SERVER_API: '/services/c4samuaa',
  CMDB_SERVER_API: '/services/cmdb',
  CORE_SERVER_API: '/services/core',
  DEBUG_INFO_ENABLED: true,
  VERSION: 'v0.0.1'
};
